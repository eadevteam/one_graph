FROM node:20.8.0-alpine3.17

# Define working directory and copy source
WORKDIR /home/node/app
COPY . .

# Install Node dependencies and build server
RUN npm install
RUN npm run build

# AWS creds
RUN echo 'export $(strings /proc/1/environ | grep AWS_CONTAINER_CREDENTIALS_RELATIVE_URI)' >> /root/.profile

# Expose ports
EXPOSE 9999

# Start the app
CMD npm run serve
