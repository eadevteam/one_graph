/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  verbose: true,
  setupFilesAfterEnv: [
      "./jest.setup.js"
    ],
  testPathIgnorePatterns: [
      "<rootDir>/dist/",
      "<rootDir>/node_modules/"
    ],
};