import { appConfig, logger } from '../config';
import readline from 'readline';
import { MongoClient } from 'mongodb';

export async function dropAllFromLocalDatabase() {
  try {
    if (!appConfig.databaseURL.includes('localhost')) {
      throw new Error(
        "This script is only intended to run on a developer's local database, taking no action and exiting."
      );
    }

    const devMongoClient = new MongoClient(appConfig.databaseURL);
    await devMongoClient.connect();
    const database = devMongoClient.db(appConfig.database);
    const collections = await database.listCollections().toArray();

    if (collections.length === 0) {
      throw new Error(
        `${database.databaseName} is already empty, exiting script.`
      );
    }

    logger.info(
      `${
        database.databaseName
      } contains the following collections: ${collections
        .map((item) => item.name)
        .join(', ')}`
    );

    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });

    const answer: string = await new Promise((resolve) =>
      rl.question('Are you sure you want to drop the database? (y/n)', resolve)
    );

    if (['Y', 'y', 'yes', 'yes'].includes(answer)) {
      logger.info(
        `Wiping all data from all collections in ${database.databaseName}...`
      );

      await database.dropDatabase();
    } else {
      logger.info('Aborted script, no data removed.');
    }
  } catch (e) {
    logger.error(e);
  } finally {
    process.exit();
  }
}
