import { Db, ObjectId } from 'mongodb';
import { Collaborator, FieldSet, Record } from 'airtable';

import {
  LineItemDocument,
  LineItems,
} from '../../../datasources/line-items-datasource-mongo';
import {
  PartnerDocument,
  Partners,
} from '../../../datasources/partners-datasource-mongo';
import {
  People,
  PeopleDocument,
} from '../../../datasources/people-datasource-mongo';
import { Portfolios } from '../../../datasources/portfolios-datasource-mongo';
import {
  ProgramDocument,
  Programs,
} from '../../../datasources/programs-datasource-mongo';
import {
  ProjectDocument,
  Projects,
} from '../../../datasources/projects-datasource-mongo';
import {
  Properties,
  PropertyDocument,
} from '../../../datasources/properties-datasource-mongo';
import {
  Savings,
  SavingsDocument,
} from '../../../datasources/savings-datasource-mongo';
import {
  StageDocument,
  Stages,
} from '../../../datasources/stage-datasource-mongo';
import {
  TaskDocument,
  Tasks,
} from '../../../datasources/task-datasource-mongo';
import { airtableCustomBase } from '../../../config';
import { OneMongoResult, UtilityServiceNumber } from '../../../interfaces';
import { AlreadyMigratedError, InvalidDataError } from '../migrate';
import { Logger } from 'log4js';
import { Counters } from '../../../datasources/counters-datasource-mongo';

export async function migrateCustomProject(
  airtableProject: Record<FieldSet>,
  database: Db,
  errorLogger: Logger,
  successLogger: Logger
) {
  try {
    // todo: filter out or treat as special case (data is missing) all projects missing vrt_id etc

    const customMethodology =
      (airtableProject.get('Exported via Calculation Version') as string) ==
      'MLC_13.0.2'
        ? 'PGE Manual Lighting Calculator 13.0.2'
        : 'PGE Manual Lighting Calculator 13';

    const countersCollection = new Counters({
      modelOrCollection: database.collection('counters'),
    });

    const projectCollection = new Projects({
      modelOrCollection: database.collection('projects'),
    });

    const programCollection = new Programs({
      modelOrCollection: database.collection('programs'),
    });

    const lineItemCollection = new LineItems({
      modelOrCollection: database.collection('line_items'),
    });

    const peopleCollection = new People({
      modelOrCollection: database.collection('people'),
    });

    const savingsCollection = new Savings({
      modelOrCollection: database.collection('savings'),
    });

    const partnerCollection = new Partners({
      modelOrCollection: database.collection('partners'),
    });

    const portfolioCollection = new Portfolios({
      modelOrCollection: database.collection('portfolios'),
    });

    const propertyCollection = new Properties({
      modelOrCollection: database.collection('properties'),
    });

    const taskCollection = new Tasks({
      modelOrCollection: database.collection('tasks'),
    });

    const stageCollection = new Stages({
      modelOrCollection: database.collection('stages'),
    });

    const projectName = airtableProject.get('Project Name') as string;

    // todo: skip if cancelled?

    if (!projectName) {
      throw new InvalidDataError('missing project name, bad record, skipping');
    }

    const retrofits = airtableProject.get('Retrofit') as string[];
    if (!retrofits) {
      throw new InvalidDataError('no retrofits, skipping');
    }

    const programCode = airtableProject.get('Program Code') as string[];
    if (!programCode) {
      throw new InvalidDataError('missing program code, skipping');
    }

    const projectDoesNotExistInCollection =
      (
        await projectCollection.getProjectByName(
          airtableProject.get('Project Name') as string
        )
      ).data.length === 0;

    if (projectDoesNotExistInCollection) {
      const scopeLineItems: any[] = [];
      const savingsSnapshotLineItems = [];

      for (const id of airtableProject.get('Retrofit') as string[]) {
        const airtableRetrofit = await airtableCustomBase('Retrofit').find(id);

        const savingsSnapshotLineItem: LineItemDocument = {
          fields: {
            'fixture_/_lamp_base': airtableRetrofit.get('Fixture / Lamp Base'),
            'fixture_/_lamp_tech_description': airtableRetrofit.get(
              'Fixture / Lamp Tech Description'
            ),
            'fixture_/_lamp_type': airtableRetrofit.get('Lamp or Fixture Type'),
            'input_fixture_/_lamp_wattage': airtableRetrofit.get(
              'Input Fixture / Lamp Wattage'
            ),
            actual_hours: airtableRetrofit.get('Actual Hours'),
            area_name: airtableRetrofit.get('Area Name'),
            area_type: airtableRetrofit.get('Area Type'),
            building_vintage: airtableRetrofit.get('Building Vintage'),
            deemed_hours_of_use: airtableRetrofit.get('Deemed Hours Of Use'),
            default_existing_equipment_RUL: airtableRetrofit.get(
              'Default Existing Equipment RUL'
            ),
            'proposed_lamp_/_fixture_shape_type': airtableRetrofit.get(
              'Proposed Fixture / Lamp Shape Type'
            ),
            direct_install: airtableRetrofit.get('Direct Install?'),
            direct_kw_savings: airtableRetrofit.get('Direct kW Savings'),
            direct_kwh_savings: airtableRetrofit.get('Direct kWh Savings'),
            existing_controls: airtableRetrofit.get('Existing Controls'),
            existing_equipment_description: airtableRetrofit.get(
              'Existing Equipment Description'
            ),
            existing_fixture_EUL_ID: airtableRetrofit.get(
              'Existing Fixture EUL_ID'
            ),
            existing_lumens: airtableRetrofit.get('Existing Lumens'),
            existing_wattage: airtableRetrofit.get('Ex Watts'),
            general_application: airtableRetrofit.get('General Application'),
            gross_kwh_savings: airtableRetrofit.get('Gross Energy Savings kWh'),
            gross_peak_demand_kw_savings: airtableRetrofit.get(
              'Gross Peak Demand Savings kW'
            ),
            gross_therm_savings: airtableRetrofit.get(
              'Gross Energy Savings thm'
            ),
            incremental_measure_cost: airtableRetrofit.get(
              'Incremental Measure Cost'
            ),
            indirect_kw_savings: airtableRetrofit.get('Indirect kW Savings'),
            indirect_kwh_savings: airtableRetrofit.get('Indirect kWh Savings'),
            indirect_therm_savings: airtableRetrofit.get(
              'Indirect Therm Savings'
            ),
            lamp_or_fixture_technology: airtableRetrofit.get(
              'Lamp or Fixture Technology'
            ),
            lamp_per_fixture: airtableRetrofit.get('Lamp Per Fixture'),
            luminaire_category: airtableRetrofit.get('Luminaire Category'),
            luminaire_group: airtableRetrofit.get('Luminaire Group'),
            material_cost_per_unit: airtableRetrofit.get('Material Cost'), // THE VALUE IN AIRTABLE IS PER UNIT
            measure_application_type: airtableRetrofit.get(
              'Measure Application Type'
            ),
            measure_code: airtableRetrofit.get('UMC'),
            measure_description: airtableRetrofit.get('Measure Code Full Name'),
            primary_use: airtableRetrofit.get('Primary Use'),
            product_id: airtableRetrofit.get('Product ID'),
            proposed_fixture_EUL_ID: airtableRetrofit.get(
              'Proposed Fixture EUL_ID (Input DEER Area Type First)'
            ),
            quantity: airtableRetrofit.get('Qty'),
            replacement_equipment_description: airtableRetrofit.get(
              'Replacement Equipment Description'
            ),
            replacement_lumens: airtableRetrofit.get('Replacement Lumens'),
            replacement_wattage: airtableRetrofit.get('Replacement Watts'),
            second_baseline_kw_savings: airtableRetrofit.get(
              'Second Baseline kW Savings'
            ),
            second_baseline_kwh_savings: airtableRetrofit.get(
              'Second Baseline kWh Savings'
            ),
            second_baseline_therm_savings: airtableRetrofit.get(
              'Second Baseline Therm Savings'
            ),
            standard_labor_cost: airtableRetrofit.get('Standard Labor Cost'),
            standard_material_cost: airtableRetrofit.get(
              'Standard Material Cost'
            ),
            standard_practice_luminaire_name: airtableRetrofit.get(
              'Standard Practice Luminaire Name'
            ),
            tech_type: airtableRetrofit.get('Tech Type'),
            total_fees: airtableRetrofit.get('Fees'),
            total_retrofit_cost: airtableRetrofit.get('Total Retrofit Cost'),
            total_tax: airtableRetrofit.get('Tax'), // is this correct?
            total_vendor_rebate_amount: airtableRetrofit.get(
              'Total Vendor Rebate Amount'
            ),
          },
        };

        // Insert Savings Snapshot LineItem
        const savingsSnapshotLineItemInsertResult: OneMongoResult =
          await lineItemCollection.addLineItem(savingsSnapshotLineItem);

        // Append ID for association with savings later
        savingsSnapshotLineItems.push(
          savingsSnapshotLineItemInsertResult.data[0]._id
        );

        // Create a copy of line item with a subset of fields
        delete savingsSnapshotLineItem.fields.deemed_hours_of_use;
        delete savingsSnapshotLineItem.fields.default_existing_equipment_RUL;
        delete savingsSnapshotLineItem.fields.direct_kw_savings;
        delete savingsSnapshotLineItem.fields.direct_kwh_savings;
        delete savingsSnapshotLineItem.fields.gross_kwh_savings;
        delete savingsSnapshotLineItem.fields.gross_peak_demand_kw_savings;
        delete savingsSnapshotLineItem.fields.gross_therm_savings;
        delete savingsSnapshotLineItem.fields.incremental_measure_cost;
        delete savingsSnapshotLineItem.fields.indirect_kw_savings;
        delete savingsSnapshotLineItem.fields.indirect_kwh_savings;
        delete savingsSnapshotLineItem.fields.indirect_therm_savings;
        delete savingsSnapshotLineItem.fields.second_baseline_kw_savings;
        delete savingsSnapshotLineItem.fields.second_baseline_kwh_savings;
        delete savingsSnapshotLineItem.fields.second_baseline_therm_savings;
        delete savingsSnapshotLineItem.fields.standard_labor_cost;
        delete savingsSnapshotLineItem.fields.standard_material_cost;
        delete savingsSnapshotLineItem.fields.standard_practice_luminaire_name;
        delete savingsSnapshotLineItem.fields.total_retrofit_cost;
        delete savingsSnapshotLineItem.fields.total_tax;
        delete savingsSnapshotLineItem.fields.total_vendor_rebate_amount;

        // Insert Scope LineItem
        const scopeLineItemInsertResult: OneMongoResult =
          await lineItemCollection.addLineItem(savingsSnapshotLineItem);

        // Append ID for association with program later
        scopeLineItems.push(scopeLineItemInsertResult.data[0]._id);
      }

      // todo: check if more fields needed?
      const savings: SavingsDocument = {
        name: 'MigratedSavings',
        methodology: customMethodology,
        fields: {
          annual_kwh_savings: airtableProject.get('Total Project kWh'),
          annual_dollar_savings: airtableProject.get(
            'Actual-Based Estimated Annual Savings'
          ), // should we use Estimated Annual Savings?
          installation_cost: airtableProject.get('Total Project Cost'),
          incentive: airtableProject.get('Total Project Incentive'), // some projects also have flex?
          payback: airtableProject.get('Payback'), // there's also Actual Payback and OBFNI Payback...
        },
        line_items: savingsSnapshotLineItems,
      };

      const savingsInsertResult: OneMongoResult =
        await savingsCollection.addSavings(savings);

      const program: ProgramDocument = {
        name: (airtableProject.get('Program Code') as string)?.includes('HOS')
          ? 'Hospitality'
          : 'NetOne',
        fields: {
          code: (airtableProject.get('Program Code') as string)[0], // might break because complex obj
          building_type: airtableProject.get('Site Building Type'),
          climate_zone: airtableProject.get('Climate Zone')
            ? Number.parseInt(airtableProject.get('Climate Zone') as string)
            : null,
          building_vintage_code: airtableProject.get(
            'Building Vintage Code'
          ) as string,
          // tax_rate: 0.0835, // No rates in AT - calculate from Estimate Tax?
          // utility_rate: 0.28, // No rates in AT - calculate from Utility Cost Savings?
          electric_service_start_date:
            airtableProject.get('Elec SA Start Date'),
          year_property_built: airtableProject.get(
            'Year Property Built'
          ) as number,
          methodology: customMethodology,
        },
        savings: [savingsInsertResult.data[0]._id],
        line_items: scopeLineItems,
      };

      const programInsertResult: OneMongoResult =
        await programCollection.addProgram(program);

      const address = (
        airtableProject.get('Installation Address From Site') as string[]
      )[0];

      const addressTokens = address.split(' ');

      const elecSaids = (
        (airtableProject.get('Electric SAID') as string[])[0] as string
      ).split(',');

      const property: PropertyDocument = {
        address_1: address,
        zip_code: Number.parseInt(addressTokens.pop()),
        state: addressTokens.pop(),
        utility_service_numbers: [
          ...elecSaids.map(
            (item) =>
              ({
                type: 'Electric',
                value: item,
              } as UtilityServiceNumber)
          ),
          {
            type: 'Gas',
            value: airtableProject.get('Gas SAID'),
          } as UtilityServiceNumber,
        ],
        surveys: [],
      };

      const propertyInsertResult: OneMongoResult =
        await propertyCollection.addProperty(property);

      const airtableManager: Collaborator = airtableProject.get(
        'Project Manager'
      ) as Collaborator;
      let first = 'Airtable';
      let last = 'Custom';
      let email = '';
      if (airtableManager) {
        first = airtableManager.name.split(' ')[0];
        last = airtableManager.name.split(' ')[1];
        email = airtableManager.email;
      }

      const manager: PeopleDocument = {
        first_name: first,
        last_name: last,
        email: email,
        company: 'Taper',
        role: 'Project Manager (Legacy)',
      };

      const managerInsertResult = await peopleCollection.addPerson(manager);

      let airtablePartnerName =
        (airtableProject.get('Partner Name') as string[])[0] ??
        'Missing Partner';

      const partnerPerson: PeopleDocument = {
        first_name: airtablePartnerName.split(' ')[0],
        last_name: airtablePartnerName.split(' ')[1],
        email: airtableProject.get('Partner Email') as string,
        phone_number: airtableProject.get(
          'Partner Company Telephone from Partner'
        ) as string,
        company: (airtableProject.get('Partner Company Name') as string[])[0],
        role: 'Partner (Legacy)',
      };

      const existingPartner = await partnerCollection.getPartnerByName(
        airtablePartnerName
      );

      let partner: ObjectId;

      if (existingPartner.data.length > 0) {
        const existingPartnerPeople = existingPartner.data[0]?.people;
        if (existingPartnerPeople) {
          const people = await peopleCollection.getPeopleByIds(
            existingPartnerPeople.map((item) => item.toString())
          );

          // If we already have the partner but need to add a person/employee/member
          const existingPartnerPeopleNames = people.data.map(
            (item) => `${item.first_name} ${item.last_name}`
          );
          if (!existingPartnerPeopleNames?.includes(airtablePartnerName)) {
            // add person to existing partner
            const partnerPersonInsertResult = await peopleCollection.addPerson(
              partnerPerson
            );
            await partnerCollection.updatePartner(
              existingPartner.data[0]._id.toString(),
              {
                people: [
                  ...existingPartner.data[0].people,
                  partnerPersonInsertResult.data[0]._id,
                ],
              }
            );

            partner = existingPartner.data[0]._id;
          }
        }
      } else {
        const partnerPersonInsertResult = await peopleCollection.addPerson(
          partnerPerson
        );

        const partnerDocument: PartnerDocument = {
          name: (airtableProject.get('Partner Company Name') as string[])[0],
          people: [partnerPersonInsertResult.data[0]._id],
        };

        const partnerInsertResult = await partnerCollection.addPartner(
          partnerDocument
        );

        partner = partnerInsertResult.data[0]._id;
      }

      const dateCommitted = new Date(
        airtableProject.get('Committed Date') as string
      );
      const commitToUtility: TaskDocument = {
        name: 'Commit Project to Utility Portal',
        completed: dateCommitted,
        description: '',
      };

      const commitTaskInsertResult = await taskCollection.addTask(
        commitToUtility
      );

      const datePosted = new Date(airtableProject.get('Date Posted') as string); // airtable dates are ISO8601

      const provideCompletionPackageToDelivery: TaskDocument = {
        name: 'Date Posted',
        completed: datePosted,
        description:
          'Provide completion package to delivery, indicating that incentivized installation is ready to report.',
      };

      const completionPacketTaskInsertResult = await taskCollection.addTask(
        provideCompletionPackageToDelivery
      );

      const dateReported = new Date(3000);
      // there is no reported date in the custom base
      const reportToUtility: TaskDocument = {
        name: 'Report Project to Utility Portal',
        completed: dateReported,
        description: '',
      };

      const reportTaskInsertResult = await taskCollection.addTask(
        reportToUtility
      );

      const workflow: StageDocument[] = [
        {
          name: 'Sold',
          tasks: [
            commitTaskInsertResult.data[0]._id,
            completionPacketTaskInsertResult.data[0]._id,
          ],
        },
        { name: 'Delivered', tasks: [reportTaskInsertResult.data[0]._id] },
        { name: 'Completed' },
      ];

      const airtableCurrentStage = airtableProject.get(
        'Current Pipeline'
      ) as string;

      let currentStageInWorkflowId;

      const workflowStageSoldInsertResult = await stageCollection.addStage(
        workflow[0]
      );
      const workflowStageDeliveredInsertResult = await stageCollection.addStage(
        workflow[1]
      );
      const workflowStageCompletedInsertResult = await stageCollection.addStage(
        workflow[2]
      );

      if (['Sales', 'Delivery'].includes(airtableCurrentStage)) {
        currentStageInWorkflowId = workflowStageSoldInsertResult.data[0]._id;
      } else if (['Ready to Report'].includes(airtableCurrentStage)) {
        currentStageInWorkflowId =
          workflowStageDeliveredInsertResult.data[0]._id;
      } else if (['Reported', 'Fullfilled'].includes(airtableCurrentStage)) {
        currentStageInWorkflowId =
          workflowStageCompletedInsertResult.data[0]._id;
      } else if (airtableCurrentStage === 'Sales') {
        throw new InvalidDataError(
          `may not be sold (what does SALES map to?), skipping migration.`
        );
      } else {
        throw new InvalidDataError(
          `unmapped Pipeline (Stage) of ${airtableCurrentStage}, skipping.`
        );
      }

      const workflowIds = [
        workflowStageSoldInsertResult.data[0]._id,
        workflowStageDeliveredInsertResult.data[0]._id,
        workflowStageCompletedInsertResult.data[0]._id,
      ];

      const project: ProjectDocument = {
        name: airtableProject.get('Project Name') as string,
        programs: [programInsertResult.data[0]._id],
        property: propertyInsertResult.data[0]._id,
        current_stage: currentStageInWorkflowId,
        workflow: workflowIds,
        manager: managerInsertResult.data[0]._id,
        partners: [partner],
      };

      let nextProjectNumber = countersCollection.getNextProjectNumber();
      const projectInsertResult: OneMongoResult =
        await projectCollection.addProject(null, project, nextProjectNumber);
      successLogger.info(`${airtableProject.get('Project Name')}`);
    } else {
      throw new AlreadyMigratedError(
        'already exists in the target database, skipping this project.'
      );
    }
  } catch (e) {
    if (e instanceof InvalidDataError || e instanceof AlreadyMigratedError) {
      errorLogger.error(`${airtableProject.get('Project Name')} ${e.name}`);
    } else {
      errorLogger.error(
        `${airtableProject.get('Project Name')} \n ${e} ${e.stack}`
      );
    }
  }
}
