import {
  airtableCustomBase,
  airtableCustomBaseId,
  airtableDeemedBase,
  airtableDeemedBaseId,
  oneMongoClient,
  appConfig,
  logger,
  summaryLoggerDeemed,
  errorLoggerDeemed,
  successLoggerDeemed,
  summaryLoggerCustom,
  errorLoggerCustom,
  successLoggerCustom,
} from '../../config';
import { MigrationArgs, OneMongoMigrationResults } from '../../interfaces';
import { migrateDeemedProject } from './deemed/single_project_migrator';
import { migrateCustomProject } from './custom/single_project_migrator';

import { exec } from 'child_process';

export class InvalidDataError extends Error {
  constructor(message?: string) {
    super(message);
    this.name = message;
  }
}

export class AlreadyMigratedError extends Error {
  constructor(message?: string) {
    super(message);
    this.name = message;
  }
}

export async function migrateFromAirtable() {
  try {
    logger.debug('Mongo Database URL: ' + appConfig.databaseURL);

    await oneMongoClient.connect();
    const oneDatabase = oneMongoClient.db(appConfig.database);

    // todo bug:
    /**
     * (Project Name) Whole Foods San Francisco-ADC shows up multiple times in the deemed base
     * Property name underneath is slightly unique. Big implications.
     */

    await migration({
      source: airtableDeemedBase,
      target: oneDatabase,
      projectMappingCallback: migrateDeemedProject,
      summaryLogger: summaryLoggerDeemed,
      errorLogger: errorLoggerDeemed,
      successLogger: successLoggerDeemed,
    });

    // await migration({
    //   source: airtableCustomBase,
    //   target: oneDatabase,
    //   projectMappingCallback: migrateCustomProject,
    //   summaryLogger: summaryLoggerCustom,
    //   errorLogger: errorLoggerCustom,
    //   successLogger: successLoggerCustom,
    // });
  } catch (e) {
    logger.error(e);
  } finally {
    await oneMongoClient.close();
  }
}

export async function migration({
  source,
  target,
  projectMappingCallback,
  summaryLogger,
  errorLogger,
  successLogger,
}: MigrationArgs) {
  let baseName = '';
  let view;
  const start = Date.now();
  let count = 0;

  try {
    if (source.getId() == airtableDeemedBaseId) {
      baseName = 'deemed';
      view = 'Future ADC 4 Migration';
    } else if (source.getId() == airtableCustomBaseId) {
      baseName = 'custom';
    }

    await source('Project')
      .select({
        maxRecords: 1400,
        view: view,
      })
      .eachPage(async (records, fetchNextPage) => {
        for (const airtableProjectRecord of records) {
          try {
            await projectMappingCallback(
              airtableProjectRecord,
              target,
              errorLogger,
              successLogger
            );
          } catch (e) {
            logger.error(e);
          }
          count++;
          console.clear();
          console.log(`Processed ${count} ${baseName} projects`);
        }

        fetchNextPage();
      });
  } catch (e) {
    logger.error(
      `Error during migration of ${baseName} base to ${appConfig.database}: \n ${e}`
    );
  } finally {
    const end = Date.now();
    const elapsed = (end - start) / 1000;

    summaryLogger.info(`${elapsed} seconds elapsed.`);
    summaryLogger.info(`${count} ${baseName} records processed`);

    exec(
      `wc -l < ./src/scripts/airtable_migration/${baseName}/success.log`,
      (error, stdout, stderr) => {
        summaryLogger.info(`${stdout.replace('\n', '')} successfully migrated`);
      }
    );

    exec(
      `wc -l < ./src/scripts/airtable_migration/${baseName}/error.log`,
      (error, stdout, stderr) => {
        summaryLogger.info(`${stdout.replace('\n', '')} failed to migrate`);
      }
    );
  }
}

function printDeleteInfo({
  project,
  program,
  savings,
}: OneMongoMigrationResults) {
  const deleteProject =
    'db.projects.deleteOne( {"_id": ObjectId("' + project.data[0]._id + '")});';

  const deleteProgram =
    'db.programs.deleteOne( {"_id": ObjectId("' + program.data[0]._id + '")});';

  let deleteLineItems = 'db.line_items.deleteMany({"_id": { $in: [ ';

  for (const line of program.data.line_items) {
    deleteLineItems += ' ObjectId("' + line + '"),';
  }

  for (const line of savings.data.line_items) {
    deleteLineItems += ' ObjectId("' + line + '"),';
  }

  deleteLineItems += ']}});';
  logger.debug(' Scripts for deleting: ');
  logger.debug(deleteProject);
  logger.debug(deleteProgram);
  logger.debug(deleteLineItems);
}
