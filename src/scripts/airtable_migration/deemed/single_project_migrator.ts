import { Db, ObjectId } from 'mongodb';
import { Collaborator, FieldSet, Record } from 'airtable';

import {
  LineItemDocument,
  LineItems,
} from '../../../datasources/line-items-datasource-mongo';
import {
  PartnerDocument,
  Partners,
} from '../../../datasources/partners-datasource-mongo';
import {
  People,
  PeopleDocument,
} from '../../../datasources/people-datasource-mongo';
import {
  PortfolioDocument,
  Portfolios,
} from '../../../datasources/portfolios-datasource-mongo';
import {
  ProgramDocument,
  Programs,
} from '../../../datasources/programs-datasource-mongo';
import {
  ProjectDocument,
  Projects,
} from '../../../datasources/projects-datasource-mongo';
import {
  Properties,
  PropertyDocument,
} from '../../../datasources/properties-datasource-mongo';
import {
  Savings,
  SavingsDocument,
} from '../../../datasources/savings-datasource-mongo';
import {
  StageDocument,
  Stages,
} from '../../../datasources/stage-datasource-mongo';
import {
  TaskDocument,
  Tasks,
} from '../../../datasources/task-datasource-mongo';
import { airtable, airtableDeemedBase, logger } from '../../../config';
import { OneMongoResult, UtilityServiceNumber } from '../../../interfaces';
import { AlreadyMigratedError, InvalidDataError } from '../migrate';
import { Logger } from 'log4js';
import { Counters } from '../../../datasources/counters-datasource-mongo';

export async function migrateDeemedProject(
  airtableProject: Record<FieldSet>,
  database: Db,
  errorLogger: Logger,
  successLogger: Logger
) {
  try {
    // Only Migrate Specific Group of ADC projects that will be used in the future

    const deemedMethodology = 'PGE Deemed 6.22';

    const countersCollection = new Counters({
      modelOrCollection: database.collection('counters'),
    });

    const projectCollection = new Projects({
      modelOrCollection: database.collection('projects'),
    });

    const programCollection = new Programs({
      modelOrCollection: database.collection('programs'),
    });

    const lineItemCollection = new LineItems({
      modelOrCollection: database.collection('line_items'),
    });

    const peopleCollection = new People({
      modelOrCollection: database.collection('people'),
    });

    const savingsCollection = new Savings({
      modelOrCollection: database.collection('savings'),
    });

    const partnerCollection = new Partners({
      modelOrCollection: database.collection('partners'),
    });

    const portfolioCollection = new Portfolios({
      modelOrCollection: database.collection('portfolios'),
    });

    const propertyCollection = new Properties({
      modelOrCollection: database.collection('properties'),
    });

    const taskCollection = new Tasks({
      modelOrCollection: database.collection('tasks'),
    });

    const stageCollection = new Stages({
      modelOrCollection: database.collection('stages'),
    });

    const projectName = airtableProject.get('Project Name') as string;

    if (!projectName) {
      throw new InvalidDataError('missing project name, bad record, skipping');
    }

    const retrofits = airtableProject.get('Scope of Work') as string[];
    if (!retrofits) {
      throw new InvalidDataError('no retrofits, skipping');
    }

    const programCode = airtableProject.get('Program Code') as string[];
    if (!programCode) {
      throw new InvalidDataError('missing program code, skipping');
    }

    const projectDoesNotExistInCollection =
      (await projectCollection.getProjectByName(projectName)).data.length === 0;

    if (projectDoesNotExistInCollection) {
      const scopeLineItems: any[] = [];
      const savingsSnapshotLineItems = [];

      for (const id of retrofits) {
        const airtableRetrofit = await airtableDeemedBase('Retrofit').find(id);

        const savingsSnapshotLineItem: LineItemDocument = {
          fields: {
            measure_code: airtableRetrofit.get('Measure Code') as string,
            measure_description: airtableRetrofit.get(
              'Measure Description'
            ) as string,
            unit_of_measure: airtableRetrofit.get('Unit of Measure') as string,
            quantity: airtableRetrofit.get('Quantity') as number,
            incentive: airtableRetrofit.get('Total Rebate') as number, // used Total Rebate column
            material_cost: airtableRetrofit.get(
              'Total Material Cost'
            ) as number,
            labor_cost: airtableRetrofit.get('Total Labor Cost') as number,
            area_name: airtableRetrofit.get('Area') as string,
            installation_cost: airtableRetrofit.get(
              'Installation Cost'
            ) as number,
            kw_savings: airtableRetrofit.get('kW Savings') as number,
            kwh_savings: airtableRetrofit.get('kWh Savings') as number,
            therms_savings: airtableRetrofit.get('Therms Savings') as number,
            utility_cost_savings: airtableRetrofit.get(
              'Utility Cost Savings'
            ) as number,
            tax:
              airtableRetrofit.get('Estimated Tax') != null
                ? (airtableRetrofit.get('Estimated Tax') as number)
                : 0,
            fees:
              airtableRetrofit.get('Estimated Fees') != null
                ? (airtableRetrofit.get('Estimated Fees') as number)
                : 0,
            building_hvac: 'whatisthis',
            ntg_id: 'whatisthis',
            gsia_id: 'whatisthis',
          },
        };

        // Insert Savings Snapshot LineItem
        const savingsSnapshotLineItemInsertResult: OneMongoResult =
          await lineItemCollection.addLineItem(savingsSnapshotLineItem);

        // Append ID for association with savings later
        savingsSnapshotLineItems.push(
          savingsSnapshotLineItemInsertResult.data[0]._id
        );

        // Create a copy of line item with a subset of fields
        delete savingsSnapshotLineItem.fields.incentive;
        delete savingsSnapshotLineItem.fields.kw_savings;
        delete savingsSnapshotLineItem.fields.kwh_savings;
        delete savingsSnapshotLineItem.fields.therms_savings;
        delete savingsSnapshotLineItem.fields.utility_cost_savings;
        delete savingsSnapshotLineItem.fields.building_hvac;
        delete savingsSnapshotLineItem.fields.ntg_id;
        delete savingsSnapshotLineItem.fields.gsia_id;

        // Insert Scope LineItem
        const scopeLineItemInsertResult: OneMongoResult =
          await lineItemCollection.addLineItem(savingsSnapshotLineItem);

        // Append ID for association with program later
        scopeLineItems.push(scopeLineItemInsertResult.data[0]._id);
      }

      const savings: SavingsDocument = {
        name: 'MigratedSavings',
        methodology: deemedMethodology,
        fields: {
          annual_kwh_savings: airtableProject.get('Final Total kWh'), // fallback to estimated?
          annual_dollar_savings: airtableProject.get('Utility Cost Savings'), // idk if this is right
          installation_cost: airtableProject.get('Final Installation Cost'), // fallback to estimated?
          incentive: airtableProject.get('Final Rebate'), // fallback to estimated?
          payback: airtableProject.get('Estimated Simple Payback'), // there's also OBFNI Payback...
        },
        line_items: savingsSnapshotLineItems,
      };

      const savingsInsertResult: OneMongoResult =
        await savingsCollection.addSavings(savings);

      const program: ProgramDocument = {
        name: programCode[0]?.includes('HOS') ? 'Hospitality' : 'NetOne',
        fields: {
          code: programCode,
          building_type: airtableProject.get('Building Type'),
          climate_zone: (airtableProject.get('Climate Zone') as string)
            ? Number.parseInt(
                (airtableProject.get('Climate Zone') as string).replace(
                  'CZ',
                  ''
                )
              )
            : null,
          // building_vintage_code: 85, // missing
          // tax_rate: 0.0835, // calculate from Estimate Tax?
          // utility_rate: 0.28, // calculate from Utility Cost Savings?
          // electric_service_start_date: '2019-03-25T23:43:14.768Z',
          year_property_built: airtableProject.get('Year Property Built'),
          methodology: deemedMethodology,
        },
        savings: [savingsInsertResult.data[0]._id],
        line_items: scopeLineItems,
      };

      const programInsertResult: OneMongoResult =
        await programCollection.addProgram(program);

      const property: PropertyDocument = {
        address_1: airtableProject.get('Site Shipping Street') as string,
        city: airtableProject.get('Site Shipping City') as string,
        state: airtableProject.get('Site Shipping State') as string,
        zip_code: airtableProject.get('Site Shipping Zip') as number,
        utility_service_numbers: [
          {
            type: 'Electric',
            value: `${airtableProject.get('Elec SAID')}`,
          } as UtilityServiceNumber,
          {
            type: 'Gas',
            value: airtableProject.get('Gas SAID'),
          } as UtilityServiceNumber,
        ],
        surveys: [],
      };

      const propertyInsertResult: OneMongoResult =
        await propertyCollection.addProperty(property);

      const airtableManager: Collaborator = airtableProject.get(
        'Incentive Manager'
      ) as Collaborator;
      let first = 'Airtable';
      let last = 'Deemed';
      let email = '';
      if (airtableManager) {
        first = airtableManager.name.split(' ')[0];
        last = airtableManager.name.split(' ')[1];
        email = airtableManager.email;
      }

      const manager: PeopleDocument = {
        first_name: first,
        last_name: last,
        email: email,
        company: 'Taper',
        role: 'Incentive Manager (Legacy)',
      };

      const managerInsertResult = await peopleCollection.addPerson(manager);

      const airtableCustomer = airtableProject.get('Customer Name') as string;
      let customer;
      if (airtableCustomer != null) {
        const customerDocument: PeopleDocument = {
          first_name: airtableCustomer.split(' ')[0],
          last_name: airtableCustomer.split(' ')[1],
          email: airtableProject.get('Customer Email') as string,
          company: airtableProject.get('Customer Phone') as string,
          role: 'Customer',
        };

        customer = (await peopleCollection.addPerson(customerDocument)).data[0]
          ._id;
      }

      const datePosted = (airtableProject.get('Date Posted') as string)
        ? new Date(airtableProject.get('Date Posted') as string)
        : null; // airtable dates are ISO8601

      const dateCommitted = (airtableProject.get(
        'Date Committed to EI'
      ) as string)
        ? new Date(airtableProject.get('Date Committed to EI') as string)
        : null;

      const commitToUtility: TaskDocument = {
        name: 'Commit Project to Utility Portal',
        completed: dateCommitted,
        description: '',
      };

      const commitTaskInsertResult = await taskCollection.addTask(
        commitToUtility
      );

      const provideCompletionPackageToDelivery: TaskDocument = {
        name: 'Date Posted',
        completed: datePosted,
        description:
          'Provide completion package to delivery, indicating that incentivized installation is ready to report.',
      };

      const completionPacketTaskInsertResult = await taskCollection.addTask(
        provideCompletionPackageToDelivery
      );

      const dateReported = (airtableProject.get(
        'Date Final Reporting to EI'
      ) as string)
        ? new Date(airtableProject.get('Date Final Reporting to EI') as string)
        : null;

      const reportToUtility: TaskDocument = {
        name: 'Report Project to Utility Portal',
        completed: dateReported,
        description: '',
      };

      const reportTaskInsertResult = await taskCollection.addTask(
        reportToUtility
      );

      const workflow: StageDocument[] = [
        {
          name: 'Sold',
          tasks: [
            commitTaskInsertResult.data[0]._id,
            completionPacketTaskInsertResult.data[0]._id,
          ],
        },
        { name: 'Delivered', tasks: [reportTaskInsertResult.data[0]._id] },
        { name: 'Completed' },
      ];

      const airtableCurrentStage = airtableProject.get('Stage') as string;

      let currentStageInWorkflowId;

      const workflowStageSoldInsertResult = await stageCollection.addStage(
        workflow[0]
      );
      const workflowStageDeliveredInsertResult = await stageCollection.addStage(
        workflow[1]
      );
      const workflowStageCompletedInsertResult = await stageCollection.addStage(
        workflow[2]
      );

      if (
        ['Application Development', 'Installing', 'Ready to Commit']?.includes(
          airtableCurrentStage
        )
      ) {
        currentStageInWorkflowId = workflowStageSoldInsertResult.data[0]._id;
      } else if (
        ['Ready to Report', 'Reported']?.includes(airtableCurrentStage)
      ) {
        currentStageInWorkflowId =
          workflowStageDeliveredInsertResult.data[0]._id;
      } else if (
        ['THERE IS NO COMPLETED????']?.includes(airtableCurrentStage)
      ) {
        currentStageInWorkflowId =
          workflowStageCompletedInsertResult.data[0]._id;
      }

      const workflowIds = [
        workflowStageSoldInsertResult.data[0]._id,
        workflowStageDeliveredInsertResult.data[0]._id,
        workflowStageCompletedInsertResult.data[0]._id,
      ];

      const project: ProjectDocument = {
        name: airtableProject.get('Project Name') as string,
        programs: [programInsertResult.data[0]._id],
        property: propertyInsertResult.data[0]._id,
        current_stage: currentStageInWorkflowId,
        workflow: workflowIds,
        manager: managerInsertResult.data[0]._id,
        contact: customer,
      };

      let nextProjectNumber = countersCollection.getNextProjectNumber();
      const projectInsertResult: OneMongoResult =
        await projectCollection.addProject(null, project, nextProjectNumber);

      const portfolioName = airtableProject.get('Opportunity Name') as string;
      const portfolioExistsCheck = await portfolioCollection.getPortfolioByName(
        portfolioName
      );

      if (portfolioExistsCheck.data.length === 0) {
        const portfolio: PortfolioDocument = {
          name: portfolioName,
          projects: [projectInsertResult.data[0]._id],
        };
        await portfolioCollection.addPortfolio(portfolio);
      } else {
        await portfolioCollection.addToPortfolio(
          portfolioExistsCheck.data[0]._id,
          [projectInsertResult.data[0]._id]
        );
      }

      successLogger.info(`${airtableProject.get('Project Name')}`);
    } else {
      throw new AlreadyMigratedError(
        'already exists in the target database, skipping this project.'
      );
    }
  } catch (e) {
    if (e instanceof InvalidDataError || e instanceof AlreadyMigratedError) {
      errorLogger.error(`${airtableProject.get('Project Name')} ${e.name}`);
    } else {
      errorLogger.error(
        `${airtableProject.get('Project Name')} \n ${e} ${e.stack}`
      );
    }
  }
}
