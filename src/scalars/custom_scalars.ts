import { logger } from '../config';

const { GraphQLScalarType, Kind } = require('graphql');

export const dateScalar = new GraphQLScalarType({
  name: 'Date',
  description: 'Date custom scalar type',
  serialize(value: Date) {
    //    logger.debug('dateScalar value ' + value);
    //    logger.debug(typeof value);
    if (value instanceof Date) {
      return value.toISOString();
    } else {
      return value; // Convert outgoing Date to string for JSON
    }
  },
  parseValue(value: any) {
    return new Date(value); // Convert incoming integer to Date
  },
  parseLiteral(ast: any) {
    if (ast.kind === Kind.INT) {
      return new Date(parseInt(ast.value, 10)); // Convert hard-coded AST string to integer and then to Date
    }
    return null; // Invalid hard-coded value (not an integer)
  },
});

export const jsonScalar = new GraphQLScalarType({
  name: 'JSON',
  description:
    'The `JSON` scalar type represents JSON objects as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf).',
  specifiedByUrl:
    'http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf',
  serialize: ensureObject,
  parseValue: ensureObject,
  parseLiteral: (ast: any, variables: any) => {
    if (ast.kind !== Kind.OBJECT) {
      throw new TypeError(`JSON cannot represent non-object value: ${ast}`);
    }

    return parseObject('JSON', ast, variables);
  },
});

function ensureObject(value: any) {
  if (typeof value !== 'object' || value === null || Array.isArray(value)) {
    throw new TypeError(`JSON cannot represent non-object value: ${value}`);
  }

  return value;
}

function parseObject(typeName: any, ast: any, variables: any) {
  const value = Object.create(null);
  ast.fields.forEach((field: any) => {
    // eslint-disable-next-line no-use-before-define
    value[field.name.value] = parseLiteral(typeName, field.value, variables);
  });

  return value;
}

function parseLiteral(typeName: any, ast: any, variables: any) {
  switch (ast.kind) {
    case Kind.STRING:
    case Kind.BOOLEAN:
      return ast.value;
    case Kind.INT:
    case Kind.FLOAT:
      return parseFloat(ast.value);
    case Kind.OBJECT:
      return parseObject(typeName, ast, variables);
    case Kind.LIST:
      return ast.values.map((n: any) => parseLiteral(typeName, n, variables));
    case Kind.NULL:
      return null;
    case Kind.VARIABLE:
      return variables ? variables[ast.name.value] : undefined;
    default:
      throw new TypeError(`${typeName} cannot represent value: ${ast}`);
  }
}
