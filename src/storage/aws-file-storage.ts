import Multer from 'multer';
import { logger, bucket, s3Client, awsregion } from '../config';
import { PutObjectCommand } from '@aws-sdk/client-s3';
import fs from 'fs';
import Jimp from 'jimp';

const uploadToBucket = async ({ directory, photo, key }) => {
  const filename = directory + '/' + key + '.jpg';

  const buffer = Buffer.from(photo, 'base64');

  const image = await Jimp.read(buffer);
  const imageBuffer = await image.getBufferAsync(Jimp.MIME_JPEG);

  try {
    const uploadResponse = await s3Client.send(
      new PutObjectCommand({
        Bucket: bucket,
        Key: filename,
        Body: imageBuffer,
        ContentType: 'image/jpeg',
      })
    );

    logger.debug(uploadResponse);

    // S3 automatically URI encodes the "Key" for a bucket object, but doesn't return that object
    // so we have to encode the filename before we store that in our database.
    return `https://${bucket}.s3-${awsregion}.amazonaws.com/${encodeURIComponent(
      filename
    )}`;
  } catch (e) {
    logger.error(e);
  }
};

const downloadFromBucket = (
  req: any,
  res: any,
  next: any,
  filename: string
) => {
  const params: any = {
    Bucket: bucket,
    Key: filename,
  };
  s3Client.getObject(params, (err, data) => {
    if (err) {
      logger.error('Failed to get S3 object/file', err);
    }
    if (data) {
      logger.info(`Successfully got S3 object/file: ${filename}`);
      res.send(data.Body);
    }
  });
};

const downloadFromBucketAndDelete = (
  req: any,
  res: any,
  next: any,
  filename: string
) => {
  const params: any = {
    Bucket: bucket,
    Key: filename,
  };
  s3Client.getObject(params, (err, data) => {
    if (err) {
      logger.error('Failed to get S3 object/file', err);
    }
    if (data) {
      logger.info(`Successfully got S3 object/file: ${filename}`);
      res.send(data.Body);
      s3Client.deleteObject(params, (delErr, delData) => {
        if (delErr) {
          logger.error('Failed to delete S3 object/file' + filename, delErr);
        }
        if (delData) {
          logger.info('Successfully deleted S3 object/file ' + filename);
        }
      });
    }
  });
};

const deleteFromBucket = (req: any, res: any, next: any, filename: string) => {
  const params: any = {
    Bucket: bucket,
    Key: filename,
  };
  s3Client.deleteObject(params, (err, data) => {
    if (err) {
      logger.error('Failed to delete S3 object/file' + filename, err);
    }
    if (data) {
      logger.info('Successfully deleted S3 object/file ' + filename);
    }
  });
};

const multer = Multer({
  limits: {
    fileSize: 100 * 1024 * 1024, // max filesize of 100mb
  },
  storage: Multer.memoryStorage(),
});

export {
  multer,
  uploadToBucket,
  downloadFromBucket,
  deleteFromBucket,
  downloadFromBucketAndDelete,
};
