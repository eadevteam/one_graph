import Airtable from 'airtable';
import { configure, getLogger } from 'log4js';
import { MongoClient } from 'mongodb';
import { ConnectionsConfig } from './interfaces';
import pg from 'pg';
import { S3 } from '@aws-sdk/client-s3';

const dburl = process.env.DBURL ?? '';
export const onedatabase = process.env.ONEDATABASE ?? '';
const awsversion = process.env.AWSVERSION || '2006-03-01';
export const awsregion = process.env.AWSREGION || 'us-west-2';

const apiHost = process.env.HOST || 'http://localhost';
const apiPort = process.env.PORT || '9999';
const emailService = process.env.EMAILSERVICE || 'http://localhost:5007';

export const emailBase = `${emailService}`;

const amiDataService = process.env.AMIDATASERVICE || 'http://localhost:3006';//'https://6796-73-8-27-171.ngrok-free.app/';
export const amiDataServiceBase = `${amiDataService}`;

export const airtableCustomBaseId = 'appOYnNIon0CKazR7';
export const airtableDeemedBaseId = 'app3dluf89mBZUYOI';
const airtableAPIKey = process.env.AIRTABLE_API_KEY;

export const airtable = new Airtable({ apiKey: airtableAPIKey });
export const airtableCustomBase = airtable.base(airtableCustomBaseId);
export const airtableDeemedBase = airtable.base(airtableDeemedBaseId);

export const caEtrmToken = process.env.CA_ETRM_TOKEN;
export const caEtrmHost = process.env.CA_ETRM_HOST;
export const caEtrmUser = process.env.CA_ETRM_USER;
export const caEtrmPassword = process.env.CA_ETRM_PASSWORD;
export const caEtrmDatabase = process.env.CA_ETRM_DATABASE;

export const hubspotAPIKey = process.env.HUBSPOT_API_KEY;

export const pgeShareMyDataClientId = process.env.PGE_SHARE_MY_DATA_CLIENT_ID;
export const pgeShareMyDataClientSecret =
  process.env.PGE_SHARE_MY_DATA_CLIENT_SECRET;


export const bucket = process.env.S3BUCKET || 'one-app-files'; // change to one-dev

const ws = process.env.WSBASE || 'http://localhost:8889'; // rules engine service
export const wsBase = `${ws}`;

const appConfig: ConnectionsConfig = {
  awsRegion: awsregion,
  awsVersion: awsversion,
  databaseURL: dburl,
  database: onedatabase,
  ssl: false,
};

export const oneMongoClient = new MongoClient(appConfig.databaseURL, {
  tlsCAFile: `global-bundle.pem`, //Specify the AWS DocDB cert https://docs.aws.amazon.com/documentdb/latest/developerguide/ca_cert_rotation.html
});

export const s3Client = new S3({
  apiVersion: awsversion,
  region: awsregion,
});

// Call S3 to list the buckets
s3Client.listBuckets(function (err, data) {
  if (err) {
    console.log('Error', err);
  } else {
    console.log('Success', data.Buckets);
  }
});

/**
 * Setup the connection pool to the eTrm database for use by endpoint
 */
export const pgPool = new pg.Pool({
  host: caEtrmHost,
  user: caEtrmUser,
  password: caEtrmPassword,
  database: caEtrmDatabase,
  max: 20,
  idleTimeoutMillis: 30000,
  connectionTimeoutMillis: 2000,
});

pgPool.on('error', (err, client) => {
  logger.error('Unexpected error on idle postgress client' + err);
  client.release();
});

/** Build Environment is set in AWS ECS Task Definition and determines which URLs
 *  are allowed by CORS.
 */
export const buildEnvironment = process.env.BUILD_ENVIRONMENT || 'local';
export const getCorsOrigins = () => {
  logger.info(`Build Environment: ${buildEnvironment}`);

  if (buildEnvironment === 'dev') {
    return ['https://one-dev.codemagic.app'];
  }

  if (buildEnvironment === 'staging') {
    return ['https://one-stage.codemagic.app'];
  }

  if (buildEnvironment === 'prod') {
    return ['https://one.codemagic.app'];
  }
};

/**
 * All logs go to standard out, and are piped from their docker container
 * into AWS CloudWatch for storage and processing. Documentation available via
 * https://log4js-node.github.io/log4js-node/api.html
 */
const defaultLogCategoryConfig = {
  appenders: ['app'],
  level: 'debug',
  enableCallStack: true,
};

configure({
  appenders: {
    app: {
      type: 'stdout',
      layout: {
        type: 'pattern',
        pattern:
          '%[[%d{MM/dd/yyyy hh:mm:ss}] [%p] [%c] [Line %l of %f{3}]%] %n%m%n',
      },
    },
    deemed_summary: {
      flags: 'w',
      type: 'file', // todo: change back to app,
      filename: 'src/scripts/airtable_migration/deemed/summary.log',
      layout: {
        type: 'pattern',
        pattern: '[%d{MM/dd/yyyy hh:mm:ss}] %m',
      },
    },
    deemed_error: {
      flags: 'w',
      type: 'file', // todo: change back to app,
      filename: 'src/scripts/airtable_migration/deemed/error.log',
      layout: {
        type: 'pattern',
        pattern: '%[%m%]',
      },
    },
    deemed_success: {
      flags: 'w',
      type: 'file',
      filename: 'src/scripts/airtable_migration/deemed/success.log',
      layout: {
        type: 'pattern',
        pattern: '%[%m%]',
      },
    },
    custom_summary: {
      flags: 'w',
      type: 'file',
      filename: 'src/scripts/airtable_migration/custom/summary.log',
      layout: {
        type: 'pattern',
        pattern: '[%d{MM/dd/yyyy hh:mm:ss}] %m',
      },
    },
    custom_error: {
      flags: 'w',
      type: 'file',
      filename: 'src/scripts/airtable_migration/custom/error.log',
      layout: {
        type: 'pattern',
        pattern: '%[%m%]',
      },
    },
    custom_success: {
      flags: 'w',
      type: 'file',
      filename: 'src/scripts/airtable_migration/custom/success.log',
      layout: {
        type: 'pattern',
        pattern: '%[%m%]',
      },
    },
  },

  categories: {
    default: defaultLogCategoryConfig,
    OneGraphLogger: defaultLogCategoryConfig,
    SummaryLoggerDeemed: {
      appenders: ['deemed_summary'],
      level: 'info',
    },
    ErrorLoggerDeemed: {
      appenders: ['deemed_error'],
      level: 'error',
    },
    SuccessLoggerDeemed: {
      appenders: ['deemed_success'],
      level: 'info',
    },
    SummaryLoggerCustom: {
      appenders: ['custom_summary'],
      level: 'info',
    },
    ErrorLoggerCustom: {
      appenders: ['custom_error'],
      level: 'error',
    },
    SuccessLoggerCustom: {
      appenders: ['custom_success'],
      level: 'info',
    },
  },
});

export const logger = getLogger('OneGraphLogger');

export const summaryLoggerDeemed = getLogger('SummaryLoggerDeemed');
export const errorLoggerDeemed = getLogger('ErrorLoggerDeemed');
export const successLoggerDeemed = getLogger('SuccessLoggerDeemed');

export const summaryLoggerCustom = getLogger('SummaryLoggerCustom');
export const errorLoggerCustom = getLogger('ErrorLoggerCustom');
export const successLoggerCustom = getLogger('SuccessLoggerCustom');

if (!hubspotAPIKey) {
  logger.error('No env var found for HUBSPOT_API_KEY');
}

export { appConfig };
