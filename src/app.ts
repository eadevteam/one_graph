import 'babel-polyfill';
import cors from 'cors';
import express from 'express';
import http from 'http';
import helmet from 'helmet';
import { ApolloServer } from '@apollo/server';
import { expressMiddleware } from '@apollo/server/express4';
import notFound from './endpoints/not-found';
import calculations from './endpoints/calculations';
import s3Image from './endpoints/s3Images';
import etrm from './endpoints/etrm';
import hubspot from './endpoints/hubspot';
import pgeShareMyDataPublic from './endpoints/pge-sharemydata-public';
import pgeShareMyDataPrivate from './endpoints/pge-sharemydata-private';
import publicCalculations from './endpoints/public-calculations';
import { typeDefs } from './schema';
import {
  buildEnvironment,
  getCorsOrigins,
  logger,
  oneMongoClient,
} from './config';
import { appResolver } from './resolvers/one-resolver';
import oneUserSync from './middleware/one-user-sync';

import { ApolloServerPluginDrainHttpServer } from '@apollo/server/plugin/drainHttpServer';
import { makeExecutableSchema } from '@graphql-tools/schema';
import { WebSocketServer } from 'ws';
import { useServer } from 'graphql-ws/lib/use/ws';
import { applyMiddleware } from 'graphql-middleware';
import { permissions } from './middleware/role-access';
import { 
  countersDatasource, 
  lineItemsDatasource, 
  partnersDatasource, 
  peopleDatasource, 
  portfoliosDatasource, 
  programsDatasource, 
  projectsDatasource, 
  propertiesDatasource, 
  proposalsDatasource, 
  savingsDatasource, 
  shareMyDataQueueDatasource, 
  stagesDatasource, 
  surveysDatasource, 
  tasksDatasource, 
  utilityDataIntegrationsDatasource 
} from './datasources';

/**
 * Set up Express app with helmet for security
 */
const app = express();
app.use(helmet.hsts());
app.use(helmet.frameguard());
app.use(express.json({ limit: '50mb' }));

if (buildEnvironment !== 'local') {
  app.use(
    cors({
      origin: getCorsOrigins(),
    })
  );
} else {
  app.use(cors()); // allow wildcard origin in dev environment
}

/**
 * Connect to Mongo database
 */
oneMongoClient.connect();



const dataSources = {
  people: peopleDatasource,
  projects: projectsDatasource,
  programs: programsDatasource,
  lineItems: lineItemsDatasource,
  savings: savingsDatasource,
  partners: partnersDatasource,
  portfolios: portfoliosDatasource,
  properties: propertiesDatasource,
  surveys: surveysDatasource,
  proposals: proposalsDatasource,
  tasks: tasksDatasource,
  stages: stagesDatasource,
  counters: countersDatasource,
  shareMyDataQueue: shareMyDataQueueDatasource,
  utilityDataIntegrations: utilityDataIntegrationsDatasource,
};

/**
 * Splice GraphQL and REST servers
 */
async function startApolloServer(typeDefs: any, resolvers: any) {
  const loggerPlugin = {
    // Fires whenever a GraphQL request is received from a client.
    async requestDidStart(requestContext: any) {
      console.log(
        'Request started! GraphQL Operation:\n' +
          requestContext.request.operationName
      );

      console.log(requestContext.request.query);
      console.log(requestContext.request.variables);

      return {
        // Fires whenever Apollo Server will parse a GraphQL
        // request to create its associated document AST.
        async parsingDidStart(requestContext: any) {
          console.log('Parsing started!');
        },

        // Fires whenever Apollo Server will validate a
        // request's document AST against your GraphQL schema.
        async validationDidStart(requestContext: any) {
          console.log('Validation started!');
        },
      };
    },
  };

  const httpServer = http.createServer(app);
  const schema = applyMiddleware(
    makeExecutableSchema({ typeDefs, resolvers }),
    permissions
  );

  // Creating the WebSocket server
  const wsServer = new WebSocketServer({
    server: httpServer,
    path: '/graphql',
  });

  // Hand in the schema we just created and have the
  // WebSocketServer start listening.
  const subscriptionServer = useServer(
    {
      schema,
      context: (context) => {
        return {
          context,
          dataSources,
        };
      },
    },
    wsServer
  );

  const server = new ApolloServer({
    schema,
    plugins: [
      ApolloServerPluginDrainHttpServer({ httpServer }),
      // Proper shutdown for the WebSocket server.
      {
        async serverWillStart() {
          return {
            async drainServer() {
              await subscriptionServer.dispose();
            },
          };
        },
      },
      loggerPlugin,
    ],
  });

  await server.start();

  app.use(
    '/graphql',
    oneUserSync,
    expressMiddleware(server, {
      context: async ({ req }) => ({
        user: req.body.user,
        dataSources: dataSources,
      }),
    })
  );

  app.use('/rules/calculations', oneUserSync, calculations);

  app.use('/s3Image', oneUserSync, s3Image);

  app.use('/etrm', oneUserSync, etrm);

  app.use('/hubspot', oneUserSync, hubspot);

  app.use('/pge', pgeShareMyDataPublic); // make sure this route is ddos-proof etc
  app.use('/utility-integration/pge', oneUserSync, pgeShareMyDataPrivate); // make sure this route is ddos-proof etc
  app.use('/freaper-calcs', publicCalculations);

  app.use(notFound);
  app.use((err: any, req: any, res: any, next: any) => {
    logger.error(err);
    res.status(500).json({
      id: 'internal_server_error',
      message: err.toString(),
    });
  });

  const port = process.env.PORT || 9999;
  httpServer.listen(port);
  logger.debug(`One Graph running on port ${port}`);
}

startApolloServer(typeDefs, appResolver);

logger.debug(`Running ${buildEnvironment} environment`);

export default app;
