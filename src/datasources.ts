import { appConfig, oneMongoClient } from './config';
import { Programs } from './datasources/programs-datasource-mongo';
import { LineItems } from './datasources/line-items-datasource-mongo';
import { Projects } from './datasources/projects-datasource-mongo';
import { Counters } from './datasources/counters-datasource-mongo';
import { People } from './datasources/people-datasource-mongo';
import { Partners } from './datasources/partners-datasource-mongo';
import { Portfolios } from './datasources/portfolios-datasource-mongo';
import { Properties } from './datasources/properties-datasource-mongo';
import { Surveys } from './datasources/surveys-datasource-mongo';
import { Proposals } from './datasources/proposals-datasource-mongo';
import { Savings } from './datasources/savings-datasource-mongo';
import { Tasks } from './datasources/task-datasource-mongo';
import { Stages } from './datasources/stage-datasource-mongo';
import { ShareMyDataQueue } from './datasources/sharemydata-queue-datasource-mongo';
import { UtilityDataIntegration } from './datasources/utilitydataintegration-datasource-mongo';

export const peopleDatasource = new People({
  modelOrCollection: oneMongoClient.db(appConfig.database).collection('people'),
});

export const projectsDatasource = new Projects({
  modelOrCollection: oneMongoClient
    .db(appConfig.database)
    .collection('projects'),
});

export const programsDatasource = new Programs({
  modelOrCollection: oneMongoClient
    .db(appConfig.database)
    .collection('programs'),
});

export const lineItemsDatasource = new LineItems({
  modelOrCollection: oneMongoClient
    .db(appConfig.database)
    .collection('line_items'),
});

export const savingsDatasource = new Savings({
  modelOrCollection: oneMongoClient
    .db(appConfig.database)
    .collection('savings'),
});

export const partnersDatasource = new Partners({
  modelOrCollection: oneMongoClient
    .db(appConfig.database)
    .collection('partners'),
});

export const portfoliosDatasource = new Portfolios({
  modelOrCollection: oneMongoClient
    .db(appConfig.database)
    .collection('portfolios'),
});

export const propertiesDatasource = new Properties({
  modelOrCollection: oneMongoClient
    .db(appConfig.database)
    .collection('properties'),
});

export const surveysDatasource = new Surveys({
  modelOrCollection: oneMongoClient
    .db(appConfig.database)
    .collection('surveys'),
});

export const proposalsDatasource = new Proposals({
  modelOrCollection: oneMongoClient
    .db(appConfig.database)
    .collection('proposals'),
});

export const tasksDatasource = new Tasks({
  modelOrCollection: oneMongoClient.db(appConfig.database).collection('tasks'),
});

export const stagesDatasource = new Stages({
  modelOrCollection: oneMongoClient.db(appConfig.database).collection('stages'),
});

export const countersDatasource = new Counters({
  modelOrCollection: oneMongoClient
    .db(appConfig.database)
    .collection('counters'),
});

export const shareMyDataQueueDatasource = new ShareMyDataQueue({
  modelOrCollection: oneMongoClient
    .db(appConfig.database)
    .collection('sharemydata_queue'),
});

export const utilityDataIntegrationsDatasource = new UtilityDataIntegration({
  modelOrCollection: oneMongoClient
    .db(appConfig.database)
    .collection('utility_data_integrations'),
});