import { AirtableBase } from 'airtable/lib/airtable_base';
import { Logger } from 'log4js';
import { Db } from 'mongodb';
import { PeopleDocument } from './datasources/people-datasource-mongo';

export interface ConnectionsConfig {
  databaseURL: string;
  database: string;
  ssl: boolean;
  awsRegion: string;
  awsVersion: string;
}

export interface MigrationArgs {
  source: AirtableBase;
  target: Db;
  projectMappingCallback: Function;
  summaryLogger: Logger;
  errorLogger: Logger;
  successLogger: Logger;
  useTestRecord?: boolean;
}

export interface OneMongoResult {
  success: boolean;
  message: any;
  data: any;
}

export interface OneMongoMigrationResults {
  project: OneMongoResult;
  program: OneMongoResult;
  savings: OneMongoResult;
  line_items: OneMongoResult;
}

export interface UtilityServiceNumber {
  type: string;
  value: string;
}
