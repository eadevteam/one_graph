import {
  CreateSecretCommand,
  DeleteSecretCommand,
  GetSecretValueCommand,
  PutSecretValueCommand,
  SecretsManagerClient,
} from '@aws-sdk/client-secrets-manager';
import { logger } from '../config';

let cachedPGEShareMyDataCertKey = null;

export async function getPGEShareMyDataCertKey() {
  try {
    if (cachedPGEShareMyDataCertKey == null) {
      logger.debug(
        'PGE Share My Data Cert/Key is not initialized, fetching from secret manager');
      let awsSecretsManager = new SecretsManagerClient({ region: 'us-west-2' });
      const keyResponse = await awsSecretsManager.send(
        new GetSecretValueCommand({ SecretId: 'star-ecoact-cert-key' })
      );      
      cachedPGEShareMyDataCertKey = keyResponse.SecretString;
      logger.debug('Cert/Key successfully retrieved');
    }
    return cachedPGEShareMyDataCertKey;

  } catch (e) {
    logger.error('Failed to get key from AWS Secret Manager');
    return null;
  }    
}

export async function getPGEShareMyDataRefreshToken(key: string) {
  try {
    let awsSecretsManager = new SecretsManagerClient({ region: 'us-west-2' });
    const keyResponse = await awsSecretsManager.send(
      new GetSecretValueCommand({ SecretId: key })
    );

    return JSON.parse(keyResponse.SecretString);
  } catch (e) {
    logger.error('Failed to get key from AWS Secret Manager');
    return null;
  }
}

export async function createPGEShareMyDataRefreshToken(
  key: string, 
  refreshToken: string, 
  accessToken: string, 
  expiration: Date, 
  duration: number) {
  try {
    logger.debug("Creating new entry in secrets manager: \n`{\"refresh_token\": \"${refreshToken}\", \"access_token\": \"${accessToken}\", \"access_token_expiration\": \"${expiration}\", \"access_token_duration\": \"${duration}\"}`");

    let awsSecretsManager = new SecretsManagerClient({ region: 'us-west-2' });
       
    const keyResponse = await awsSecretsManager.send(
      new CreateSecretCommand({
         Name: key,
         SecretString: `{\"refresh_token\": \"${refreshToken}\", \"access_token\": \"${accessToken}\", \"access_token_expiration\": \"${expiration.toISOString()}\", \"access_token_duration\": \"${duration}\"}`,
         })
    );

    return;
  } catch (e) {
    logger.error(e);
    logger.error('Failed to create key in AWS Secret Manager');
    return null;
  }
}

export async function updatePGEShareMyDataRefreshToken(
  key: string, 
  refreshToken: string, 
  accessToken: string, 
  expiration: Date, 
  duration: number) {
  try {
    let awsSecretsManager = new SecretsManagerClient({ region: 'us-west-2' });
    const secretString = 
      `{\"refresh_token\": \"${refreshToken}\", \"access_token\": \"${accessToken}\", \"access_token_expiration\": \"${expiration.toISOString()}\", \"access_token_duration\": \"${duration}\"}`;
    logger.debug("updating pge sharemydata token to : " + secretString);
    const keyResponse = await awsSecretsManager.send(
      new PutSecretValueCommand({
         SecretId: key,
         SecretString: secretString,
         })
    );
    return;
  } catch (e) {
    logger.error('Failed to update key in AWS Secret Manager');
    return null;
  }
}

export async function deletePGEShareMyDataRefreshToken(key: string) {
  try {
    let awsSecretsManager = new SecretsManagerClient({ region: 'us-west-2' });
    const keyResponse = await awsSecretsManager.send(
      new DeleteSecretCommand({ SecretId: key })
    );

    return;
  } catch (e) {
    logger.error('Failed to delete key from AWS Secret Manager');
    return null;
  }
}