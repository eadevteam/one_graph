/*
 * rulesFormat - convert case style of object keys.
 * Convert object keys from snake_case, which is the postgress standard,
 * to camelCase which is the OpenL tablets standard.
 */
const rulesFormat = (item: any) => {
  if (typeof item !== 'object' || item === null) {
    return item;
  }
  const formattedObject: any = item;
  Object.keys(formattedObject).forEach((key, index) => {
    if (!Array.isArray(formattedObject[key])) {
      formattedObject[key] = rulesFormat(formattedObject[key]);
    } else {
      formattedObject[key] = formattedObject[key].map((arrayItem: any) =>
        rulesFormat(arrayItem)
      );
    }
  });
  return formattedObject;
};

export { rulesFormat };
