import { InMemoryLRUCache, KeyValueCache } from '@apollo/utils.keyvaluecache'
import { GraphQLError } from 'graphql'
import { Collection as MongoCollection, ObjectId } from 'mongodb'
import {
  Collection as MongooseCollection,
  Document,
  Model as MongooseModel,
} from 'mongoose'
import { isCollectionOrModel, isModel } from './helpers'
import {createCachingMethods} from './cache'

export type Collection<T extends { [key: string]: any }, U = MongoCollection<T>> = T extends Document
  ? MongooseCollection
  : U

export type Model<T, U = MongooseModel<T>> = T extends Document
  ? U
  : undefined

export type ModelOrCollection<T extends { [key: string]: any }, U = Model<T>> = T extends Document
  ? U
  : Collection<T>

export interface Fields {
  [fieldName: string]:
    | string
    | number
    | boolean
    | ObjectId
    | (string | number | boolean | ObjectId)[]
}

export interface Options {
  ttl: number
}

export interface MongoDataSourceConfig<TData extends { [key: string]: any }> {
  modelOrCollection: ModelOrCollection<TData>
  cache?: KeyValueCache<TData>
}

export class MongoDataSource<TData extends { [key: string]: any }> {
  collection: Collection<TData>
  model: Model<TData>

  constructor(options: MongoDataSourceConfig<TData>) {
    if (!isCollectionOrModel(options.modelOrCollection)) {
      throw new GraphQLError(
        'MongoDataSource constructor must be given a collection or Mongoose model'
      )
    }

    if (isModel(options.modelOrCollection)) {
      this.model = options.modelOrCollection as Model<TData>;
      // if (this.model !== undefined) {
      //   this.collection =  this.model.collection;
      // }
    } else {
      this.collection = options.modelOrCollection
    }

    const methods = createCachingMethods({
      collection: this.collection,
      model: this.model,
      cache: options.cache || new InMemoryLRUCache()
    })

    Object.assign(this, methods)
  }

}

function modelOrCollection(modelOrCollection: any) {
  throw new Error('Function not implemented.')
}
