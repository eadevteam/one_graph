import { afterAll, beforeAll, describe, expect, test } from '@jest/globals';
import { transformSharemydataXML } from '../pge_sharemydata_utils';

describe('PGE Sharemydata utils', () => {

  beforeAll(async () => {
  });

  afterAll(async () => {
  });

  test('Transform XML function test', async () => {
    const sampleXML = `<ns1:feed xmlns:ns1="http://www.w3.org/2005/Atom">
        <ns1:entry>
          <ns1:id xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns1:idType">4736fbd1-56a7-4782-891d-9a63175e4675</ns1:id>
          <ns1:link xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns1:linkType" href="https://api.pge.com/GreenButtonConnect/espi/1_1/resource/Authorization" rel="up"/>
          <ns1:link xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns1:linkType" href="https://api.pge.com/GreenButtonConnect/espi/1_1/resource/Authorization/13880574" rel="self"/>
          <ns1:published xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns1:dateTimeType">2024-03-20T18:44:23.576Z</ns1:published>
          <ns1:updated xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns1:dateTimeType">2024-03-20T18:44:23.576Z</ns1:updated>
          <ns1:content xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns1:contentType">
            <ns0:Authorization xmlns:ns0="http://naesb.org/espi">
              <ns0:authorizedPeriod>
                <ns0:duration>1435</ns0:duration>
                <ns0:start>1710887032</ns0:start>
              </ns0:authorizedPeriod>
              <ns0:publishedPeriod>
                <ns0:duration>94725368</ns0:duration>
                <ns0:start>1616192632</ns0:start>
              </ns0:publishedPeriod>
              <ns0:status>0</ns0:status>
              <ns0:expires_at>0</ns0:expires_at>
              <ns0:scope>FB=1_3_8_13_14_18_19_31_32_35_37_38_39_4_15_5_46_47_16;AdditionalScope=USAGE_BASIC_ACCOUNT_PROGRAM ENROLLMENT_BILLING;IntervalDuration=900_3600;BlockDuration=Daily;HistoryLength=94670640;AccountCollection=2;BR=51692;dataCustodianId=PGE;</ns0:scope>
              <ns0:token_type>Bearer</ns0:token_type>
              <ns0:resourceURI>https://api.pge.com/GreenButtonConnect/espi/1_1/resource/Subscription/13880574</ns0:resourceURI>
              <ns0:authorizationURI>https://api.pge.com/GreenButtonConnect/espi/1_1/resource/Authorization/13880574</ns0:authorizationURI>
            </ns0:Authorization>
          </ns1:content>
        </ns1:entry>
      </ns1:feed>`;

    const expectedJSON = {
      Authorization: {
        authorizedPeriod: {
          duration: 1435,
          start: 1710887032,
        },
        publishedPeriod: {
          duration: 94725368,
          start: 1616192632,
        },
        status: 0,
        expires_at: 0,
        scope: 'FB=1_3_8_13_14_18_19_31_32_35_37_38_39_4_15_5_46_47_16;AdditionalScope=USAGE_BASIC_ACCOUNT_PROGRAM ENROLLMENT_BILLING;IntervalDuration=900_3600;BlockDuration=Daily;HistoryLength=94670640;AccountCollection=2;BR=51692;dataCustodianId=PGE;',
        token_type: 'Bearer',
        resourceURI: 'https://api.pge.com/GreenButtonConnect/espi/1_1/resource/Subscription/13880574',
        authorizationURI: 'https://api.pge.com/GreenButtonConnect/espi/1_1/resource/Authorization/13880574',
      }
    }

    const result = transformSharemydataXML(sampleXML);
    expect(result).toStrictEqual(expectedJSON);
  });
});