import { peopleDatasource } from "../datasources";
import { emailBase, logger } from '../config';
import axios from 'axios';

export async function sendTeamsNotification(title: string, content: string) {

  var formatted_Card_Payload = {
    "type": "message",
    "attachments": [
        {
            "contentType": "application/vnd.microsoft.card.adaptive",
            "contentUrl": null,
            "content": {
                "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
                "type": "AdaptiveCard",
                "version": "1.2",
                "body": [
                    {
                        "type": "TextBlock",
                        "size": "medium",
                        "text": `${title}`,
                        "style": "heading",
                        "wrap": true,
                    },
                    {
                      "type": "TextBlock",
                      "text": `${content}`,
                      "wrap": true,
                    }
                ]
            }
        }
    ]
  }

  var webhookUrl = "https://ecoact.webhook.office.com/webhookb2/a45fddfe-9099-4582-89a6-e345d50cc4d8@5d9ddfe6-8f9c-43f6-8304-c663cb07ee28/IncomingWebhook/64f389167e8645579dbcc48967e84530/bda1dc35-596f-49ec-97e5-6adc3d26e1ac";

  axios.post(webhookUrl , formatted_Card_Payload )
  .then(res => {
      console.log(`statusCode: ${res.status}`)
      console.log(res)
  })
  .catch(error => {
      console.error(error)
  })
}