import {
  logger,
  pgeShareMyDataClientId,
  pgeShareMyDataClientSecret,
} from '../config';
import {
  propertiesDatasource,
  utilityDataIntegrationsDatasource,
} from '../datasources';
import https from 'https';
import fs from 'fs';
import axios from 'axios';
import { 
  getPGEShareMyDataCertKey,
  getPGEShareMyDataRefreshToken,
  updatePGEShareMyDataRefreshToken,
} from './aws_secrets_manager_utils';
import { compareAsc, addSeconds } from 'date-fns';
import { XMLParser } from 'fast-xml-parser';

// Call the token endpoint with the ID
// This will return a new access_token (use for data call) and refresh_token (overwrite original in db)
// If an error occurs in PGEs endpoint, we do NOT overwrite the property in the hope that the old tokens
// remain current and valid.
export async function getShareMyDataAuthTokens(retailCustomerId) {
  let mostCurrentAccessToken;
  let mostCurrentRefreshToken;

  try {
    const tokenData = await getPGEShareMyDataRefreshToken(retailCustomerId);

    logger.debug(
      `Token retreived from secret manager: ${JSON.stringify(tokenData)}`
    );
    mostCurrentAccessToken = tokenData.access_token;
    mostCurrentRefreshToken = tokenData.refresh_token;
    let tokenExpirationDate = new Date(tokenData.access_token_expiration);
    const tokenDuration = tokenData.access_token_duration;

    let newTokens;
    if (compareAsc(new Date(Date.now()), tokenExpirationDate) > 0) {
      logger.debug('Getting new tokens from PGE');
      try {
        newTokens = await getNewCustomerAuthTokenPair(mostCurrentRefreshToken);
      } catch (e) {
        logger.error(
          'Error requesting new tokens from PGE endpoint, dumping OLD tokens below which should still be valid. NOT modifiying token data.'
        );
        logger.info('Refresh Token: ' + mostCurrentRefreshToken);
        logger.info('Access Token: ' + mostCurrentAccessToken);
        throw e;
      }

      if (newTokens) {
        logger.debug(
          'Attempting to update the secret manager with NEW tokens: '
        );
        logger.debug(newTokens['access_token']);
        logger.debug(newTokens['refresh_token']);
        logger.debug(tokenDuration);

        await updatePGEShareMyDataRefreshToken(
          retailCustomerId,
          newTokens['refresh_token'],
          newTokens['access_token'],
          addSeconds(Date.now(), tokenDuration),
          tokenDuration
        );
        // Returns the tokens for immediate use, but they are also stored above
      } else {
        logger.info(
          'newTokenRetrievalResult is null, did not update secret manager'
        );
      }
    } else {
      newTokens = {
        access_token: mostCurrentAccessToken,
        refresh_token: mostCurrentRefreshToken,
      };
    }
    return newTokens;
  } catch (e) {
    logger.error('Error syncing the auth tokens to secret manager');
    // return null;
  }
}

async function getNewCustomerAuthTokenPair(existingRefreshToken) {
  try {
    const pgeShareMyClientBasicToken = Buffer.from(
      `${pgeShareMyDataClientId}:${pgeShareMyDataClientSecret}`
    ).toString('base64');

    const key = await getPGEShareMyDataCertKey();
    const authTokenResult = await axios.post(
      `https://api.pge.com/datacustodian/oauth/v2/token?grant_type=refresh_token&refresh_token=${existingRefreshToken}`,
      {},
      {
        headers: {
          Authorization: `Basic ${pgeShareMyClientBasicToken}`,
        },
        httpsAgent: new https.Agent({
          cert: fs.readFileSync('./*.ecoact.org.pem'),
          key: key,
        }),
      }
    );
    return authTokenResult.data;
  } catch (e) {
    logger.error('Error getting a new auth token pair from PGE', e);
    return null;
  }
}

// Helper function to quickly auth with PGE's ShareMyData endpoints
// with all the necessary components (Usage & Customer data requests only, not for retrieving new tokens)
export async function sendUsageDataRequest(url: string, accessToken: string) {
  try {
    const key = await getPGEShareMyDataCertKey();

    const result = await axios.get(url, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
      httpsAgent: new https.Agent({
        cert: fs.readFileSync('./*.ecoact.org.pem'),
        key: key,
      }),
    });

    return result;
  } catch (e) {
    logger.error('Error sending get request to ' + url, e);
    throw e;
  }
}

// helper function that takes the url from a pge notification call and
// determins what type it is and fetches the appropriate data.
// SP3: currently only supports authorization revocation.
export async function getNotificationInformation(notificatoinUrl: string) {

  const urlParts = notificatoinUrl.split('/');
  const startIndex = urlParts.indexOf('resource');
  if (startIndex > 0) {
    switch (urlParts[startIndex + 1]) {
      case 'Authorization' : {
        const customerId = urlParts[startIndex + 2];
        logger.debug("found Authorization revocation for customerId " + customerId);
        try {
          const clientAccessToken = await getClientAccessToken();
          const key = await getPGEShareMyDataCertKey();
          const authInfoRequest = await axios.get(
            notificatoinUrl,
            {
              headers: {
                Authorization: `Bearer ${clientAccessToken}`,
              },
              httpsAgent: new https.Agent({
                cert: fs.readFileSync('./*.ecoact.org.pem'),
                key: key,
              }),
            }
          );
          return transformSharemydataXML(authInfoRequest.data);
        } catch (e) {
          logger.error('Error getting a new client access token from PGE', e);
          return null;
        }

      }
      default: {
        logger.debug("getNotifictionInformation url doesn't handle " + notificatoinUrl);
      }
    }
  }

}

export async function getClientAccessToken() {
  try {
    const pgeShareMyClientBasicToken = Buffer.from(
      `${pgeShareMyDataClientId}:${pgeShareMyDataClientSecret}`
    ).toString('base64');

    const key = await getPGEShareMyDataCertKey();
    const clientAccessTokenResult = await axios.post(
      `https://api.pge.com/datacustodian/oauth/v2/token?grant_type=client_credentials`,
      {},
      {
        headers: {
          Authorization: `Basic ${pgeShareMyClientBasicToken}`,
        },
        httpsAgent: new https.Agent({
          cert: fs.readFileSync('./*.ecoact.org.pem'),
          key: key,
        }),
      }
    );
    return clientAccessTokenResult.data.client_access_token;
  } catch (e) {
    logger.error('Error getting a new client access token from PGE', e);
    return null;
  }
}

// helper function for transforming the xml response from sharemydata
// requests into more useful json.  Current approach is to take each
// content section and turn it into a property with it's subsequent
// properties with values underneath
export function transformSharemydataXML(data: any) {
  let retVal: Record<string,any> = {};
  const xmlParser = new XMLParser({
    ignoreAttributes: false,
    attributeNamePrefix: '@_',
  });

  const xmlJSON = xmlParser.parse(data);
  
  //feeds
  Object.entries(xmlJSON['ns1:feed']).forEach(([feedKey, feedValue]) => {
    // entries
      // find the content node
    if (feedKey === 'ns1:entry') {
      const content = feedValue['ns1:content'];
      Object.entries(content).forEach(([itemKey, itemValue]) => {
        if (!itemKey.startsWith('@_')) {
          const name = itemKey.split(':')[1];
          const subObj: Record<string, any> = {};
          Object.entries(itemValue).forEach(([propKey, propValue]) => {
            if (!propKey.startsWith('@_')) {
              // properties and values
              const propertyName = propKey.split(':')[1];
              // value or object
              if (typeof propValue === 'object') {
                let subProperty: Record<string, any> = {};
                // then it must be an object with properties.  One more dive down.
                Object.entries(propValue).forEach(([subPropKey, subPropValue]) => {
                  subProperty[subPropKey.split(':')[1]] = subPropValue;
                });
                subObj[propertyName] = subProperty;
              } else {
                subObj[propertyName] = propValue;
              }
            }
          });
          retVal[name] = subObj;
        }
      });
    }
  });

  return retVal;
}