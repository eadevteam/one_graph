/* tslint:disable */

const sampleRuntime: any = {
  currentDate: '2018-07-31T22:49:39.066Z',
  requestDate: '2018-07-31T22:49:39.067Z',
  lob: 'string',
  nature: 'string',
  usState: 'AL',
  country: 'AL',
  usRegion: 'MW',
  currency: 'ALL',
  lang: 'ALB',
  region: 'NCSA',
  caProvince: 'AB',
  caRegion: 'QC'
};

export default sampleRuntime;
