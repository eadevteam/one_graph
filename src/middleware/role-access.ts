import { rule, shield, or } from 'graphql-shield';

const isReader = rule()(async (parent, args, ctx, info) => {
  return ctx.user.role === 'one-resource-server/readonly';
});

const isTaperDefault = rule()(async (parent, args, ctx, info) => {
  var check = ['Basic'].includes(ctx.user.role);
  return check;
});

const isAdmin = rule()(async (parent, args, ctx, info) => {
  var check = ['Admin'].includes(ctx.user.role);
  return check;
});

const permissions = shield(
  {
    Query: {
      '*': or(isReader, isTaperDefault, isAdmin),
      shareMyDataQueue: isAdmin,
    },
    Mutation: {
      '*': or(isTaperDefault, isAdmin),
    },
  },
  {
    allowExternalErrors: true,
    debug: true,
  }
);

export { permissions };
