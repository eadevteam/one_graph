import { MongoClient, ObjectId } from 'mongodb';
import { networkInterfaces } from 'os';
import { appConfig, logger, oneMongoClient, onedatabase } from '../config';

const oneUserSync = async (request: any, response: any, next: any) => {
  const jwtUser = request.body.user || {};
  if (jwtUser.cognito_uuid) {
       logger.debug('Usersync happening with user ' + JSON.stringify(jwtUser));
    try {
      await oneMongoClient.connect();
      const collection = oneMongoClient.db(onedatabase).collection('people');

        const now = new Date(Date.now());
        // check for one by email in case they already exist
        let user = await collection.findOneAndUpdate(
          { email: jwtUser.email },
          {
            $set: {
              last_name: jwtUser.last_name,
              first_name: jwtUser.first_name,
              email: jwtUser.email,
              company: jwtUser.company,
              cognito_uuid: jwtUser.cognito_uuid,
              role: jwtUser.role,
              phone_number: jwtUser.phone_number,
              time_created: now,
              time_modified: now,
            },
          },
          {
            upsert: true,
            returnDocument: 'after',
          }
        );
        logger.debug('User being set to ' + JSON.stringify(user));
        request.body.user = user.value;
    } finally {
      //   client.close();
    }
  } else {
    // This is an api call with no user, just a role.
    request.body.user = {role: jwtUser.scope};
  }
  next();
};

export default oneUserSync;
