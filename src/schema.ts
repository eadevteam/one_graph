import { gql } from 'graphql-tag';

export const typeDefs = gql`
  scalar Date
  scalar JSON

  type Person {
    _id: ID
    last_name: String
    first_name: String
    email: String!
    company: String
    cognito_uuid: String
    role: String
    bookmarks: JSON
    phone_number: String
    time_created: Date
    time_last_modified: Date
  }

  type PersonResponse {
    success: Boolean!
    message: String
    data: [Person]
  }

  input PersonInput {
    last_name: String
    first_name: String
    email: String!
    company: String
    cognito_uuid: String
    role: String
    bookmarks: JSON
    phone_number: String
  }

  type Program {
    _id: ID
    time_created: Date
    time_last_modified: Date
    name: String!
    line_items: [LineItem]
    savings: [Savings]
    fields: JSON
  }

  type ProgramResponse {
    success: Boolean!
    message: String
    data: [Program]
  }

  input ProgramInput {
    name: String!
    line_items: [ID]
    savings: [ID]
    fields: JSON
  }

  type Savings {
    _id: ID
    name: String
    time_created: Date
    time_last_modified: Date
    methodology: String
    line_items: [LineItem]
    fields: JSON
    isDraft: Boolean
  }

  type SavingsResponse {
    success: Boolean!
    message: String
    data: [Savings]
  }

  input SavingsInput {
    name: String
    methodology: String
    line_items: [ID]
    fields: JSON
    isDraft: Boolean
  }

  type LineItem {
    _id: ID
    time_created: Date
    time_last_modified: Date
    name: String
    fields: JSON
  }
  #   measure_id: String!
  #   pre_committed_above_ex_kw_savings: Float
  #   pre_committed_above_code_kw_savings: Float
  #   indirect_kw_savings: Float
  #   pre_committed_above_ex_kwh_savings: Float
  #   pre_committed_above_code_kwh_savings: Float
  #   indirect_kwh_savings: Float
  #   pre_committed_above_ex_therm_savings: Float
  #   pre_committed_above_code_therm_savings: Float
  #   indirect_therm_savings: Float
  #   total_vendor_rebate_amount: Float
  #   full_measure_cost: Float
  #   incremental_measure_cost: Float
  #   material_cost: Float
  #   installation_labor_cost: Float
  #   cmpa_project_description: String
  #   measure_application_type: String!
  #   requested_quantity_of_equipment: Int!
  #   measure_comments: String
  # }

  type LineItemResponse {
    success: Boolean!
    message: String
    data: [LineItem]
  }

  input LineItemInput {
    name: String
    fields: JSON
  }
  # measure_id: String!
  # pre_committed_above_ex_kw_savings: Float
  # pre_committed_above_code_kw_savings: Float
  # indirect_kw_savings: Float
  # pre_committed_above_ex_kwh_savings: Float
  # pre_committed_above_code_kwh_savings: Float
  # indirect_kwh_savings: Float
  # pre_committed_above_ex_therm_savings: Float
  # pre_committed_above_code_therm_savings: Float
  # indirect_therm_savings: Float
  # total_vendor_rebate_amount: Float
  # full_measure_cost: Float
  # incremental_measure_cost: Float
  # material_cost: Float
  # installation_labor_cost: Float
  # cmpa_project_description: String
  # measure_application_type: String!
  # requested_quantity_of_equipment: Int!
  # measure_comments: String

  type Project {
    _id: ID
    number: Int
    name: String
    time_created: Date
    time_last_modified: Date
    current_stage: Stage
    workflow: [Stage]
    description: String
    creator: Person
    contact: Person
    partners: [Partner]
    manager: Person
    property: Property
    programs: [Program]
    notes: [JSON]
  }

  type ProjectResponse {
    success: Boolean!
    message: String
    data: [Project]
  }

  input ProjectInput {
    number: Int
    name: String
    workflow: [ID]
    current_stage: ID
    description: String
    contact: ID
    manager: ID
    partners: [ID]
    property: ID
    programs: [ID]
    notes: [JSON]
  }

  input BulkImportProjectInput {
    number: Int
    name: String
    stage: String
    property: PropertyInput
    program: ProgramInput
  }

  type Partner {
    _id: ID
    name: String!
    time_created: Date
    time_last_modified: Date
    people: [Person]
  }

  type PartnerResponse {
    success: Boolean!
    message: String
    data: [Partner]
  }

  input PartnerInput {
    name: String
    people: [ID]
  }

  type Portfolio {
    _id: ID
    name: String!
    projects: [Project]
    time_created: Date
    time_last_modified: Date
    notes: [JSON]
  }

  type PortfolioResponse {
    success: Boolean!
    message: String
    data: [Portfolio]
  }

  input PortfolioInput {
    name: String
    projects: [ID]
    notes: [JSON]
  }

  type Subscription {
    portfolioCreated: Portfolio
  }

  type Property {
    _id: ID
    store_number: Int
    company: JSON
    banner: String
    utility_service_numbers: [JSON]
    address_1: String
    address_2: String
    city: String
    state: String
    zip_code: Int
    time_created: Date
    time_last_modified: Date
    contact: Person
    surveys: [Survey]
    utility_data_integrations: [UtilityDataIntegration]
  }

  type PropertyResponse {
    success: Boolean!
    message: String
    data: [Property]
  }

  input PropertyInput {
    store_number: Int
    company: JSON
    banner: String
    utility_service_numbers: [JSON]
    address_1: String
    address_2: String
    city: String
    state: String
    zip_code: Int
    time_created: Date
    time_last_modified: Date
    contact: ID
    surveys: [ID]
    utility_data_integrations: [ID]
  }

  input PropertySearchInput {
    company: JSON
    banner: String
    utility_service_numbers: [JSON]
    address_1: String
    address_2: String
    city: String
    state: String
    zip_code: Int
  }

  type UtilityDataIntegration {
    _id: ID
    retail_customer_id: String
    provider: String
    type: String
    meter_association_ids: [JSON]
    time_created: Date
    time_last_modified: Date
  }

  type Survey {
    _id: ID
    type: String
    fields: JSON
    time_created: Date
    time_last_modified: Date
  }

  type SurveyResponse {
    success: Boolean!
    message: String
    data: [Survey]
  }

  input SurveyInput {
    type: String
    fields: JSON
  }

  type Proposal {
    _id: ID
    name: String!
    time_created: Date
    time_last_modified: Date
  }

  type ProposalResponse {
    success: Boolean!
    message: String
    data: [Proposal]
  }

  input ProposalInput {
    name: String
  }

  type Stage {
    _id: ID
    name: String!
    time_created: Date
    time_last_modified: Date
    start: Date
    completed: Date
    tasks: [Task]
  }

  type StageResponse {
    success: Boolean!
    message: String
    data: [Stage]
  }

  input StageInput {
    name: String!
    start: Date
    completed: Date
    tasks: [ID]
  }

  type Task {
    _id: ID
    name: String!
    time_created: Date
    time_last_modified: Date
    start: Date
    end: Date
    completed: Date
    cost: JSON
    description: String
    parent: Task
  }

  type TaskResponse {
    success: Boolean!
    message: String
    data: [Task]
  }

  input TaskInput {
    name: String!
    start: Date
    end: Date
    completed: Date
    cost: JSON
    description: String
    parent: ID
  }

  type ShareMyDataQueueItem {
    _id: ID
    type: String!
    email: String
    time_created: Date
    access_token: String!
    refresh_token: String!
    resource_URIs: [String]
    status: String
    access_token_duration: Int
    retail_customer_id: String
  }

  type ShareMyDataQueueItemResponse {
    success: Boolean!
    message: String
    data: [ShareMyDataQueueItem]
  }

  input ShareMyDataQueueItemInput {
    type: String!
    email: String
    access_token: String!
    refresh_token: String!
    resource_URIs: [String]
    time_created: Date
    scope: String
    status: String
  }

  type Query {
    person(id: ID!): PersonResponse
    personByCognitoUUID(cognito_uuid: String!): PersonResponse
    people: PersonResponse
    project(id: ID!): ProjectResponse
    projects: ProjectResponse
    projectsByPerson(id: ID!): ProjectResponse
    projectsByProgram(programName: String): ProjectResponse
    partner(id: ID!): PartnerResponse
    partners: PartnerResponse
    portfolio(id: ID!): PortfolioResponse
    portfolios: PortfolioResponse
    program(id: ID!): ProgramResponse
    programs: ProgramResponse
    property(id: ID!): PropertyResponse
    properties(ids: [ID], searchTerm: String): PropertyResponse
    findProperties(filter: PropertySearchInput): PropertyResponse
    survey(id: ID!): SurveyResponse
    surveysByProperty(id: ID!): SurveyResponse
    proposal(id: ID!): ProposalResponse
    proposals: ProposalResponse
    lineItem(id: ID!): LineItemResponse
    lineItems: LineItemResponse
    savings(id: ID, isDraft: Boolean): SavingsResponse
    task(id: ID!): TaskResponse
    tasks: TaskResponse
    stage(id: ID!): StageResponse
    shareMyDataQueue: ShareMyDataQueueItemResponse
    utilityDataIntegration(id: ID!): UtilityDataIntegration
  }

  type Mutation {
    addPerson(person: PersonInput): PersonResponse
    updatePerson(id: ID, person: PersonInput): PersonResponse
    deletePerson(id: ID!): PersonResponse
    addProject(project: ProjectInput): ProjectResponse
    # applyIncrementedProjectNumberToAllProjects: String
    updateProject(id: ID!, project: ProjectInput): ProjectResponse
    deleteProject(id: ID!): ProjectResponse
    addPartner(partner: PartnerInput): PartnerResponse
    updatePartner(id: ID!, partner: PartnerInput): PartnerResponse
    deleteProgram(id: ID!): ProgramResponse
    addProgram(program: ProgramInput): ProgramResponse
    updateProgram(id: ID!, program: ProgramInput): ProgramResponse
    deletePartner(id: ID!): PartnerResponse
    addPortfolio(portfolio: PortfolioInput): PortfolioResponse
    updatePortfolio(id: ID!, portfolio: PortfolioInput): PortfolioResponse
    importPortfolio(
      name: String
      projects: [BulkImportProjectInput]
    ): PortfolioResponse
    addToPortfolio(id: ID!, projects: [ID]): PortfolioResponse
    deleteFromPortfolio(id: ID!, projects: [ID]): PortfolioResponse
    deletePortfolio(id: ID!): PortfolioResponse
    addProperty(id: ID, property: PropertyInput): PropertyResponse
    updateProperty(id: ID!, property: PropertyInput): PropertyResponse
    deleteProperty(id: ID!): PropertyResponse
    addSurvey(id: ID, survey: SurveyInput): SurveyResponse
    updateSurvey(id: ID!, survey: SurveyInput): SurveyResponse
    addProposal(id: ID, proposal: ProposalInput): ProposalResponse
    updateProposal(id: ID!, proposal: ProposalInput): ProposalResponse
    deleteProposal(id: ID!): ProposalResponse
    updateLineItem(id: ID!, lineItem: LineItemInput): LineItemResponse
    deleteLineItem(id: ID!): LineItemResponse
    addLineItem(lineItem: LineItemInput): LineItemResponse
    addSavings(savings: SavingsInput): SavingsResponse
    deleteSavings(id: ID!): SavingsResponse
    updateSavings(id: ID!, savings: SavingsInput): SavingsResponse
    addTask(task: TaskInput): TaskResponse
    updateTask(id: ID!, task: TaskInput): TaskResponse
    deleteTask(id: ID!): TaskResponse
    addStage(stage: StageInput): StageResponse
    updateStage(id: ID!, stage: StageInput): StageResponse
    deleteStage(id: ID!): StageResponse
    deleteShareMyDataQueueItem(id: ID!): ShareMyDataQueueItemResponse
  }
`;
