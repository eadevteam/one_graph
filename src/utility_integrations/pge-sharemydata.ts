import {
  amiDataServiceBase,
  appConfig,
  logger,
  oneMongoClient,
  pgeShareMyDataClientId,
  pgeShareMyDataClientSecret,
} from '../config';
import {
  SecretsManagerClient,
  GetSecretValueCommand,
} from '@aws-sdk/client-secrets-manager';
import axios, { isAxiosError } from 'axios';
import https from 'https';
import fs from 'fs';
import {
  propertiesDatasource,
  shareMyDataQueueDatasource,
  utilityDataIntegrationsDatasource,
} from '../datasources';
import { addDays, differenceInDays, compareAsc, add } from 'date-fns';
import { XMLParser } from 'fast-xml-parser';
import {
  createPGEShareMyDataRefreshToken,
  getPGEShareMyDataCertKey
} from '../utils/aws_secrets_manager_utils';
import { 
  getNotificationInformation, 
  getShareMyDataAuthTokens, 
  sendUsageDataRequest } from '../utils/pge_sharemydata_utils';
import { uomMap } from '../utils/green_button_maps';
import { 
  ShareMyDataQueue, 
  NotificationQueueItem, 
  AuthQueueItem
} from '../datasources/sharemydata-queue-datasource-mongo';
import { sendTeamsNotification } from '../utils/notification_utils';

export class PGEShareMyDataIntegration {
 
  async approveIntegration(queueItemId: any) {
    let retVal = {};
    try {
      // Call the token endpoint with the ID
      // This will return a new access_token (use for data call) and refresh_token (overwrite original in db)
      // TODO(ant): Fire off the actual data request call to PGE ShareMyData
      // Splice the utility data into associated SAIDs/utility number/UsagePointId

      const pgeShareMyAuth = Buffer.from(
        `${pgeShareMyDataClientId}:${pgeShareMyDataClientSecret}`
      ).toString('base64');

      const headers = {
        Authorization: `Basic ${pgeShareMyAuth}`,
      };

      let awsSecretsManager = new SecretsManagerClient({ region: 'us-west-2' });
      let key;
      try {
        const keyResponse = await awsSecretsManager.send(
          new GetSecretValueCommand({ SecretId: 'star-ecoact-cert-key' })
        );

        key = keyResponse.SecretString;
      } catch (e) {
        logger.error('Failed to get key from secret manager');
        throw e;
      }

      const axiosConfig = {
        headers: headers,
        httpsAgent: new https.Agent({
          cert: fs.readFileSync('./*.ecoact.org.pem'),
          key: key,
        }),
      };

      const queueItemResult = await shareMyDataQueueDatasource.getQueueItem(
        queueItemId
      );

      logger.debug(queueItemResult);
      logger.debug(`${queueItemResult.data[0].refresh_token}`);

      const queueItem = queueItemResult.data[0];
      let accessToken = null;
      let refreshToken = null;
      let tokenExpirationDate = add(new Date(queueItem.time_last_modified), {
        seconds: queueItem.access_token_duration,
      });
      let updatedQueueItem;
      if (compareAsc(new Date(Date.now()), tokenExpirationDate) > 0) {
        // Using the latest refresh_token, get a new access_token + refresh_token pair. I know, it's weird. PGE.
        const pgeShareMyDataAccessTokenResult = await axios.post(
          `https://api.pge.com/datacustodian/oauth/v2/token?grant_type=refresh_token&refresh_token=${queueItem.refresh_token}`,
          {},
          axiosConfig
        );

        try {
          logger.debug('NEW TOKENS RESULT: ' + pgeShareMyDataAccessTokenResult);
        } catch (e) {
          logger.error(' Token log failed but continuing on ' + e);
        }
        if (pgeShareMyDataAccessTokenResult.data) {
          accessToken = pgeShareMyDataAccessTokenResult.data.access_token;
          refreshToken = pgeShareMyDataAccessTokenResult.data.refresh_token;
          tokenExpirationDate = add(new Date(Date.now()), {
            seconds: queueItem.access_token_duration,
          });
          updatedQueueItem = await shareMyDataQueueDatasource.updateQueueItem(
            queueItemId,
            {
              status: 'inflight',
              access_token: accessToken,
              refresh_token: refreshToken,
            }
          );
        }
      } else {
        accessToken = queueItem.access_token;
        refreshToken = queueItem.refresh_token;
      }

      if (accessToken) {
        const headersGetData = {
          Authorization: `Bearer ${accessToken}`,
        };

        const axiosConfigGetData = {
          headers: headersGetData,
          httpsAgent: new https.Agent({
            cert: fs.readFileSync('./*.ecoact.org.pem'),
            key: key,
          }),
        };

        const pgeShareMyDataGetCustomerDataResult = await axios.get(
          `https://api.pge.com/GreenButtonConnect/espi/1_1/resource/Batch/RetailCustomer/${queueItem.retail_customer_id}`,
          axiosConfigGetData
        );

        if (pgeShareMyDataGetCustomerDataResult.data) {
          // Find info to tie to property.
          // Find related property and add to utility_data_integrations
          // change status of queue item.
          let utilityDataIntegration = {
            provider: 'pge',
            type: 'electric',
            retail_customer_id: queueItem.retail_customer_id,
            meter_association_ids: [],
          };
          let properties = [];
          const xmlParser = new XMLParser({
            ignoreAttributes: false,
            attributeNamePrefix: '@_',
          });

          const jsonCustomerData = xmlParser.parse(
            pgeShareMyDataGetCustomerDataResult.data
          );
          try {
            logger.debug(jsonCustomerData);
            // create a pge utility data integration object

            // Find number of ns2:entry tags (there will be 5 entries per 1 SAID/CustomerAgreement number)
            const numberOfUniqueMeters =
              jsonCustomerData['ns1:feed']['ns2:entry'].length / 5;
            var customerAgreementId;

            for (let i = 0; i < numberOfUniqueMeters; i++) {
              const usagePointAndSAIDEntry =
                jsonCustomerData['ns1:feed']['ns2:entry'][i * 5 + 3];
              const addressEntry =
                jsonCustomerData['ns1:feed']['ns2:entry'][i * 5 + 1];
              const customerLocation =
                addressEntry['ns2:content']['ServiceLocation']['mainAddress'];

              const splitLink =
                usagePointAndSAIDEntry['ns2:link'][1]['@_href'].split('/');

              const usagePointId = splitLink[splitLink.length - 1];
              customerAgreementId =
                usagePointAndSAIDEntry['ns2:content']['CustomerAgreement'][
                  'name'
                ];

              const street = customerLocation['streetDetail']['addressGeneral'];
              const zip_code = customerLocation['townDetail']['code'];
              const city = customerLocation['townDetail']['name'];
              const state = customerLocation['townDetail']['stateOrProvince'];

              logger.debug(
                `Address we are matching is ${street} ${city} ${state} ${zip_code}`
              );

              const matchResult = await propertiesDatasource.matchProperties({
                address_1: street,
                city: city,
                state: state,
                zip_code: zip_code,
              });

              logger.debug(matchResult);

              if (matchResult.data != null && matchResult.data.length > 0) {
                utilityDataIntegration.meter_association_ids.push({
                  customer_agreement_id: `${customerAgreementId}`,
                  usage_point_id: `${usagePointId}`,
                  property_id: matchResult.data[0]._id,
                });
                properties.push(matchResult.data[0]);
                logger.debug('Updating property ' + i);
              } else {
                const addPropertyResult =
                  await propertiesDatasource.addProperty({
                    address_1: street,
                    city: city,
                    state: state,
                    zip_code: zip_code,
                  });
                utilityDataIntegration.meter_association_ids.push({
                  customer_agreement_id: `${customerAgreementId}`,
                  usage_point_id: `${usagePointId}`,
                  property_id: addPropertyResult.data[0]._id,
                });
                properties.push(addPropertyResult.data[0]);
              }
            }
            // now create and save the utility integration
            const addUtilityDataIntResult =
              await utilityDataIntegrationsDatasource.addUtilityDataIntegration(
                utilityDataIntegration
              );
            // save tokens off to key manager
            await createPGEShareMyDataRefreshToken(
              queueItem.retail_customer_id.toString(),
              refreshToken,
              accessToken,
              tokenExpirationDate,
              queueItem.access_token_duration
            );

            properties.forEach(async (value) => {
              const udiId = addUtilityDataIntResult.data[0]._id;

              const updatePropertyResult =
                await propertiesDatasource.updateProperty(value._id, {
                  ...value,
                  utility_data_integrations:
                    value.utility_data_integrations != null
                      ? [
                          ...value.utility_data_integrations,
                          addUtilityDataIntResult.data[0]._id,
                        ]
                      : [addUtilityDataIntResult.data[0]._id],
                });
              logger.debug('done updating a property');
            });
          } catch (e) {
            logger.error(e);
            throw new Error(
              'Failed to match or create a property for this authorization'
            );
          }

          // update queue item to be 'integrated'.  Later remove completely.
          const queueChangeResults =
            await shareMyDataQueueDatasource.updateQueueItem(queueItemId, {
              ...updatedQueueItem,
              status: 'integrated',
            });
          logger.debug('Queue Changed: ' + JSON.stringify(queueChangeResults));

          retVal ={
            message: 'success',
            data: pgeShareMyDataGetCustomerDataResult.data,
          };
        } else {
          throw new Error('Failed to get Customer Data');
        }
      } else {
        throw new Error('Failed to get token from PGE');
      }
    } catch (e) {
      if (isAxiosError(e)) {
        logger.error(e.response.status);
        logger.error(
          'Axios Request Headers: ' + JSON.stringify(e.request.headers)
        );
        logger.error(e.response.data);
        retVal = {
          status: e.response.status,
          requestHeaders: e.request.headers,
          data: e.response.data,
        };
      } else {
        logger.error(e);
        retVal = { status: '500', message: 'Error caught', error: e };
      }
    }
    return retVal;
  };

  async getUsageInterval(
    usagePointId: any, 
    intervalStart: any, 
    intervalEnd: any, 
    retailCustomerId: any) {
    
    let retVal = {};

    try {  
      const syncedToken = await getShareMyDataAuthTokens(retailCustomerId);

      /** We can only fetch 2 months at a time due to PGE API constraints.  Going to default
       * to a years worth of data so do 6 fetches, starting with today and
       * going backwards in time. */
      let allReadings = [];
      let partialFailInfo = '';
      const intervalStartDate = new Date(intervalStart as string);
      const intervalEndDate = new Date(intervalEnd as string);
      let start = intervalStartDate;
      let end = intervalEndDate;

      const numLoops = Math.floor(differenceInDays(end, start) / 58) + 1;

      for (let i = 0; i < numLoops; i++) {
        end = i == numLoops - 1 ? intervalEndDate : addDays(start, 58);

        try {
          const pgeShareMyDataGetUsageData = await sendUsageDataRequest(
            `https://api.pge.com/GreenButtonConnect/espi/1_1/resource/Batch/Subscription/${retailCustomerId}/UsagePoint/${usagePointId}?published-min=${
              start.toISOString().split('.')[0]
            }Z&published-max=${end.toISOString().split('.')[0]}Z`,
            syncedToken['access_token']
          );
          // Increment cursor
          start = end;

          if (pgeShareMyDataGetUsageData.data) {
            const xmlParser = new XMLParser({
              ignoreAttributes: false,
              attributeNamePrefix: '@_',
            });

            const jsonUsageData = xmlParser.parse(
              pgeShareMyDataGetUsageData.data
            );
            /**
             *   Interval blocks have 3 useless entries before and one after.
             */
            const numEntries = jsonUsageData['ns1:feed']['ns1:entry'].length;
            var uom: string = '';
            var powerOfTenMultiplier = 0;

            for (let j = 0; j < numEntries - 1; j++) {
              const title: string = jsonUsageData['ns1:feed']['ns1:entry'][j][
                'ns1:title'
              ]['#text'] as string;

              if (title === 'Type of Meter Reading Data') {
                uom =
                  uomMap[
                    jsonUsageData['ns1:feed']['ns1:entry'][j]['ns1:content'][
                      'ns0:ReadingType'
                    ]['ns0:uom']
                  ];
                powerOfTenMultiplier =
                  jsonUsageData['ns1:feed']['ns1:entry'][j]['ns1:content'][
                    'ns0:ReadingType'
                  ]['ns0:powerOfTenMultiplier'];
              } else if (title.startsWith('IntervalBlock')) {
                const jsonIntervals =
                  jsonUsageData['ns1:feed']['ns1:entry'][j]['ns1:content'][
                    'ns0:IntervalBlock'
                  ];

                let readings;
                if (jsonIntervals) {
                  readings = (
                    jsonIntervals['ns0:IntervalReading'] as Array<any>
                  ).map((item) => ({
                    value:
                      parseInt(item['ns0:value']) * 10 ** powerOfTenMultiplier, // Watt-Hour for a 15min interval
                    // quality: item['ns0:ReadingQuality']['ns0:quality'],
                    start: item['ns0:timePeriod']['ns0:start'],
                    // duration: item['ns0:timePeriod']['ns0:duration'],
                  }));
                  allReadings = allReadings.concat(readings);
                }
              }
            }
          } else {
            logger.error(
              `Failed to get usage data for ${usagePointId} from ${end} to ${start}`
            );
          }
        } catch (error) {
          if (allReadings.length) {
            // failed on a chunk, if we have some,let's return it and pass on the
            // message.
            if (isAxiosError(error)) {
              partialFailInfo = error.response.data;
            }
            break;
          } else {
            throw error;
          }
        }
      }

      retVal = {
        message: 'success',
        data: {
          uom: uom,
          readings: allReadings,
          partialFailInfo: partialFailInfo,
        },
      };
    } catch (e) {
      if (isAxiosError(e)) {
        logger.error(e.response.status);
        logger.error(e.response.data);
        retVal = {
          status: e.response.status,
          requestHeaders: e.request.headers,
          data: e.response.data,
        };
      } else {
        logger.error(e);
        retVal = { status: '500', message: 'Error caught', error: e };
      }
    }          
    return retVal;
  };

  async processNotification(notificationId: any) {
    let retVal = {};
    try {
      let returnData = {};
      const datasource = new ShareMyDataQueue({
        modelOrCollection: oneMongoClient
          .db(appConfig.database)
          .collection('sharemydata_queue'),
      });
      const notificationRequest = await datasource.getQueueItem(notificationId);
      const notification = notificationRequest.data[0];
      if (notification.type === NotificationQueueItem) {
        // check the first URI to see what type of notification this is.
        // currently support Authorization and Batch/Subscription
        const URIs =  notification.resource_URIs as string[];
        if (URIs.length > 1 && URIs[0].includes("/Authorization/")) {
          const notificationInfo = await getNotificationInformation(notification.resource_URIs[0]);

          if (notificationInfo['Authorization']) {
            const authInfo = notificationInfo['Authorization'];
            logger.debug("Found authorization notification");
            if (authInfo.status == 0) {
              const uriArray = authInfo.resourceURIs.split('/');
              const customerId = uriArray[uriArray.length - 1];

              const udiFindResults = utilityDataIntegrationsDatasource.collection.find({
                "retail_customer_id" : customerId,
                "provider" : "pge",
              });
              logger.debug("retail_customer_id is " + customerId);
              let udiArray = [];
              while(await udiFindResults.hasNext()) {
                const e = await udiFindResults.next();
                udiArray.push(e);
              }
              for (let i = 0; i < udiArray.length; i++) {
                const udi = udiArray[i];
                // remove from any properties associated with.
                for (let j = 0; j < udi.meter_association_ids.length; j++) {
                  const propId = udi.meter_association_ids[j].property_id;
                  const propertyResponse = await propertiesDatasource.getProperty(propId);
                  const property = propertyResponse.data[0];
                  const changeResult = await propertiesDatasource.updateProperty(propId, {
                    ...property,
                    utility_data_integrations: property.utility_data_integrations.filter(
                      (element) => element != udi),
                  });
                  logger.debug("Updated Property to " + JSON.stringify(changeResult));
                }
                // remove the integration
                const removeResult = utilityDataIntegrationsDatasource.deleteUtilityDataIntegration(
                  udi._id
                );
                // update the status of the queue item (maybe remove it at some point?)
                const updateResponse = await datasource.updateQueueItem(notification._id.toString(), {
                  ...notification,
                  status: "integrated",
                });
                returnData = updateResponse.data;
              }
            }
          }
        } else if (URIs.length > 1 && URIs[0].includes("/Batch/Subscription/")) {
          // data from a subscription is ready.  Pass on to the AMI data service
          const response = await axios.post(
            `${amiDataServiceBase}/provider/pge/share_my_data/usage/batch_subscription`,
            URIs,
          );
          returnData = "Good";
        }
      }
      retVal = {message: "success", data: returnData};
    } catch (e) {
      if (isAxiosError(e)) {
        if (e.response != null) {
          logger.error(e.response.status);
          logger.error(e.response.data);
          retVal = {
            status: e.response.status,
            requestHeaders: e.request.headers,
            data: e.response.data,
          };
        } else {
          logger.error(e);
          retVal = {
            status: 500,
            message: "Uknown Error",
            data: JSON.stringify(e)
          }          
        }
      } else {
        logger.error(e);
        retVal = { status: '500', message: 'Error caught', error: e };
      }
    }
    return retVal;
  };

  async authorizeCustomer(authCode: any) {
    let retVal = {};
    try {
      const pgeShareMyAuth = Buffer.from(
        `${pgeShareMyDataClientId}:${pgeShareMyDataClientSecret}`
      ).toString('base64');

      // Functionally unnecessary for this call, required by PGE anyway
      const redirectUri = encodeURIComponent(
        'https://one-dev.codemagic.app/sharemydata-authorization'
      );

      const headers = {
        Authorization: `Basic ${pgeShareMyAuth}`,
      };

      const key = await getPGEShareMyDataCertKey();

      const axiosConfig = {
        headers: headers,
        httpsAgent: new https.Agent({
          cert: fs.readFileSync('./*.ecoact.org.pem'),
          key: key,
        }),
      };      
      logger.debug(`https://api.pge.com/datacustodian/oauth/v2/token?grant_type=authorization_code&code=${authCode}&redirect_uri=${redirectUri}`);
      logger.debug(`axiosConfig: ${JSON.stringify(axiosConfig)}`);
      logger.debug('pgeShareMyAuth: ' + pgeShareMyAuth);

      const pgeShareMyDataCode4TokenExchangeResult = await axios.post(
        `https://api.pge.com/datacustodian/oauth/v2/token?grant_type=authorization_code&code=${authCode}&redirect_uri=${redirectUri}`, //
        {},
        axiosConfig
      );

      const accessToken = pgeShareMyDataCode4TokenExchangeResult.data.access_token;
      const refreshToken = pgeShareMyDataCode4TokenExchangeResult.data.refresh_token;
      const expiresIn = pgeShareMyDataCode4TokenExchangeResult.data.expires_in;
      const resourceURI = pgeShareMyDataCode4TokenExchangeResult.data.resourceURI;
      const uriTokens = resourceURI.split('/');
      const retailCustomerId = uriTokens[uriTokens.length - 1];

      logger.debug('Refresh Token is ' + refreshToken);
      logger.debug('Access Token is ' + accessToken);


      const datasource = new ShareMyDataQueue({
        modelOrCollection: oneMongoClient
          .db(appConfig.database)
          .collection('sharemydata_queue'),
      });

      datasource.addQueueItem({
        type: AuthQueueItem,
        access_token: accessToken,
        refresh_token: refreshToken,
        retail_customer_id: retailCustomerId,
        access_token_duration: expiresIn,
        status: 'pending',
      });

      if (pgeShareMyDataCode4TokenExchangeResult.data) {
        retVal = {
          message: 'success',
          data: pgeShareMyDataCode4TokenExchangeResult.data,
        };
        sendTeamsNotification(
          "One Notification Added", 
          `PGE Notification Queue item added. Please review in the ${AuthQueueItem} tab.`);
      } else {
        throw new Error('Failed to get token from PGE');
      }

    } catch (e) {
      if (isAxiosError(e)) {
        logger.error(e.response.status);
        logger.error(e.response.data);
        retVal = {
          status: e.response.status,
          requestHeaders: e.request.headers,
          data: e.response.data,
        };
      } else {
        logger.error(e);
        retVal = { status: '500', message: 'Error caught', error: e };
      }
    }
    return retVal;
  }

  async addNotification(data: any) {
    let retVal = {};
    try {
      try {
        logger.debug("data-notification-queue received " + data );
      } catch(e) { logger.error('printing as object', e)}      
      try {
        logger.debug("data-notification-queue received " + JSON.stringify(data) );
      } catch(e) { logger.error('printing as json', e)}
      
        const resourceURIs = data['ns0:batchlist']['ns0:resources'];
        // Add
        const datasource = new ShareMyDataQueue({
          modelOrCollection: oneMongoClient
            .db(appConfig.database)
            .collection('sharemydata_queue'),
        });
        datasource.addQueueItem({
          type: NotificationQueueItem,
          resource_URIs: resourceURIs,
          access_token: '',
          refresh_token: '',
          access_token_duration: '',
          status: 'pending',
          });
        sendTeamsNotification(
          "One Notification Added", 
          `PGE Notification Queue item added. Please review in the ${NotificationQueueItem} tab.`);

      retVal = { status: '200', message: 'OK' };
    } catch (e) {
      logger.error(e);
      retVal = { status: '500', message: 'Error caught', error: e };
    }
    return retVal;
  }
}