import { ObjectId } from 'mongodb';
import { OneMongoResult } from '../interfaces';
import { logger } from '../config';
import { OneMongoDataSource } from './OneMongoDatasource';

export const AuthQueueItem = 'Auth';
export const NotificationQueueItem = 'Notification';

export interface ShareMyDataQueueItemDocument {
  _id?: ObjectId;
  name: String;
  type: String;
  time_created?: Date;
  time_last_modified?: Date;
  refresh_token: String;
  access_token: String;
  retail_customer_id: String;
  resource_URIs: String[];
  access_token_duration: number;
}

export class ShareMyDataQueue extends OneMongoDataSource<ShareMyDataQueueItemDocument> {
  async addQueueItem(item: any) {
    const now = new Date(Date.now());
    logger.debug(`Adding ShareMyDataQueueItem - ${JSON.stringify(item)}`);

    item.time_created = now;
    item.time_last_modified = now;
    try {
      const addedQueueItem = await this.collection.insertOne(item);

      if (addedQueueItem.acknowledged) {
        const inserted = [{ ...item, _id: addedQueueItem.insertedId }];

        logger.debug(
          `ShareMyDataQueueItem.addQueueItem: result - \n ${JSON.stringify(
            inserted
          )}`
        );

        return {
          success: true,
          message: 'ShareMyDataQueueItem inserted',
          data: inserted as ShareMyDataQueueItemDocument[],
        } as OneMongoResult;
      } else {
        return {
          success: false,
          message: 'Error inserting ShareMyDataQueueItem',
          data: [] as ShareMyDataQueueItemDocument[],
        };
      }
    } catch (e) {
      logger.debug('Error adding ShareMyDataQueueItem ' + e.message);
      return {
        success: false,
        message: e.message,
        data: [] as ShareMyDataQueueItemDocument[],
      };
    }
  }

  async getQueueItem(id: any) {
    const queueItem = await this.findOneByIdNoCache(new ObjectId(id));
    if (queueItem) {
      return { success: true, data: [queueItem] };
    } else {
      return {
        success: false,
        message: 'Error getting QueueItem',
        data: [] as ShareMyDataQueueItemDocument[],
      };
    }
  }

  async getQueueItems() {
    const queueItems = await this.findByFieldsNoCache({});
    if (queueItems) {
      return { success: true, data: queueItems };
    } else {
      return {
        success: false,
        message: 'Error getting QueueItems',
        data: [] as ShareMyDataQueueItemDocument[],
      };
    }
  }

  async getQueueItemByRefreshToken(refreshToken: String) {
    if (refreshToken) {
      const queueItems = await this.findByFieldsNoCache({
        refresh_token: `${refreshToken}`,
      });

      if (queueItems) {
        return {
          success: true,
          data: queueItems,
        };
      } else {
        return {
          success: false,
          message: `Error finding share my data queue item for refreshToken : ${JSON.stringify(
            refreshToken
          )}`,
          data: [] as ShareMyDataQueueItemDocument[],
        };
      }
    }
  }

  async deleteQueueItem(id: any) {
    logger.debug(`Deleting ShareMyDataQueueItem ${id}`);

    const deleted = await this.collection.deleteOne({ _id: new ObjectId(id) });
    if (deleted.deletedCount > 0) {
      return {
        success: true,
        data: [] as ShareMyDataQueueItemDocument[],
      };
    } else {
      return {
        success: false,
        message: 'Error deleting QueueItem',
        data: [] as ShareMyDataQueueItemDocument[],
      };
    }
  }

  async updateQueueItem(id: string, queueItem: any) {
    const now = new Date(Date.now());

    logger.debug(
      `Updating ShareMyDataQueueItem ${id} to ${JSON.stringify(queueItem)}`
    );

    queueItem.time_last_modified = now;
    const updatedQueueItem: any = await this.collection.findOneAndUpdate(
      { _id: new ObjectId(id) },
      { $set: queueItem },
      { returnDocument: 'after' }
    );

    logger.debug(
      `QueueItems.updateQueueItem ${id} result: \n ${JSON.stringify(
        updatedQueueItem
      )}`
    );

    if (updatedQueueItem.ok === 1) {
      return {
        success: true,
        message: 'QueueItem updated',
        data: [updatedQueueItem.value],
      };
    } else {
      return {
        success: false,
        message: 'Error updating QueueItem',
        data: [] as ShareMyDataQueueItemDocument[],
      };
    }
  }
}
