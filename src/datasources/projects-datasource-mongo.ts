import { ObjectId } from 'mongodb';
import { logger } from '../config';
import { OneMongoResult } from '../interfaces';
import { OneMongoDataSource } from './OneMongoDatasource';

export interface ProjectDocument {
  _id?: ObjectId;
  number?: number;
  name: string; // @deprecated
  time_created?: Date;
  time_last_modified?: Date;
  current_stage?: ObjectId;
  workflow?: ObjectId[];
  stage_history?: string;
  description?: string;
  creator?: ObjectId;
  manager?: ObjectId;
  partners?: ObjectId[];
  programs?: ObjectId[];
  property?: ObjectId;
  contact?: ObjectId;
  tasks?: ObjectId[];
}

export class Projects extends OneMongoDataSource<ProjectDocument> {
  async addProject(user: any, project: any, projectNumber: any) {
    const now = new Date(Date.now());
    logger.debug(projectNumber);

    project.number = projectNumber;

    if (project.programs && project.programs.length > 0) {
      project.programs?.forEach((program: any, index: number) => {
        project.programs[index] = program
          ? new ObjectId(program as string)
          : undefined;
      });
    }
    project.property = project.property
      ? new ObjectId(project.property as string)
      : undefined;
    project.contact = project.contact
      ? new ObjectId(project.contact as string)
      : undefined;
    project.creator = user ? user._id : undefined;
    project.time_created = now;
    project.time_last_modified = now;
    logger.debug('Projects.addProject: Adding - ' + JSON.stringify(project));

    const result = await this.collection.insertOne(project);
    logger.debug('Projects.addProject: result - ');
    logger.debug(JSON.stringify(result));

    if (result.acknowledged) {
      const inserted = [{ ...project, _id: result.insertedId }];
      logger.debug('Projects.addProject: result - ');
      logger.debug(JSON.stringify(inserted));
      const returnVal: OneMongoResult = {
        success: true,
        message: 'Project inserted',
        data: inserted,
      };

      return returnVal;
    } else {
      return {
        success: false,
        message: 'Error inserting Project',
        data: [] as ProjectDocument[],
      };
    }
  }

  async getProject(projectId: any) {
    const project = await this.findOneByIdNoCache(new ObjectId(projectId));
    logger.debug(
      `getProject ${projectId}: result = \n ${JSON.stringify(project)}`
    );
    if (project) {
      return { success: true, data: [project] };
    } else {
      return {
        success: false,
        message: 'Error getting Project',
        data: [] as ProjectDocument[],
      };
    }
  }

  async getProjectByName(projectName: string) {
    const project = await this.findByFieldsNoCache({ name: projectName }); // TODO: only return first found?
    logger.debug(
      `getProjectByName ${projectName}: result = \n ${JSON.stringify(project)}`
    );
    if (project) {
      return { success: true, data: [project] };
    } else {
      return {
        success: false,
        message: 'Error getting Project by name ' + projectName,
        data: [] as ProjectDocument[],
      };
    }
  }

  async getAllProjects() {
    const result = await this.findByFieldsNoCache({});
    logger.debug(`getAllProjects: result = ${JSON.stringify(result)}`);

    if (result) {
      const returnVal = { success: true, data: result };
      return returnVal;
    } else {
      return {
        success: false,
        message: 'Error getting Projects',
        data: [] as ProjectDocument[],
      };
    }
  }

  async getProjectsByIds(ids: any[]) {
    if (ids) {
      const projects = await this.findManyByIdsNoCache(ids);
      logger.debug(`getProjectsByIds: result = ${JSON.stringify(projects)}`);

      if (projects) {
        return {
          success: true,
          data: projects as ProjectDocument[],
        };
      } else {
        return {
          success: false,
          message: `Error finding projects for list of ids ${JSON.stringify(
            ids
          )}`,
          data: [] as ProjectDocument[],
        };
      }
    } else {
      return {
        success: true,
        data: [] as ProjectDocument[],
      };
    }
  }

  async getProjectsByUser(userId: ObjectId) {
    const projectsByUser = await this.findByFieldsNoCache({
      creator: new ObjectId(userId),
    });
    logger.debug(
      `getProjectsByUser ${userId}: result = ${JSON.stringify(projectsByUser)}`
    );

    if (projectsByUser) {
      return {
        success: true,
        data: projectsByUser as ProjectDocument[],
      };
    } else {
      return {
        success: false,
        message: `Error finding projects for user ${userId}`,
        data: [] as ProjectDocument[],
      };
    }
  }

  async getProjectsByProgram(programId: any) {
    const projectsByProgram = await this.findByFieldsNoCache({
      program: new ObjectId(programId),
    });
    logger.debug(
      `projectsByProgram ${programId}: result = ${JSON.stringify(
        projectsByProgram
      )}`
    );

    if (projectsByProgram) {
      return {
        success: true,
        data: projectsByProgram as ProjectDocument[],
      };
    } else {
      return {
        success: false,
        data: [] as ProjectDocument[],
        message: `Error finding projects for program ${programId}`,
      };
    }
  }

  async updateProject(id: any, project: any) {
    const now = new Date(Date.now());

    logger.debug(`Updating project ${id} to = ${JSON.stringify(project)}`);

    project.program = project.program
      ? new ObjectId(project.program)
      : undefined;
    project.property = project.property
      ? new ObjectId(project.property)
      : undefined;
    project.contact = project.contact
      ? new ObjectId(project.contact)
      : undefined;
    project.time_last_modified = now;

    const updatedProject: any = await this.collection.findOneAndUpdate(
      { _id: new ObjectId(id) },
      { $set: project },
      { returnDocument: 'after' }
    );

    logger.debug(
      `updateProject ${id}: result = ${JSON.stringify(updatedProject)}`
    );

    if (updatedProject.ok) {
      return {
        success: true,
        message: 'Project updated',
        data: [updatedProject.value],
      };
    } else {
      return {
        success: false,
        message: 'Error inserting Project',
        data: [] as ProjectDocument[],
      };
    }
  }

  async deleteProject(id: any) {
    logger.debug(`Deleting project ${id}`);
    const deleted = await this.collection.deleteOne({ _id: new ObjectId(id) });
    if (deleted.deletedCount > 0) {
      return {
        success: true,
        data: [] as ProjectDocument[],
      };
    } else {
      return {
        success: false,
        message: 'Error deleting Program',
        data: [] as ProjectDocument[],
      };
    }
  }
}
