import { Portfolios } from '../portfolios-datasource-mongo';
import { Projects } from '../projects-datasource-mongo';
import { MongoClient, ObjectId } from 'mongodb';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { afterAll, beforeAll, describe, expect, test } from '@jest/globals';

import { appConfig } from '../../config';

const testPortfolio: any = {
  name: 'Test Portfolio 1',
};

const testProjectInputs = [
  {
    name: 'Test Project 1',
    stage: 'onboarding',
  },
  {
    name: 'Test Project 2',
    stage: 'estimating',
  },
];
const user = { _id: new ObjectId('624c5f30cc162f513896167f') };

describe('Portfolio datasource in apollo', () => {
  let con: MongoClient;
  let mongoServer: MongoMemoryServer;
  let ds: Portfolios;
  let projectDs: Projects;
  let insertedPortfolio: any;
  let testProjects: any[] = [];

  beforeAll(async () => {
    mongoServer = await MongoMemoryServer.create();
    con = await MongoClient.connect(mongoServer.getUri(), {});
    ds = new Portfolios({
      modelOrCollection: con.db(appConfig.database).collection('Portfolios'),
    });
    projectDs = new Projects({
      modelOrCollection: con.db(appConfig.database).collection('Projects'),
    });
    // add in some projects for linking later.
    testProjects.push(
      await projectDs.addProject(user, testProjectInputs[0], 1)
    );
    testProjects.push(
      await projectDs.addProject(user, testProjectInputs[1], 2)
    );
  });

  afterAll(async () => {
    if (con) {
      await con.close();
    }
    if (mongoServer) {
      await mongoServer.stop();
    }
  });

  test('Add a Portfolio', async () => {
    testPortfolio.projects = [testProjects[0]._id, testProjects[1]._id];
    const result = await ds.addPortfolio(testPortfolio);
    insertedPortfolio = result.data[0];
    expect(result.success).toBeTruthy();
    expect(result.data[0]._id).toBeDefined();
    expect(result.data[0].name).toBe(testPortfolio.name);
    expect(result.data[0].projects.length).toBe(2);
  });

  test('Get a Portfolio', async () => {
    const result = await ds.getPortfolio(insertedPortfolio._id);
    expect(result.data[0]._id).toBeDefined();
    expect(result.data[0]._id).toStrictEqual(insertedPortfolio._id);
  });
  test('update a Portfolio', async () => {
    const result = await ds.updatePortfolio(insertedPortfolio._id, {
      name: 'Updated Name',
    });

    expect(result.success).toBeTruthy();
    expect(result.data[0].name).toBe('Updated Name');
    const updatedValue = await ds.getPortfolio(insertedPortfolio._id);
    expect(updatedValue.success).toBeTruthy();
    expect(updatedValue.data[0].name).toBe('Updated Name');
  });

  test('get all Portfolios', async () => {
    await ds.addPortfolio({ name: 'Portfolio 2' });
    const result = await ds.getPortfolios();
    expect(result.success).toBeTruthy();
    expect(result.data.length).toBe(2);
  });

  test('delete a Portfolio', async () => {
    const result = await ds.deletePortfolio(insertedPortfolio._id);
    expect(result.success).toBeTruthy();
    // now do a get to see if it's in there.
    const checkResult = await ds.getPortfolio(insertedPortfolio._id);
    expect(checkResult.success).toBeFalsy();
  });

  test('fail to delete a Portfolio', async () => {
    const result = await ds.deletePortfolio('624c5f30cc162f513896167f');
    expect(result.success).toBeFalsy();
  });
});
