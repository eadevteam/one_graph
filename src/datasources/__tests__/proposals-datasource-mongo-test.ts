import { Proposals } from '../proposals-datasource-mongo';
import { MongoClient, ObjectId } from 'mongodb';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { afterAll, beforeAll, describe, expect, test } from '@jest/globals';

import { appConfig } from '../../config';

const testProposal = {
  name: 'Test Proposal 1',
};

describe('Proposal datasource in apollo', () => {
  let con: MongoClient;
  let mongoServer: MongoMemoryServer;
  let ds: Proposals;
  let insertedProposal: any;

  beforeAll(async () => {
    mongoServer = await MongoMemoryServer.create();
    con = await MongoClient.connect(mongoServer.getUri(), {});
    ds = new Proposals({
      modelOrCollection: con.db(appConfig.database).collection('Proposals'),
    });
  });

  afterAll(async () => {
    if (con) {
      await con.close();
    }
    if (mongoServer) {
      await mongoServer.stop();
    }
  });

  test('Add a Proposal', async () => {
    const result = await ds.addProposal(testProposal);
    insertedProposal = result.data[0];
    expect(result.success).toBeTruthy();
    expect(result.data[0]._id).toBeDefined();
    expect(result.data[0].name).toBe(testProposal.name);
  });

  test('Get a Proposal', async () => {
    const result = await ds.getProposal(insertedProposal._id);
    expect(result.data[0]._id).toBeDefined();
    expect(result.data[0]._id).toStrictEqual(insertedProposal._id);
  });

  test('update a Proposal', async () => {
    const result = await ds.updateProposal(insertedProposal._id, {
      name: 'Updated Name',
    });

    expect(result.success).toBeTruthy();
    expect(result.data[0].name).toBe('Updated Name');
    const updatedValue = await ds.getProposal(insertedProposal._id);
    expect(updatedValue.success).toBeTruthy();
    expect(updatedValue.data[0].name).toBe('Updated Name');
  });

  test('get all Proposals', async () => {
    await ds.addProposal({ name: 'Proposal 2' });
    const result = await ds.getProposals();
    expect(result.success).toBeTruthy();
    expect(result.data.length).toBe(2);
  });

  test('delete a Proposal', async () => {
    const updatedValue = await ds.getProposal(insertedProposal._id);
    const result = await ds.deleteProposal(insertedProposal._id);
    expect(result.success).toBeTruthy();
    // now do a get to see if it's in there.
    const checkResult = await ds.getProposal(insertedProposal._id);
    expect(checkResult.success).toBeFalsy();
  });

  test('fail to delete a Proposal', async () => {
    const result = await ds.deleteProposal('624c5f30cc162f513896167f');
    expect(result.success).toBeFalsy();
  });
});
