import { People } from '../people-datasource-mongo';
import { MongoClient, ObjectId } from 'mongodb';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { afterAll, beforeAll, describe, expect, test } from '@jest/globals';

import { appConfig } from '../../config';

const testPerson = {
  last_name: 'person',
  first_name: 'Not a',
  email: 'test@test.com',
  company: 'ecoact',
  cognito_uuid: '624c5f30cc162f513896167f',
  role: 'Admin',
  phone_number: '(555)555-5555',
  time_created: Date.now(),
  time_last_modified: Date.now(),
};

describe('Person datasource in apollo', () => {
  let con: MongoClient;
  let mongoServer: MongoMemoryServer;
  let ds: People;
  let insertedPerson: any;

  beforeAll(async () => {
    mongoServer = await MongoMemoryServer.create();
    con = await MongoClient.connect(mongoServer.getUri(), {});
    ds = new People({
      modelOrCollection: con.db(appConfig.database).collection('People'),
    });
  });

  afterAll(async () => {
    if (con) {
      await con.close();
    }
    if (mongoServer) {
      await mongoServer.stop();
    }
  });

  test('Add a Person', async () => {
    const result = await ds.addPerson(testPerson);
    insertedPerson = result.data[0];
    expect(result.data[0]._id).toBeDefined();
    expect(result.data[0].first_name).toBe(testPerson.first_name);
  });

  test('Get a Person', async () => {
    const result = await ds.getPerson(insertedPerson._id);
    expect(result.data[0]._id).toBeDefined();

    expect(result.data[0]._id).toStrictEqual(insertedPerson._id);
  });

  test('update a Person', async () => {
    const result = await ds.updatePerson(insertedPerson._id, {
      first_name: 'Updated',
      last_name: 'Name',
    });

    expect(result).toBeDefined();
    expect(result.data[0].first_name).toBe('Updated');
    expect(result.data[0].email).toBe(testPerson.email);
    const updatedValue = await ds.getPerson(insertedPerson._id);
    expect(updatedValue).toBeDefined();
    expect(updatedValue.data[0].first_name).toBe('Updated');
  });

  test('get all People', async () => {
    await ds.addPerson({ first_name: 'Person', last_name: 'Two' });
    const result = await ds.getPeople();
    expect(result).toBeDefined();
    expect(result.data.length).toBe(2);
  });

  test('delete a Person', async () => {
    const updatedValue = await ds.getPerson(insertedPerson._id);
    const result = await ds.deletePerson(insertedPerson._id);
    expect(result).toBeDefined();
    expect(result.success).toBeTruthy();
    // now do a get to see if it's in there.
    const checkResult = await ds.getPerson(insertedPerson._id);
    expect(checkResult.success).toBeFalsy();
  });

  test('fail to delete a Person', async () => {
    const result = await ds.deletePerson('624cb76e69e6c3c23d1f4129');
    expect(result).toBeDefined();
    expect(result.success).toBeFalsy();
  });
});
