import { Projects } from '../projects-datasource-mongo';
import { MongoClient, ObjectId } from 'mongodb';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { afterAll, beforeAll, describe, expect, test } from '@jest/globals';

import { appConfig, logger } from '../../config';
import { Savings } from '../savings-datasource-mongo';
import { Counters } from '../counters-datasource-mongo';

const user = { _id: new ObjectId('624c5f30cc162f513896167f') };
const user2 = { _id: new ObjectId('627bfbfa52a638a881dab2ee') };
const program = { _id: new ObjectId('624cb76e69e6c3c23d1f4129') };
const property = { _id: new ObjectId('627bfb90d67c8ea97fb2077c') };
const contact = { _id: new ObjectId('627bfb9a26a8686e6a5b0719') };
const measures = [
  { _id: new ObjectId('627bfb9f25a6ba6669327cf7') },
  { _id: new ObjectId('627bfba6c952786244bc97f3') },
];

const testSavings = {
  methodology: 'ManualLighting',
  annual_kwh_savings: 965.0,
  installation_cost: 24999.99,
  incentive: 10000,
  payback: 5.2,
};

const testProject = {
  name: 'Test Projeect 1',
  current_stage: 'onboarding',
  property: property._id,
  program: program._id,
  contact: contact._id,
  measures: [measures[0]._id, measures[1]._id],
};

describe('Project datasource in apollo', () => {
  let con: MongoClient;
  let mongoServer: MongoMemoryServer;
  let ds: Projects;
  let counter: Counters;
  let savingsDatasource: Savings;
  let insertedProject: any;

  beforeAll(async () => {
    mongoServer = await MongoMemoryServer.create();
    con = await MongoClient.connect(mongoServer.getUri(), {});
    ds = new Projects({
      modelOrCollection: con.db(appConfig.database).collection('projects'),
    });
    counter = new Counters({
      modelOrCollection: con.db(appConfig.database).collection('counters'),
    });
    await counter.collection.insertOne({_id: 'projectnumber', seq: 1});
    savingsDatasource = new Savings({
      modelOrCollection: con.db(appConfig.database).collection('savings'),
    });
  });

  afterAll(async () => {
    if (con) {
      await con.close();
    }
    if (mongoServer) {
      await mongoServer.stop();
    }
  });

  test('Add a project', async () => {
    const nextProjectNumber = await counter.getNextProjectNumber();
    const result = await ds.addProject(user, testProject, nextProjectNumber);
    insertedProject = result.data[0];
    expect(result.success).toBeTruthy();
    expect(result.data[0]._id).toBeDefined();
    expect(result.data[0].name).toBe(testProject.name);
  });

  test('Get a project', async () => {
    const result = await ds.getProject(insertedProject._id);
    expect(result.success).toBeTruthy();
    expect(result.data[0]._id).toStrictEqual(insertedProject._id);
  });

  test('update a project', async () => {
    const addSavingsResult = await savingsDatasource.addSavings(testSavings);
    const insertedId = addSavingsResult.data[0]._id;

    const result = await ds.updateProject(insertedProject._id, {
      name: 'Updated Name',
      current_stage: 'reporting',
      program: insertedProject.programs,
      savings: [insertedId],
    });

    expect(result.success).toBeTruthy();
    expect(result.data[0].name).toBe('Updated Name');
    const updatedValue = await ds.getProject(insertedProject._id);
    expect(updatedValue.success).toBeTruthy();
    expect(updatedValue.data[0].name).toBe('Updated Name');
    expect(updatedValue.data[0].current_stage).toBe('reporting');
    //    expect(updatedValue.data[0].savings).toStrictEqual([insertedId]);
  });

  test('get all projects', async () => {
    const nextProjectNumber = await counter.getNextProjectNumber();

    await ds.addProject(
      user,
      { name: 'Project 2', current_stage: 'onboarding' },
      nextProjectNumber
    );
    const result = await ds.getAllProjects();

    expect(result.success).toBeTruthy();
    expect(result.data.length).toBe(2);
  });

  test('get projects for user', async () => {
    const nextProjectNumber = await counter.getNextProjectNumber();

    // add an extra project assigned to a different user.
    await ds.addProject(
      user,
      {
        name: 'Project 3',
        current_stage: 'onboarding',
        program: program._id,
      },
      nextProjectNumber
    );

    const result = await ds.getProjectsByUser(user._id);
    expect(result.success).toBeTruthy();
    expect(result.data.length).toBe(3);
  });

  test('get projects for program', async () => {
    const result = await ds.getProjectsByProgram(program._id);
    expect(result.success).toBeTruthy();
    expect(result.data.length).toBe(1);
  });

  test('delete a project', async () => {
    const result = await ds.deleteProject(insertedProject._id);
    expect(result.success).toBeTruthy();
    // now do a get to see if it's in there.
    const checkResult = await ds.getProject(insertedProject._id);
    expect(checkResult.success).toBeFalsy();
  });

  test('fail to delete a project', async () => {
    const result = await ds.deleteProject(user._id.toString());
    expect(result.success).toBeFalsy();
  });
});
