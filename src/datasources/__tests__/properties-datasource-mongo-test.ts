import { Properties, PropertyDocument } from '../properties-datasource-mongo';
import { Collection, MongoClient, ObjectId } from 'mongodb';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { afterAll, beforeAll, describe, expect, test } from '@jest/globals';

import { appConfig } from '../../config';

const testProperty = {
  banner: 'Test Property 1',
  said: 5343243,
  address1: '444 Street Road',
  address2: 'Around the Corner',
  city: 'Chicago',
  state: 'IL',
  zip_code: 99999,
  year_built: 1999,
  building_type: 'RTL',
  company: {name: 'Test Company 1'}
};

describe('Property datasource in apollo', () => {
  let con: MongoClient;
  let mongoServer: MongoMemoryServer;
  let ds: Properties;
  let insertedProperty: any;

  beforeAll(async () => {
    mongoServer = await MongoMemoryServer.create();
    con = await MongoClient.connect(mongoServer.getUri(), {});


    new Properties({
      modelOrCollection: con.db(appConfig.database).collection('properties'),
    });

    let collection = con.db(appConfig.database).collection('Properties') as Collection<PropertyDocument>;
    ds = new Properties({ modelOrCollection: collection });
    // create the index
    collection.createIndex({banner: "text", "company.name": "text", address_1: "text", address_2: "text", city: "text",state: "text",zip_code: "text"});
  });

  afterAll(async () => {
    if (con) {
      await con.close();
    }
    if (mongoServer) {
      await mongoServer.stop();
    }
  });

  test('Add a Property', async () => {
    const result = await ds.addProperty(testProperty);
    insertedProperty = result.data[0];
    expect(result.success).toBeTruthy();
    expect(result.data[0]._id).toBeDefined();
    expect(result.data[0].banner).toBe(testProperty.banner);
  });
  test('Get a Property', async () => {
    const result = await ds.getProperty(insertedProperty._id);
    expect(result.success).toBeTruthy();
    expect(result.data[0]._id).toBeDefined();
    expect(result.data[0]._id).toStrictEqual(insertedProperty._id);
  });

  test('update a Property', async () => {
    const result = await ds.updateProperty(insertedProperty._id, {
      company: { name: 'Updated Name' },
      said: 222222,
    });

    expect(result.success).toBeTruthy();
    expect(result.data[0].company.name).toBe('Updated Name');
    expect(result.data[0].said).toBe(222222);

    const updatedValue = await ds.getProperty(insertedProperty._id);
    expect(updatedValue).toBeDefined();
    expect(updatedValue.data[0].company['name']).toBe('Updated Name');
  });

  test('get all Properties', async () => {
    const testProperty2 = {
      banner: 'Property 2',
      company: {name: "no name"},
      said: 12311,
    };
    await ds.addProperty(testProperty2);
    const result = await ds.getProperties();
    expect(result.success).toBeTruthy();
    expect(result.data.length).toBe(2);
  });

  // test('use search term to get some properties', async() => {
  //   const searchTerm = "Updated"

  //   const results = await ds.searchProperties(searchTerm);
  //   expect(results.success).toBeTruthy();
  //   expect(results.data.length).toBe(1);
  // })

  test('delete a Property', async () => {
    const updatedValue = await ds.getProperty(insertedProperty._id);
    const result = await ds.deleteProperty(insertedProperty._id);
    expect(result.success).toBeTruthy();
    // now do a get to see if it's in there.
    const checkResult = await ds.getProperty(insertedProperty._id);
    expect(checkResult).toBeFalsy();
  });

  test('fail to delete a Property', async () => {
    const result = await ds.deleteProperty('624c5f30cc162f513896167f');
    expect(result.success).toBeFalsy();
  });
});
