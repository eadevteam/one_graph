import { Partners } from '../partners-datasource-mongo';
import { MongoClient, ObjectId } from 'mongodb';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { afterAll, beforeAll, describe, expect, test } from '@jest/globals';

import { appConfig } from '../../config';

const testPartner = {
  name: 'Test Partner 1',
};

describe('Partner datasource in apollo', () => {
  let con: MongoClient;
  let mongoServer: MongoMemoryServer;
  let ds: Partners;
  let insertedPartner: any;

  beforeAll(async () => {
    mongoServer = await MongoMemoryServer.create();
    con = await MongoClient.connect(mongoServer.getUri(), {});
    ds = new Partners({
      modelOrCollection: con.db(appConfig.database).collection('Partners'),
    });
  });

  afterAll(async () => {
    if (con) {
      await con.close();
    }
    if (mongoServer) {
      await mongoServer.stop();
    }
  });

  test('Add a Partner', async () => {
    const result = await ds.addPartner(testPartner);
    insertedPartner = result.data[0];
    expect(result.data[0]._id).toBeDefined();
    expect(result.data[0].name).toBe(testPartner.name);
  });

  test('Get a Partner', async () => {
    const result = await ds.getPartner(insertedPartner._id);
    expect(result.data[0]._id).toBeDefined();

    expect(result.data[0]._id).toStrictEqual(insertedPartner._id);
  });

  test('update a Partner', async () => {
    const result = await ds.updatePartner(insertedPartner._id, {
      name: 'Updated Name',
    });

    expect(result).toBeDefined();
    expect(result.data[0].name).toBe('Updated Name');
    const updatedValue = await ds.getPartner(insertedPartner._id);
    expect(updatedValue).toBeDefined();
    expect(updatedValue.data[0].name).toBe('Updated Name');
  });

  test('get all Partners', async () => {
    await ds.addPartner({ name: 'Partner 2' });
    const result = await ds.getPartners();
    expect(result).toBeDefined();
    expect(result.data.length).toBe(2);
  });

  test('delete a Partner', async () => {
    const updatedValue = await ds.getPartner(insertedPartner._id);
    const result = await ds.deletePartner(insertedPartner._id);
    expect(result).toBeDefined();
    expect(result.success).toBeTruthy();
    // now do a get to see if it's in there.
    const checkResult = await ds.getPartner(insertedPartner._id);
    expect(checkResult.success).toBeFalsy();
  });

  test('fail to delete a Partner', async () => {
    const result = await ds.deletePartner('624c5f30cc162f513896167f');
    expect(result).toBeDefined();
    expect(result.success).toBeFalsy();
  });
});
