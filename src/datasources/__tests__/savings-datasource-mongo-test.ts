import { Savings } from '../savings-datasource-mongo';
import { MongoClient } from 'mongodb';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { afterAll, beforeAll, describe, expect, test } from '@jest/globals';

import { appConfig } from '../../config';

// NOTE: The real calculation

const manualLightingSavings = {
  methodology: 'ManualLighting',
  annual_kwh_savings: 965.0,
  installation_cost: 24999.99,
  incentive: 10000,
  payback: 5.2,
};

const quickLightingSavings = {
  methodology: 'QuickLighting',
  annual_kwh_savings: 398765.09,
  installation_cost: 83400,
  incentive: 29550.5,
  payback: 1.2,
};

const quickHvacSavings = {
  methodology: 'QuickHvac',
  annual_kwh_savings: 200,
  installation_cost: 1000,
  incentive: 50.5,
  payback: 0.5,
  tonnage: 200,
  utility_rate: 0.17,
  fee: 10000,
};

describe('Savings datasource in apollo', () => {
  let con: MongoClient;
  let mongoServer: MongoMemoryServer;
  let ds: Savings;
  let insertedSavings: any;

  beforeAll(async () => {
    mongoServer = await MongoMemoryServer.create();
    con = await MongoClient.connect(mongoServer.getUri(), {});
    ds = new Savings({
      modelOrCollection: con.db(appConfig.database).collection('Savings'),
    });
  });

  afterAll(async () => {
    if (con) {
      await con.close();
    }
    if (mongoServer) {
      await mongoServer.stop();
    }
  });

  test('Add a Manual Lighting Savings', async () => {
    const result = await ds.addSavings(manualLightingSavings);
    insertedSavings = result.data[0];
    expect(result.data[0]._id).toBeDefined();
    expect(result.data[0].methodology).toBe('ManualLighting');
    expect(result.data[0].annual_kwh_savings).toBe(965.0);
    expect(result.data[0].installation_cost).toBe(24999.99);
    expect(result.data[0].incentive).toBe(10000);
    expect(result.data[0].payback).toBe(5.2);
  });

  test('Add a Quick Lighting Savings', async () => {
    const result = await ds.addSavings(quickLightingSavings);
    insertedSavings = result.data[0];
    expect(result.data[0]._id).toBeDefined();
    expect(result.data[0].methodology).toBe('QuickLighting');
    expect(result.data[0].annual_kwh_savings).toBe(398765.09);
    expect(result.data[0].installation_cost).toBe(83400);
    expect(result.data[0].incentive).toBe(29550.5);
    expect(result.data[0].payback).toBe(1.2);
  });

  test('Add a Quick HVAC Savings', async () => {
    const result = await ds.addSavings(quickHvacSavings);
    insertedSavings = result.data[0];
    expect(result.data[0]._id).toBeDefined();
    expect(result.data[0].methodology).toBe('QuickHvac');
    expect(result.data[0].annual_kwh_savings).toBe(200);
    expect(result.data[0].installation_cost).toBe(1000);
    expect(result.data[0].incentive).toBe(50.5);
    expect(result.data[0].payback).toBe(0.5);
    expect(result.data[0].tonnage).toBe(200);
    expect(result.data[0].utility_rate).toBe(0.17);
    expect(result.data[0].fee).toBe(10000);
  });
});
