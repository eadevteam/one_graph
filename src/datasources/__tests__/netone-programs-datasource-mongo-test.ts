import { Programs } from '../programs-datasource-mongo';
import { MongoClient, ObjectId } from 'mongodb';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { afterAll, beforeAll, describe, expect, test } from '@jest/globals';

import { appConfig } from '../../config';

describe('Program datasource in apollo', () => {
  let con: MongoClient;
  let mongoServer: MongoMemoryServer;
  let ds: Programs;
  let insertedProgram: any;

  beforeAll(async () => {
    mongoServer = await MongoMemoryServer.create();
    con = await MongoClient.connect(mongoServer.getUri(), {});
    ds = new Programs({
      modelOrCollection: con.db(appConfig.database).collection('Programs'),
    });
  });

  afterAll(async () => {
    if (con) {
      await con.close();
    }
    if (mongoServer) {
      await mongoServer.stop();
    }
  });

  test('Add a Program', async () => {
    const result = await ds.addProgram({name: 'Program Code', type: 'NetOne'});
    insertedProgram = result.data[0];
    expect(result.data[0]._id).toBeDefined();
  });

  test('Get a Program', async () => {
    const result = await ds.getProgram(insertedProgram._id);
    expect(result.data[0]._id).toBeDefined();

    expect(result.data[0]._id).toStrictEqual(insertedProgram._id);
  });

  test('update a Program', async () => {
    const result = await ds.updateProgram(insertedProgram._id, {name: 'program code', type: 'NetOne'});

    expect(result).toBeDefined();
    expect(result.data[0].type).toBe('NetOne');
    expect(result.data[0].name).toBe('program code');
    const updatedValue = await ds.getProgram(insertedProgram._id);
    expect(updatedValue).toBeDefined();
    expect(updatedValue.data[0].type).toBe('NetOne');
  });

  test('get all Programs', async () => {
    await ds.addProgram({name: 'program code 2'});
    const result = await ds.getPrograms();
    expect(result).toBeDefined();
    expect(result.data.length).toBe(2);
  });

  test('delete a Program', async () => {
    const updatedValue = await ds.getProgram(insertedProgram._id);
    const result = await ds.deleteProgram(insertedProgram._id);
    expect(result).toBeDefined();
    expect(result.success).toBeTruthy();
    // now do a get to see if it's in there.
    const checkResult = await ds.getProgram(insertedProgram._id);
    expect(checkResult.success).toBeFalsy();
  });

  test('fail to delete a Program', async () => {
    const result = await ds.deleteProgram('624c5f30cc162f513896167f');
    expect(result).toBeDefined();
    expect(result.success).toBeFalsy();
  });
});
