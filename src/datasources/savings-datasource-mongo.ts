import { ObjectId } from 'mongodb';
import { logger } from '../config';
import { OneMongoDataSource } from './OneMongoDatasource';

export interface SavingsDocument {
  _id?: ObjectId;
  name: String;
  time_created?: Date;
  time_last_modified?: Date;
  methodology: String;
  fields?: any;
  line_items?: ObjectId[];
  isDraft?: boolean;
}

export class Savings extends OneMongoDataSource<SavingsDocument> {
  async addSavings(savings: any) {
    logger.debug(`Adding Savings ${JSON.stringify(savings)}`);

    const now = new Date(Date.now());
    savings.time_created = now;
    savings.time_last_modified = now;
    savings.line_items = savings.line_items
      ? savings.line_items.map((value: any) => new ObjectId(value))
      : [];

    try {
      const insertedSavings = await this.collection.insertOne(savings);

      logger.debug(`Added ${JSON.stringify(insertedSavings) ?? 'nothing'}`);

      if (insertedSavings.acknowledged) {
        const inserted = [{ ...savings, _id: insertedSavings.insertedId }];
        return {
          success: true,
          message: 'Savings inserted',
          data: inserted,
        };
      }
    } catch (e) {
      logger.error(`Error adding savings ${e}`);
      return {
        success: false,
        message: e,
        data: [] as SavingsDocument[],
      };
    }
  }

  async updateSavings(id: any, newSavings: any) {
    const now = new Date(Date.now());
    newSavings.time_last_modified = now;
    newSavings.line_items = newSavings.line_items
      ? newSavings.line_items.map((value: any) => new ObjectId(value))
      : [];

    logger.debug(`Updating savings ${id} to be ${JSON.stringify(newSavings)}`);

    const updatedSavings: any = await this.collection.findOneAndUpdate(
      { _id: new ObjectId(id) },
      {
        $set: newSavings,
      },
      { returnDocument: 'after' }
    );

    if (updatedSavings.ok == 1) {
      return {
        success: true,
        message: 'Savings updated',
        data: [updatedSavings.value],
      };
    } else {
      logger.error(
        `Mongo result was not ok \n Value: ${JSON.stringify(
          updatedSavings.value
        )} \n Last Error Object: ${JSON.stringify(
          updatedSavings.lastErrorObject
        )}`
      );
      return {
        success: false,
        message: 'Error updating Savings',
        data: [] as SavingsDocument[],
      };
    }
  }

  async getSavings(id: any, isDraft: boolean) {
    try {
      let result;
      let returnVal;
      if (id != null) {
        result = await this.findByFieldsNoCache({
          _id: new ObjectId(id),
          isDraft: isDraft ?? false,
        });
        logger.debug(`Got one savings ${JSON.stringify(result)}`);
        returnVal = { success: true, data: [result] };
      } else {
        result = await this.findByFieldsNoCache({ isDraft: isDraft });
        logger.debug(`Got list of savings ${JSON.stringify(result)}`);
        returnVal = { success: true, data: result };
      }

      if (result) {
        logger.debug(returnVal);
        return returnVal;
      } else {
        logger.debug(`Couldn't find any savings objects`);
        return null;
      }
    } catch (e) {
      logger.error(e);
    }
  }

  async getAllSavingsByIds(ids: any[]) {
    if (ids) {
      const savingsByIds = await this.findManyByIdsNoCache(ids);

      if (savingsByIds) {
        logger.debug(`Got all savings by ids ${JSON.stringify(savingsByIds)}`);
        return {
          success: true,
          data: savingsByIds as SavingsDocument[],
        };
      } else {
        const errorMessage = `Couldn't find savings for list of ids ${JSON.stringify(
          ids
        )}`;

        logger.debug(errorMessage);

        return {
          success: false,
          message: errorMessage,
          data: [] as SavingsDocument[],
        };
      }
    } else {
      logger.error("You didn't pass in any ids");
    }
  }

  async deleteSavings(id: any) {
    logger.debug(`Deleting savings ${id}`);

    const deleted = await this.collection.deleteOne({ _id: id });
    if (deleted.deletedCount > 0) {
      return {
        success: true,
        data: [] as SavingsDocument[],
      };
    } else {
      return {
        success: false,
        message: 'Error deleting Savings',
        data: [] as SavingsDocument[],
      };
    }
  }
}
