import { ObjectId } from 'mongodb';
import { logger } from '../config';
import { OneMongoDataSource } from './OneMongoDatasource';

export interface ProposalDocument {
  _id?: ObjectId;
  name: string;
  time_created: Date;
  time_last_modified: Date;
}

export class Proposals extends OneMongoDataSource<ProposalDocument> {
  async addProposal(proposal: any) {
    const now = new Date(Date.now());
    logger.debug(
      `Proposals.addProposal: Adding -  ${JSON.stringify(proposal)}`
    );
    proposal.time_created = now;
    proposal.time_last_modified = now;
    try {
      const result = await this.collection.insertOne(proposal);

      if (result.acknowledged) {
        const inserted = [{ ...proposal, _id: result.insertedId }];
        logger.debug(
          `Proposals.addProposal: result -  ${JSON.stringify(inserted)}`
        );

        return {
          success: true,
          message: 'Proposal inserted',
          data: inserted,
        };
      } else {
        return {
          success: false,
          message: 'Error inserting Proposal',
          data: [] as ProposalDocument[],
        };
      }
    } catch (e) {
      logger.debug('Error adding Proposal ' + e.message);
      return {
        success: false,
        message: e.message,
        data: [] as ProposalDocument[],
      };
    }
  }

  async getProposal(id: any) {
    const proposal = await this.findOneByIdNoCache(new ObjectId(id));
    if (proposal) {
      return { success: true, data: [proposal] };
    } else {
      return {
        success: false,
        message: 'Error getting Proposal',
        data: [] as ProposalDocument[],
      };
    }
  }

  async getProposals() {
    const proposals = await this.findByFieldsNoCache({});
    if (proposals) {
      return { success: true, data: proposals };
    } else {
      return {
        success: false,
        message: 'Error getting Proposals',
        data: [] as ProposalDocument[],
      };
    }
  }

  async updateProposal(id: string, proposal: any) {
    const now = new Date(Date.now());
    logger.debug(`Updating proposal ${id} to ${JSON.stringify(proposal)}`);

    proposal.time_last_modified = now;
    const updatedProposal: any = await this.collection.findOneAndUpdate(
      { _id: new ObjectId(id) },
      { $set: proposal },
      { returnDocument: 'after' }
    );

    logger.debug(
      `Proposals.updateProposal ${id}: result - \n ${JSON.stringify(
        updatedProposal
      )}`
    );

    if (updatedProposal.ok === 1) {
      return {
        success: true,
        message: 'Proposal updated',
        data: [updatedProposal.value],
      };
    } else {
      return {
        success: false,
        message: 'Error updating Proposal',
        data: [] as ProposalDocument[],
      };
    }
  }

  async deleteProposal(id: any) {
    logger.debug('Deleting proposal' + id);

    const deleted = await this.collection.deleteOne({ _id: new ObjectId(id) });
    if (deleted.deletedCount > 0) {
      return {
        success: true,
        data: [] as ProposalDocument[],
      };
    } else {
      return {
        success: false,
        message: 'Error deleting Proposal',
        data: [] as ProposalDocument[],
      };
    }
  }
}
