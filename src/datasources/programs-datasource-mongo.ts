import { ObjectId } from 'mongodb';
import { logger } from '../config';
import { OneMongoDataSource } from './OneMongoDatasource';

export interface ProgramDocument {
  name: String;
  _id?: ObjectId;
  time_created?: Date;
  time_last_modified?: Date;
  type?: String;
  line_items?: ObjectId[];
  savings?: ObjectId[];
  fields?: {};
}

export class Programs extends OneMongoDataSource<ProgramDocument> {
  async addProgram(program: any) {
    const now = new Date(Date.now());
    logger.debug('adding in program datasource ');
    logger.debug(JSON.stringify(program));
    program.time_created = now;
    program.time_last_modified = now;
    program.line_items = program.line_items
      ? program.line_items.map((value: any) => new ObjectId(value))
      : [];

    try {
      const result = await this.collection.insertOne(program);

      if (result.acknowledged) {
        const inserted = [{ ...program, _id: result.insertedId }];
        const returnVal = {
          success: true,
          message: 'Programs inserted',
          data: inserted,
        };
        return returnVal;
      } else {
        return {
          success: false,
          message: 'Error inserting Programs',
          data: [] as ProgramDocument[],
        };
      }
    } catch (e) {
      logger.error(e.message);
      return {
        success: false,
        message: e.message,
        data: [] as ProgramDocument[],
      };
    }
  }

  async getProgram(id: any) {
    const program = await this.findOneByIdNoCache(new ObjectId(id));
    logger.debug(`getProgram ${id}: result = \n ${JSON.stringify(program)}`);

    if (program) {
      return { success: true, data: [program] };
    } else {
      return {
        success: false,
        message: 'Error getting Program',
        data: [] as ProgramDocument[],
      };
    }
  }

  async getPrograms() {
    const programs = await this.findByFieldsNoCache({});
    logger.debug(`getPrograms: result = \n ${JSON.stringify(programs)}`);

    if (programs) {
      return { success: true, data: programs };
    } else {
      return {
        success: false,
        message: 'Error getting Programs',
        data: [] as ProgramDocument[],
      };
    }
  }

  async getProgramsByIds(ids: string[]) {
    const programs = await this.findManyByIdsNoCache(ids);
    logger.debug(
      `getProgramsByIds (${ids.length} ids): result = \n ${JSON.stringify(
        programs
      )}`
    );

    if (programs) {
      return {
        success: true,
        data: programs as ProgramDocument[],
      };
    } else {
      return {
        success: false,
        message: `Error finding Programs for list of ids ${JSON.stringify(
          ids
        )}`,
        data: [] as ProgramDocument[],
      };
    }
  }

  async updateProgram(id: string, program: any) {
    const now = new Date(Date.now());

    logger.debug(`Updating program ${id} to: ${JSON.stringify(program)}`);

    program.line_items = program.line_items
      ? program.line_items.map((value: any) => new ObjectId(value))
      : [];
    program.savings = program.savings
      ? program.savings.map((value: any) => new ObjectId(value))
      : [];
    program.time_last_modified = now;

    const updatedProgram: any = await this.collection.findOneAndUpdate(
      { _id: new ObjectId(id) },
      {
        $set: program,
      },
      { returnDocument: 'after' }
    );

    logger.debug(
      `Programs.updateProgram: result - \n ${JSON.stringify(updatedProgram)}`
    );

    if (updatedProgram.ok == 1) {
      return {
        success: true,
        message: 'Programs updated',
        data: [updatedProgram.value],
      };
    } else {
      return {
        success: false,
        message: 'Error updating Programs',
        data: [] as ProgramDocument[],
      };
    }
  }

  async deleteProgram(id: any) {
    logger.debug(`Deleting program ${id}`);

    const deleted = await this.collection.deleteOne({ _id: new ObjectId(id) });

    if (deleted.deletedCount > 0) {
      return {
        success: true,
        data: [] as ProgramDocument[],
      };
    } else {
      return {
        success: false,
        message: 'Error deleting Program',
        data: [] as ProgramDocument[],
      };
    }
  }
}
