import { ObjectId } from 'mongodb';
import { OneMongoResult, UtilityServiceNumber } from '../interfaces';
import { logger } from '../config';
import { OneMongoDataSource } from './OneMongoDatasource';

export interface PropertyDocument {
  _id?: ObjectId;
  company?: {};
  store_number?: number;
  utility_service_numbers: UtilityServiceNumber[];
  time_created?: Date;
  time_last_modified?: Date;
  address_1?: string;
  address_2?: string;
  city?: string;
  state?: string;
  zip_code?: number;
  surveys: ObjectId[];
  utility_data_integrations?: [];
}

export class Properties extends OneMongoDataSource<PropertyDocument> {
  async addProperty(property: any) {
    const now = new Date(Date.now());

    logger.debug(
      `Properties.addProperty: Adding - ${JSON.stringify(property)}`
    );

    try {
      property.time_created = now;
      property.time_last_modified = now;
      const addedProperty = await this.collection.insertOne(property);

      logger.debug(
        `Properties.addProperty: result \n ${JSON.stringify(addedProperty)}`
      );

      if (addedProperty.acknowledged) {
        const inserted = [{ ...property, _id: addedProperty.insertedId }];
        return {
          success: true,
          message: 'Property inserted',
          data: inserted,
        };
      } else {
        return {
          success: false,
          message: 'Error inserting Property',
          data: [] as PropertyDocument[],
        };
      }
    } catch (e) {
      logger.debug('Error adding property ' + e.message);
      return {
        success: false,
        message: e.message,
        data: [] as PropertyDocument[],
      };
    }
  }

  async addSurveyToProperty(id: any, survey: ObjectId) {
    const now = new Date(Date.now());
    logger.debug(
      'Property.addSurveyToProperty: updating ' +
        id +
        ' to ' +
        JSON.stringify(survey)
    );

    const result: any = await this.collection.findOneAndUpdate(
      { _id: new ObjectId(id) },
      {
        $set: { time_last_modified: now },
        $addToSet: { surveys: survey },
      },
      { returnDocument: 'after' }
    );
    logger.debug('Property.addSurveyToProperty: result - ');
    logger.debug(JSON.stringify(result));

    if (result.ok === 1) {
      const returnVal = {
        success: true,
        message: 'Survey added to Property',
        data: result.value,
      };
      return returnVal;
    } else {
      return {
        success: false,
        message: 'Error adding Survey to Property',
        data: [] as PropertyDocument[],
      };
    }
  }

  async getProperty(id: any) {
    try {
      const property = await this.collection.findOne({ _id: new ObjectId(id) });
      logger.debug(`getProperty: result = \n ${JSON.stringify(property)}`);

      if (property) {
        return { success: true, data: [property] };
      } else {
        // const returnVal = {
        //   success: false,
        //   message: 'Error getting Property',
        //   data: [] as PropertyDocument[],
        // };

        return null;
      }
    } catch (e) {
      logger.debug(e);
    }
  }

  // TODO: reimplement this in bezosdb
  async searchProperties(searchTerm: string) {
    let result;

    if (searchTerm != null && searchTerm.length > 0) {
      result = this.collection.find({
        $or: [
          {
            address_1: { $regex: `${searchTerm}` },
            city: { $regex: `${searchTerm}` },
            state: { $regex: `${searchTerm}` },
            zip_code: parseInt(searchTerm),
          },
        ],
      });
      // result = await this.findByFields({ $text: { $search: searchTerm } });
    }
    console.log('search result is ' + JSON.stringify(result));
    if (result) {
      return { success: true, data: result.toArray() };
    } else {
      return {
        success: false,
        message: 'Error getting Properties for search term ' + searchTerm,
        data: [] as PropertyDocument[],
      };
    }
  }

  /**
   * Provides possible matches for properties given the filters.
   *
   * @param {*} filters
   * @return {*}
   * @memberof Properties
   */
  async matchProperties(filters: any) {
    let matches = [];

    if (filters != null) {
      // build a search term
      let searchTerm = '';
      if ('company' in filters) searchTerm += ' ' + filters.company;
      if ('banner' in filters) searchTerm += ' ' + filters.banner;
      if ('address_1' in filters) searchTerm += filters.address_1;
      if ('city' in filters) searchTerm += ' ' + filters.city;
      if ('state' in filters) searchTerm += ' ' + filters.state;
      if ('zip_code' in filters) searchTerm += ' ' + filters.zip_code;

      // do the search
      // const result = await this.find({ $text: { $search: searchTerm } });

      // Temporary workaround for bezosdb incompatibility with mongo text search
      const result = await this.collection.find({
        address_1: filters.address_1,
        city: filters.city,
        state: filters.state,
        zip_code: filters.zip_code,
      });

      /* check for actual matches on important fields.
       since this is sorted by weight, most likely matches are at the top.
       for now we just check if the company name or address are the same.
       Need to tweak this. */
      while (await result.hasNext()) {
        const e = await result.next();
        let ret = false;
        ret =
          e.address_1 === filters.address_1 &&
          e.city === filters.city &&
          e.state === filters.state &&
          `${e.zip_code}` === `${filters.zip_code}`;
        //        ret = ret ||  (e.company["name"] ===  filters.company);
        if (ret) {
          matches.push(e);
        }
      }
    }
    console.log('match result is ' + JSON.stringify(matches));
    if (matches.length > 0) {
      const returnVal = { success: true, data: matches };
      return returnVal;
    } else {
      return {
        success: true,
        message: 'No match for filter: ' + JSON.stringify(filters),
        data: [] as PropertyDocument[],
      };
    }
  }

  async getProperties(ids = null, name = null, company = null) {
    let properties;

    if (ids) {
      properties = await this.findManyByIdsNoCache(ids);
    } else if (name || company) {
      properties = await this.findByFieldsNoCache({
        name: name,
        company: company,
      });
    } else {
      properties = await this.findByFieldsNoCache({});
    }

    if (properties) {
      return { success: true, data: properties };
    } else {
      return {
        success: false,
        message: 'Error getting Properties',
        data: [] as PropertyDocument[],
      };
    }
  }

  async updateProperty(id: any, property: any) {
    const now = new Date(Date.now());

    logger.debug(`Updating property ${id} to ${JSON.stringify(property)}`);

    property.time_last_modified = now;
    const updatedProperty: any = await this.collection.findOneAndUpdate(
      { _id: new ObjectId(id) },
      { $set: property },
      { returnDocument: 'after' }
    );

    logger.debug(
      `Properties.updateProperty ${id}: result - \n ${JSON.stringify(
        updatedProperty
      )}`
    );

    if (updatedProperty.ok) {
      return {
        success: true,
        message: 'Property updated',
        data: [updatedProperty.value],
      };
    } else {
      return {
        success: false,
        message: 'Error updating Property',
        data: [] as PropertyDocument[],
      };
    }
  }

  // For Migrations only, not available to the resolver or app clients
  async updateProperties(filter: any, update: any) {
    const result: any = await this.collection.updateMany(filter, update);

    logger.debug('Properties.updateProperties: result - ');
    logger.debug(JSON.stringify(result));
  }

  async deleteProperty(id: any) {
    const returnVal = false;
    logger.debug('Properties.deleteProperty: deleting ' + id);

    const qResult = await this.collection.deleteOne({ _id: new ObjectId(id) });
    if (qResult.deletedCount > 0) {
      return {
        success: true,
        data: [] as PropertyDocument[],
      };
    } else {
      return {
        success: false,
        message: 'Error deleting Property',
        data: [] as PropertyDocument[],
      };
    }
  }
}
