import { ObjectId } from 'mongodb';
import { MongoDataSource } from '../utils/apollo-datasource-mongodb/datasource';

export class OneMongoDataSource<TData> extends MongoDataSource<TData> {

  async findManyByIdsNoCache(ids: (string | ObjectId)[]) {

    try {
      const results = 
        await Promise.all(ids.map(id => this.collection.findOne({ _id: new ObjectId(id) })));
      return results.reduce(
        (accumulator, current) => {
          if (current !== null) {
            accumulator.push(current);
          }
          return accumulator;
        }, 
        new Array(),
      );    
    } catch (e) {
      throw new Error(e);
    }
  }

  async findByFieldsNoCache(fields: any) {

    try {
      const results = this.collection.find(fields);

      let resultsArray = [];
      while(await results.hasNext()) {
        const e = await results.next();
          resultsArray.push(e);
       }
      return resultsArray;        
    } catch (error) {
      throw new Error(error);
    }
  }

  async findOneByIdNoCache(id: (string | ObjectId)) {

  try {
	  return  await this.collection.findOne({ _id: new ObjectId(id) });	
  } catch (error) {
    throw new Error(error);
  }
}
 
}