import { ObjectId } from 'mongodb';
import { logger } from '../config';
import { OneMongoResult } from '../interfaces';
import { OneMongoDataSource } from './OneMongoDatasource';

export interface StageDocument {
  _id?: ObjectId;
  name: string;
  time_created?: Date;
  time_last_modified?: Date;
  start?: Date;
  completed?: Date;
  tasks?: ObjectId[];
}

export class Stages extends OneMongoDataSource<StageDocument> {
  async addStage(stage: any) {
    const now = new Date(Date.now());
    logger.debug('Stages.addStage: Adding - ' + JSON.stringify(stage));
    stage.time_created = now;
    stage.time_last_modified = now;
    try {
      const result = await this.collection.insertOne(stage);

      if (result.acknowledged) {
        const inserted = [{ ...stage, _id: result.insertedId }];
        logger.debug('Stages.addStage: result - ');
        logger.debug(JSON.stringify(inserted));
        const returnVal = {
          success: true,
          message: 'Stage inserted',
          data: inserted,
        };
        return returnVal;
      } else {
        return {
          success: false,
          message: 'Error inserting Stage',
          data: [] as StageDocument[],
        };
      }
    } catch (e) {
      logger.debug('Error adding Stage ' + e.message);
      return {
        success: false,
        message: e.message,
        data: [] as StageDocument[],
      };
    }
  }

  async getStage(id: any) {
    const stage = await this.findOneByIdNoCache(new ObjectId(id));
    if (stage) {
      return { success: true, data: [stage] } as OneMongoResult;
    } else {
      const returnVal = {
        success: false,
        message: 'Error getting Stage',
        data: [] as StageDocument[],
      };
      return returnVal;
    }
  }

  async getStagesByIds(ids: any[]) {
    const stages = await this.findManyByIdsNoCache(ids);

    if (stages) {
      return {
        success: true,
        data: stages as StageDocument[],
      };
    } else {
      return {
        success: false,
        message: `Error finding stages for list of ids ${JSON.stringify(ids)}`,
        data: [] as StageDocument[],
      };
    }
  }

  async updateStage(id: string, stage: any) {
    const now = new Date(Date.now());
    logger.debug(
      'Stages.updateStage: updating ' + id + ' to ' + JSON.stringify(stage)
    );
    stage.time_last_modified = now;
    const result: any = await this.collection.findOneAndUpdate(
      { _id: new ObjectId(id) },
      { $set: stage },
      { returnDocument: 'after' }
    );
    logger.debug('Stages.updateStage: result - ');
    logger.debug(JSON.stringify(result));

    if (result.ok === 1) {
      const returnVal = {
        success: true,
        message: 'Stage updated',
        data: [result.value],
      };
      return returnVal;
    } else {
      return {
        success: false,
        message: 'Error updating Stage',
        data: [] as StageDocument[],
      };
    }
  }

  async deleteStage(id: any) {
    const returnVal = false;
    logger.debug('Stages.deleteStage: deleting ' + id);

    const qResult = await this.collection.deleteOne({ _id: new ObjectId(id) });
    if (qResult.deletedCount > 0) {
      return {
        success: true,
        data: [] as StageDocument[],
      };
    } else {
      return {
        success: false,
        message: 'Error deleting Stage',
        data: [] as StageDocument[],
      };
    }
  }
}
