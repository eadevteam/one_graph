import { OneMongoDataSource } from './OneMongoDatasource';
import { Db } from 'mongodb';

interface CounterDocument {
  _id: string;
  seq: number;
}

export class Counters extends OneMongoDataSource<CounterDocument> {
  async getNextProjectNumber() {
    let result = await this.collection.findOneAndUpdate(
      { _id: 'projectnumber' },
      { $inc: { seq: 1 } },
      { returnDocument: 'after' }
    );

    return result.value.seq;
  }
}
