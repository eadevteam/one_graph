import { ObjectId } from 'mongodb';
import { OneMongoResult } from '../interfaces';
import { logger } from '../config';
import { OneMongoDataSource } from './OneMongoDatasource';

export interface UtilityDataIntegrationDocument {
  _id?: ObjectId;
  retail_customer_id: String;
  provider: String;
  type: String;
  access_token?: String;
  refresh_token?: String;
  access_token_duration?: number;
  access_token_expiration?: Date;
  meter_association_ids?: [String];
}

export class UtilityDataIntegration extends OneMongoDataSource<UtilityDataIntegrationDocument> {
  async addUtilityDataIntegration(item: any) {
    const now = new Date(Date.now());
    logger.debug(
      'UtilityDataIntegration.addUtilityDataIntegration: Adding - ' +
        JSON.stringify(item)
    );
    item.time_created = now;
    item.time_last_modified = now;
    try {
      const addedUtilityDataIntegration = await this.collection.insertOne(item);

      if (addedUtilityDataIntegration.acknowledged) {
        const inserted = [
          { ...item, _id: addedUtilityDataIntegration.insertedId },
        ];
        logger.debug(
          `UtilityDataIntegration.addUtilityDataIntegration: result - \n ${JSON.stringify(
            inserted
          )}`
        );

        return {
          success: true,
          message: 'UtilityDataIntegration inserted',
          data: inserted as UtilityDataIntegrationDocument[],
        } as OneMongoResult;
      } else {
        return {
          success: false,
          message: 'Error inserting UtilityDataIntegration',
          data: [] as UtilityDataIntegrationDocument[],
        };
      }
    } catch (e) {
      logger.debug('Error adding UtilityDataIntegration ' + e.message);
      return {
        success: false,
        message: e.message,
        data: [] as UtilityDataIntegrationDocument[],
      };
    }
  }

  async getUtilityDataIntegrationsById(ids: any[]) {
    const utilityDataIntegrations = await this.findManyByIdsNoCache(ids);

    logger.debug(
      `getUtilityDataIntegrationById: result - \n ${JSON.stringify(
        utilityDataIntegrations
      )}`
    );

    if (utilityDataIntegrations) {
      return {
        success: true,
        data: utilityDataIntegrations as UtilityDataIntegrationDocument[],
      };
    } else {
      return {
        success: false,
        message: `Error finding Utility Data Integrations for list of ids ${JSON.stringify(
          ids
        )}`,
        data: [] as UtilityDataIntegrationDocument[],
      };
    }
  }

  async getUtilityDataIntegration(id: any) {
    const utilityDataIntegration = await this.findOneByIdNoCache(new ObjectId(id));
    if (utilityDataIntegration) {
      return { success: true, data: [utilityDataIntegration] };
    } else {
      return {
        success: false,
        message: 'Error getting UtilityDataIntegration',
        data: [] as UtilityDataIntegrationDocument[],
      };
    }
  }

  async getUtilityDataIntegrations() {
    const utilityDataIntegrations = await this.findByFieldsNoCache({});
    if (utilityDataIntegrations) {
      return { success: true, data: utilityDataIntegrations };
    } else {
      return {
        success: false,
        message: 'Error getting UtilityDataIntegrations',
        data: [] as UtilityDataIntegrationDocument[],
      };
    }
  }

  async deleteUtilityDataIntegration(id: any) {
    logger.debug(`Deleting utility_data_integration ${id}`);

    const deleted = await this.collection.deleteOne({ _id: new ObjectId(id) });
    if (deleted.deletedCount > 0) {
      return {
        success: true,
        data: [] as UtilityDataIntegrationDocument[],
      };
    } else {
      return {
        success: false,
        message: 'Error deleting UtilityDataIntegration',
        data: [] as UtilityDataIntegrationDocument[],
      };
    }
  }

  async updateUtilityDataIntegration(id: string, utilityDataIntegration: any) {
    const now = new Date(Date.now());
    logger.debug(
      `Updating utility_data_integration ${id} to ${JSON.stringify(
        utilityDataIntegration
      )}`
    );

    utilityDataIntegration.time_last_modified = now;
    const updatedUtilityDataIntegration: any =
      await this.collection.findOneAndUpdate(
        { _id: new ObjectId(id) },
        { $set: utilityDataIntegration },
        { returnDocument: 'after' }
      );
    logger.debug(
      `UtilityDataIntegrations.updateUtilityDataIntegration ${id}: result - ${JSON.stringify(
        updatedUtilityDataIntegration
      )}`
    );

    if (updatedUtilityDataIntegration.ok === 1) {
      return {
        success: true,
        message: 'UtilityDataIntegration updated',
        data: [updatedUtilityDataIntegration.value],
      };
    } else {
      return {
        success: false,
        message: 'Error updating UtilityDataIntegration',
        data: [] as UtilityDataIntegrationDocument[],
      };
    }
  }
}
