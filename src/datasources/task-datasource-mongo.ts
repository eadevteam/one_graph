import { ObjectId } from 'mongodb';
import { OneMongoResult } from '../interfaces';
import { logger } from '../config';
import { OneMongoDataSource } from './OneMongoDatasource';

export interface TaskDocument {
  _id?: ObjectId;
  name: String;
  time_created?: Date;
  time_last_modified?: Date;
  start?: Date;
  end?: Date;
  completed?: Date;
  description?: String;
  parent?: ObjectId;
  cost?: any;
}

export class Tasks extends OneMongoDataSource<TaskDocument> {
  async addTask(task: any) {
    const now = new Date(Date.now());
    logger.debug('Tasks.addTask: Adding - ' + JSON.stringify(task));
    task.time_created = now;
    task.time_last_modified = now;
    try {
      const result = await this.collection.insertOne(task);

      if (result.acknowledged) {
        const inserted = [{ ...task, _id: result.insertedId }];
        logger.debug('Tasks.addTask: result - ');
        logger.debug(JSON.stringify(inserted));
        const returnVal: OneMongoResult = {
          success: true,
          message: 'Task inserted',
          data: inserted as TaskDocument[],
        };
        return returnVal;
      } else {
        return {
          success: false,
          message: 'Error inserting Task',
          data: [] as TaskDocument[],
        };
      }
    } catch (e) {
      logger.debug('Error adding Task ' + e.message);
      return {
        success: false,
        message: e.message,
        data: [] as TaskDocument[],
      };
    }
  }

  async getTask(id: any) {
    const task = await this.findOneByIdNoCache(new ObjectId(id));
    if (task) {
      return { success: true, data: [task] };
    } else {
      const returnVal = {
        success: false,
        message: 'Error getting Task',
        data: [] as TaskDocument[],
      };
      return returnVal;
    }
  }

  async getTasks() {
    const tasks = await this.findByFieldsNoCache({});
    if (tasks) {
      return { success: true, data: tasks };
    } else {
      return {
        success: false,
        message: 'Error getting Tasks',
        data: [] as TaskDocument[],
      };
    }
  }

  async getTasksByIds(ids: any[]) {
    if (ids) {
      const tasks = await this.findManyByIdsNoCache(ids);

      if (tasks) {
        return {
          success: true,
          data: tasks as TaskDocument[],
        };
      } else {
        return {
          success: false,
          message: `Error finding stages for list of ids ${JSON.stringify(
            ids
          )}`,
          data: [] as TaskDocument[],
        };
      }
    }
  }

  async updateTask(id: string, Task: any) {
    const now = new Date(Date.now());
    logger.debug(
      'Tasks.updateTask: updating ' + id + ' to ' + JSON.stringify(Task)
    );
    Task.time_last_modified = now;
    const updatedTask: any = await this.collection.findOneAndUpdate(
      { _id: new ObjectId(id) },
      { $set: Task },
      { returnDocument: 'after' }
    );
    logger.debug('Tasks.updateTask: result - ');
    logger.debug(JSON.stringify(updatedTask));

    if (updatedTask.ok === 1) {
      const returnVal = {
        success: true,
        message: 'Task updated',
        data: [updatedTask.value],
      };
      return returnVal;
    } else {
      return {
        success: false,
        message: 'Error updating Task',
        data: [] as TaskDocument[],
      };
    }
  }

  async deleteTask(id: any) {
    logger.debug(`Deleting task ${id}`);

    const deleted = await this.collection.deleteOne({ _id: new ObjectId(id) });
    if (deleted.deletedCount > 0) {
      return {
        success: true,
        data: [] as TaskDocument[],
      };
    } else {
      return {
        success: false,
        message: 'Error deleting Task',
        data: [] as TaskDocument[],
      };
    }
  }
}
