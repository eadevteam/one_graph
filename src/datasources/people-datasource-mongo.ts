import { ObjectId } from 'mongodb';
import { logger } from '../config';
import { OneMongoResult } from '../interfaces';
import { OneMongoDataSource } from './OneMongoDatasource';

export interface PeopleDocument {
  _id?: ObjectId;
  first_name: string;
  last_name: string;
  email?: string;
  company?: string;
  cognito_uuid?: string;
  role?: string;
  phone_number?: string;
  time_created?: Date;
  time_last_modified?: Date;
  bookmarks?: [];
}

export class People extends OneMongoDataSource<PeopleDocument> {
  async addPerson(person: any) {
    const now = new Date(Date.now());
    logger.debug(`Adding person ${JSON.stringify(person)}`);

    person.time_created = now;
    person.time_last_modified = now;
    try {
      const addedPerson = await this.collection.insertOne(person);

      if (addedPerson.acknowledged) {
        const inserted = [{ ...person, _id: addedPerson.insertedId }];
        logger.debug(
          `People.addPeople: result - \n ${JSON.stringify(addedPerson)}`
        );

        return {
          success: true,
          message: 'Person inserted',
          data: inserted,
        } as OneMongoResult;
      } else {
        return {
          success: false,
          message: 'Error inserting person',
          data: [] as PeopleDocument[],
        };
      }
    } catch (e) {
      logger.debug('error adding person ' + e.message);
      return {
        success: false,
        message: e.message,
        data: [] as PeopleDocument[],
      };
    }
  }

  async getPerson(id: any) {
    const person = await this.findOneByIdNoCache(id);
    logger.debug(`Get person result ${JSON.stringify(person)}`);

    if (person) {
      return { success: true, data: [person] };
    } else {
      return {
        success: false,
        message: 'Error getting People',
        data: [] as PeopleDocument[],
      };
    }
  }

  async getPersonByCognitoUUID(cognito_uuid: any) {
    const people = await this.findByFieldsNoCache({ cognito_uuid: cognito_uuid }); // TODO: only return first found?

    logger.debug(`getPersonByCognitoUUID result ${JSON.stringify(people)}`);

    if (people) {
      return { success: true, data: people };
    } else {
      return {
        success: false,
        message: 'Error getting People',
        data: [] as PeopleDocument[],
      };
    }
  }

  async getPeople() {
    const people = await this.findByFieldsNoCache({});
    if (people) {
      return { success: true, data: people };
    } else {
      return {
        success: false,
        message: 'Error getting Peoples',
        data: [] as PeopleDocument[],
      };
    }
  }
  async updatePerson(id: string, person: any) {
    const now = new Date(Date.now());
    logger.debug(`Updating ${id} to ${JSON.stringify(person)}`);

    person.time_last_modified = now;
    const updatedPerson: any = await this.collection.findOneAndUpdate(
      { _id: new ObjectId(id) },
      {
        $set: person,
      },
      { returnDocument: 'after' }
    );

    logger.debug(
      `Peoples.updatePeople ${id}: result - ${JSON.stringify(updatedPerson)}`
    );

    if (updatedPerson.ok == 1) {
      return {
        success: true,
        message: 'People updated',
        data: [updatedPerson.value],
      };
    } else {
      return {
        success: false,
        message: 'Error updating People',
        data: [] as PeopleDocument[],
      };
    }
  }

  async getPeopleByIds(ids: string[]) {
    const people = await this.findManyByIdsNoCache(ids);
    logger.debug(
      `getPeopleByIds (${ids.length ?? 0} ids): result - ${JSON.stringify(
        people
      )}`
    );

    if (people) {
      return {
        success: true,
        data: people as PeopleDocument[],
      };
    } else {
      return {
        success: false,
        message: `Error finding People for list of ids ${JSON.stringify(ids)}`,
        data: [] as PeopleDocument[],
      };
    }
  }

  async deletePerson(id: any) {
    logger.debug('Peoples.deletePeople: deleting ' + id);

    const deleted = await this.collection.deleteOne({ _id: new ObjectId(id) });
    if (deleted.deletedCount > 0) {
      return {
        success: true,
        data: [] as PeopleDocument[],
      };
    } else {
      return {
        success: false,
        message: 'Error deleting People',
        data: [] as PeopleDocument[],
      };
    }
  }
}
