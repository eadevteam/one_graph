import { ObjectId } from 'mongodb';
import { logger } from '../config';
import { OneMongoDataSource } from './OneMongoDatasource';

export interface SurveyDocument {
  _id?: ObjectId;
  fields: {};
}
export class Surveys extends OneMongoDataSource<SurveyDocument> {
  async addSurvey(survey: any) {
    try {
      const now = new Date(Date.now());
      survey.time_created = now;
      survey.time_last_modified = now;

      logger.debug(`Adding survey ${JSON.stringify(survey)}`);

      const addedSurvey = await this.collection.insertOne(survey);

      logger.debug(
        `Surveys.addSurvey: result - \n ${JSON.stringify(addedSurvey)}`
      );

      if (addedSurvey.acknowledged) {
        const inserted = [{ ...survey, _id: addedSurvey.insertedId }];

        return {
          success: true,
          message: 'Survey inserted',
          data: inserted,
        };
      } else {
        return {
          success: false,
          message: 'Error inserting Survey',
          data: [] as SurveyDocument[],
        };
      }
    } catch (e) {
      logger.error('Error adding Survey ' + e.message);
      logger.error(e.stack);
      return {
        success: false,
        message: e.message,
        data: [] as SurveyDocument[],
      };
    }
  }

  async updateSurvey(id: string, survey: any) {
    const now = new Date(Date.now());

    logger.debug(`Updating survey ${id} to ${JSON.stringify(survey)}`);

    survey.time_last_modified = now;
    const updatedSurvey: any = await this.collection.findOneAndUpdate(
      { _id: new ObjectId(id) },
      {
        $set: survey,
      },
      { returnDocument: 'after' }
    );

    logger.debug(
      `Surveys.updateSurvey ${id}: result - \n ${JSON.stringify(updatedSurvey)}`
    );

    if (updatedSurvey.ok == 1) {

      return {
        success: true,
        message: 'surveys updated',
        data: [updatedSurvey.value],
      };
    } else {
      return {
        success: false,
        message: 'Error updating surveys',
        data: [] as SurveyDocument[],
      };
    }
  }

  async getSurvey(id: any) {
    try {
      const survey = await this.findOneByIdNoCache(new ObjectId(id));

      logger.debug(`getSurvey ${id}: result - \n ${JSON.stringify(survey)}`);

      if (survey) {
        const returnVal = { success: true, data: [survey] };
        return returnVal;
      } else {
        // const returnVal = {
        //   success: false,
        //   message: 'Error getting Survey',
        //   data: [] as SurveyDocument[],
        // };

        return null;
      }
    } catch (e) {
      logger.debug(e);
    }
  }

  async getSurveys() {
    const result = await this.findByFieldsNoCache({});

    if (result) {
      return { success: true, data: result };
    } else {
      return {
        success: false,
        message: 'Error getting Surveys',
        data: [] as SurveyDocument[],
      };
    }
  }

  async getSurveysByIds(ids: any[]) {
    if (ids) {
      const surveys = await this.findManyByIdsNoCache(ids);

      logger.debug(
        `getSurveysByIds (${ids.length} ?? 0) ids: result - \n ${JSON.stringify(
          surveys
        )}`
      );

      if (surveys) {
        return {
          success: true,
          data: surveys as SurveyDocument[],
        };
      } else {
        return {
          success: false,
          message: `Error finding surveys for list of ids ${JSON.stringify(
            ids
          )}`,
          data: [] as SurveyDocument[],
        };
      }
    } else {
      return {
        success: true,
        data: [] as SurveyDocument[],
      };
    }
  }

  async deleteSurvey(id: any) {
    logger.debug(`Deleting survey ${id}`);

    const deleted = await this.collection.deleteOne({ _id: new ObjectId(id) });
    if (deleted.deletedCount > 0) {
      return {
        success: true,
        data: [] as SurveyDocument[],
      };
    } else {
      return {
        success: false,
        message: 'Error deleting Survey',
        data: [] as SurveyDocument[],
      };
    }
  }
}
