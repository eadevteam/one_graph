import { ObjectId } from 'mongodb';
import { logger } from '../config';
import { OneMongoResult } from '../interfaces';
import { OneMongoDataSource } from './OneMongoDatasource';

export interface PortfolioDocument {
  _id?: ObjectId;
  name: string;
  projects?: ObjectId[];
  time_created?: Date;
  time_last_modified?: Date;
}

export class Portfolios extends OneMongoDataSource<PortfolioDocument> {
  async addPortfolio(portfolio: any) {
    const now = new Date(Date.now());
    logger.debug(
      'Portfolios.addPortfolio: Adding - ' + JSON.stringify(portfolio)
    );
    portfolio.time_created = now;
    portfolio.time_last_modified = now;
    try {
      const result = await this.collection.insertOne(portfolio);

      if (result.acknowledged) {
        const inserted = [{ ...portfolio, _id: result.insertedId }];
        logger.debug('Portfolios.addPortfolio: result - ');
        logger.debug(JSON.stringify(inserted));
        const returnVal = {
          success: true,
          message: 'Portfolio inserted',
          data: inserted,
        };

        return returnVal;
      } else {
        return {
          success: false,
          message: 'Error inserting Portfolio',
          data: [] as PortfolioDocument[],
        };
      }
    } catch (e) {
      logger.debug('Error adding Portfolio ' + e.message);
      return {
        success: false,
        message: e.message,
        data: [] as PortfolioDocument[],
      };
    }
  }

  async getPortfolio(id: any) {
    const result = await this.findOneByIdNoCache(new ObjectId(id));
    logger.debug(`getPortfolio ${id}: result = \n` + JSON.stringify(result));
    if (result) {
      const returnVal = { success: true, data: [result] };
      return returnVal;
    } else {
      const returnVal = {
        success: false,
        message: 'Error getting Portfolio',
        data: [] as PortfolioDocument[],
      };
      return returnVal;
    }
  }

  async getPortfolioByName(portfolioName: string) {
    const portfolio = await this.findByFieldsNoCache({ name: portfolioName }); // TODO: only return first found?
    logger.debug(
      `getPortfolioByName ${portfolioName}: result = \n ${JSON.stringify(
        portfolio
      )}`
    );

    if (portfolio) {
      const returnVal = { success: true, data: [portfolio] } as OneMongoResult;
      return returnVal;
    } else {
      const returnVal = {
        success: false,
        message: 'Error getting Portfolio by name ' + portfolioName,
        data: [] as PortfolioDocument[],
      };
      return returnVal;
    }
  }

  async getPortfolios() {
    const portfolios = await this.findByFieldsNoCache({});
    logger.debug(
      `getPortfolios ${portfolios}: result = \n ${JSON.stringify(portfolios)}`
    );

    if (portfolios) {
      const returnVal = { success: true, data: portfolios };
      return returnVal;
    } else {
      return {
        success: false,
        message: 'Error getting Portfolios',
        data: [] as PortfolioDocument[],
      };
    }
  }

  async addToPortfolio(id: any, projects: ObjectId[]) {
    const now = new Date(Date.now());
    logger.debug(`Adding ${projects.length ?? 0} projects to portfolio ${id}`);

    const updatedPortfolio: any = await this.collection.findOneAndUpdate(
      { _id: new ObjectId(id) },
      {
        $set: { time_last_modified: now },
        $addToSet: { projects: { $each: projects } },
      },
      { returnDocument: 'after' }
    );
    logger.debug(
      `Portfolios.addToPortfolio: result - ${JSON.stringify(updatedPortfolio)}`
    );

    if (updatedPortfolio.ok === 1) {
      const returnVal = {
        success: true,
        message: 'Projects added to portfolio.',
        data: updatedPortfolio.value,
      };
      return returnVal;
    } else {
      return {
        success: false,
        message: 'Error adding projects to portfolio.',
        data: [] as PortfolioDocument[],
      };
    }
  }
  async deleteFromPortfolio(id: any, projects: ObjectId[]) {
    const now = new Date(Date.now());
    logger.debug(`Deleting ${projects.length} projects from portfolio ${id}`);

    const updatedPortfolio: any = await this.collection.findOneAndUpdate(
      { _id: new ObjectId(id) },
      {
        $set: { time_last_modified: now },
        $pullAll: { projects: projects },
      },
      { returnDocument: 'after' }
    );
    logger.debug(
      `Portfolios.deleteFromPortfolio ${id}: result = \n ${JSON.stringify(
        updatedPortfolio
      )}`
    );

    if (updatedPortfolio.ok === 1) {
      const returnVal = {
        success: true,
        message: 'Projects deleted from portfolio.',
        data: updatedPortfolio.value,
      };
      return returnVal;
    } else {
      return {
        success: false,
        message: 'Error deleting projects from portfolio',
        data: [] as PortfolioDocument[],
      };
    }
  }

  async updatePortfolio(id: any, portfolio: any) {
    const now = new Date(Date.now());
    logger.debug(`Updating portfolio ${id}`);

    portfolio.time_last_modified = now;
    const updatedPortfolio: any = await this.collection.findOneAndUpdate(
      { _id: new ObjectId(id) },
      { $set: portfolio },
      { returnDocument: 'after' }
    );

    logger.debug(
      `Portfolios.updatePortfolio: result - ${JSON.stringify(updatedPortfolio)}`
    );
    if (updatedPortfolio.lastErrorObject != null) {
      const returnVal = {
        success: true,
        message: 'Portfolio updated',
        data: [updatedPortfolio.value],
      };
      return returnVal;
    } else {
      return {
        success: false,
        message: 'Error updating Portfolio',
        data: [] as PortfolioDocument[],
      };
    }
  }

  async deletePortfolio(id: any) {
    logger.debug(`Deleting portfolio ${id}`);
    const deletedPortfolio = await this.collection.deleteOne({ _id: id });

    if (deletedPortfolio.deletedCount > 0) {
      return {
        success: true,
        data: [] as PortfolioDocument[],
      };
    } else {
      return {
        success: false,
        message: 'Error deleting Portfolio',
        data: [] as PortfolioDocument[],
      };
    }
  }
}
