import { OneMongoDataSource } from './OneMongoDatasource';
import { ObjectId } from 'mongodb';
import { logger } from '../config';

export interface PartnerDocument {
  _id?: ObjectId;
  name: string;
  people: ObjectId[];
  time_created?: Date;
  time_last_modified?: Date;
}

export class Partners extends OneMongoDataSource<PartnerDocument> {
  async addPartner(partner: any) {
    const now = new Date(Date.now());
    logger.debug(`Adding partner ${JSON.stringify(partner)}`);

    partner.time_created = now;
    partner.time_last_modified = now;
    try {
      const addedPartner = await this.collection.insertOne(partner);

      logger.debug(
        `Partners.addPartner: result - \n ${JSON.stringify(addedPartner)}`
      );

      if (addedPartner.acknowledged) {
        const inserted = [{ ...partner, _id: addedPartner.insertedId }];
        return {
          success: true,
          message: 'Partner inserted',
          data: inserted,
        };
      } else {
        return {
          success: false,
          message: 'Error inserting Program',
          data: [] as PartnerDocument[],
        };
      }
    } catch (e) {
      logger.debug('Error adding partner ' + e.message);
      return {
        success: false,
        message: e.message,
        data: [] as PartnerDocument[],
      };
    }
  }

  async getPartner(id: any) {
    const partner = await this.findOneByIdNoCache(new ObjectId(id));
    logger.debug(`getPartner: result = \n  ${JSON.stringify(partner)}`);

    if (partner) {
      return { success: true, data: [partner] };
    } else {
      return {
        success: false,
        message: 'Error getting Partner',
        data: [] as PartnerDocument[],
      };
    }
  }

  async getPartnerByName(partnerName: any) {
    const partners = await this.findByFieldsNoCache({ name: partnerName }); // TODO: only return first found?
    logger.debug(`getPartnerByName: result - \n  ${JSON.stringify(partners)}`);

    if (partners) {
      return { success: true, data: partners };
    } else {
      return {
        success: false,
        message: 'Error getting Partner',
        data: [] as PartnerDocument[],
      };
    }
  }

  async getPartners() {
    const partners = await this.findByFieldsNoCache({});
    logger.debug(`getPartners: result - \n  ${JSON.stringify(partners)}`);

    if (partners) {
      return { success: true, data: partners };
    } else {
      return {
        success: false,
        message: 'Error getting Partners',
        data: [] as PartnerDocument[],
      };
    }
  }

  async getPartnersByIds(ids: string[]) {
    const partners = await this.findManyByIdsNoCache(ids);
    logger.debug(`getPartnersById: result - \n  ${JSON.stringify(partners)}`);

    if (partners) {
      return {
        success: true,
        data: partners as PartnerDocument[],
      };
    } else {
      return {
        success: false,
        message: `Error finding Partners for list of ids ${JSON.stringify(
          ids
        )}`,
        data: [] as PartnerDocument[],
      };
    }
  }

  async updatePartner(id: string, partner: any) {
    const now = new Date(Date.now());

    logger.debug(`Updating partner ${id} to ${JSON.stringify(partner)}`);

    partner.time_last_modified = now;
    const updatedPartner: any = await this.collection.findOneAndUpdate(
      { _id: new ObjectId(id) },
      { $set: partner },
      { returnDocument: 'after' }
    );
    logger.debug(
      `updatePartner: result - \n  ${JSON.stringify(updatedPartner)}`
    );

    if (updatedPartner.ok === 1) {
      const returnVal = {
        success: true,
        message: 'Partner updated',
        data: [updatedPartner.value],
      };
      return returnVal;
    } else {
      return {
        success: false,
        message: 'Error inserting Partner',
        data: [] as PartnerDocument[],
      };
    }
  }

  async deletePartner(id: any) {
    logger.debug(`Deleting partner ${id}`);

    const deleted = await this.collection.deleteOne({ _id: new ObjectId(id) });
    if (deleted.deletedCount > 0) {
      return {
        success: true,
        data: [] as PartnerDocument[],
      };
    } else {
      return {
        success: false,
        message: 'Error deleting Program',
        data: [] as PartnerDocument[],
      };
    }
  }
}
