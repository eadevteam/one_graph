import { ObjectId } from 'mongodb';
import { logger } from '../config';
import { OneMongoDataSource } from './OneMongoDatasource';

export interface LineItemDocument {
  _id?: ObjectId;
  time_created?: Date;
  time_last_modified?: Date;
  fields?: any;
}

export class LineItems extends OneMongoDataSource<LineItemDocument> {
  async addLineItem(inputLineItem: any) {
    const now = new Date(Date.now());
    const lineItem = { ...inputLineItem };
    lineItem.time_created = now;
    lineItem.time_last_modified = now;
    try {
      const result = await this.collection.insertOne(lineItem);

      if (result.acknowledged) {
        const inserted = [{ ...lineItem, _id: result.insertedId }];
        const returnVal = {
          success: true,
          message: 'LineItem inserted',
          data: inserted,
        };
        return returnVal;
      } else {
        return {
          success: false,
          message: 'Error inserting LineItem',
          data: [] as LineItemDocument[],
        };
      }
    } catch (e) {
      logger.debug('Error adding lineItem ' + e.message);
      return {
        success: false,
        message: e.message,
        data: [] as LineItemDocument[],
      };
    }
  }

  async getLineItem(id: any) {
    const result = await this.findOneByIdNoCache(new ObjectId(id));
    logger.debug('getLineItem: result = \n' + JSON.stringify(result));
    if (result) {
      const returnVal = { success: true, data: [result] };
      return returnVal;
    } else {
      const returnVal = {
        success: false,
        message: 'Error getting LineItem',
        data: [] as LineItemDocument[],
      };
      return returnVal;
    }
  }

  async getLineItems() {
    const result = await this.findByFieldsNoCache({});
    if (result) {
      const returnVal = { success: true, data: result };
      return returnVal;
    } else {
      return {
        success: false,
        message: 'Error getting LineItems',
        data: [] as LineItemDocument[],
      };
    }
  }

  async getLineItemsById(ids: any[]) {
    if (ids) {
      const lineItems = await this.findManyByIdsNoCache(ids);

      logger.debug(`getLineItemById: result - \n ${JSON.stringify(lineItems)}`);

      if (lineItems) {
        return {
          success: true,
          data: lineItems as LineItemDocument[],
        };
      } else {
        return {
          success: false,
          message: `Error finding line items for list of ids ${JSON.stringify(
            ids
          )}`,
          data: [] as LineItemDocument[],
        };
      }
    }
  }

  async updateLineItem(id: string, values: any) {
    const now = new Date(Date.now());

    logger.debug(`Updating lineItem ${id} to ${JSON.stringify(values)}`);

    values.time_last_modified = now;
    const updatedLineItems: any = await this.collection.findOneAndUpdate(
      { _id: new ObjectId(id) },
      { $set: values },
      { returnDocument: 'after' }
    );

    logger.debug(
      `LineItems.updateLineItems ${id}: result - \n ${JSON.stringify(
        updatedLineItems
      )}`
    );

    if (updatedLineItems.ok === 1) {
      return {
        success: true,
        message: 'LineItem updated',
        data: [updatedLineItems.value],
      };
    } else {
      return {
        success: false,
        message: 'Error updating LineItem',
        data: [] as LineItemDocument[],
      };
    }
  }

  async deleteLineItem(id: any) {
    logger.debug(`Deleting lineItem ${id}`);

    const deleted = await this.collection.deleteOne({ _id: new ObjectId(id) });
    if (deleted.deletedCount > 0) {
      return {
        success: true,
        data: [] as LineItemDocument[],
      };
    } else {
      return {
        success: false,
        message: 'Error deleting LineItem',
        data: [] as LineItemDocument[],
      };
    }
  }
}
