/*==============================================================================

name:       calculations.ts

purpose:    Endpoints for rules engine interaction.

history:    Fri March 1, 2019 08:46:23 (DWS) created.

notes:

                  This program was created by Giavaneers
  and is the confidential and proprietary product of Ecology Action
      Any unauthorized use, reproduction or transfer is strictly prohibited.

                  COPYRIGHT 2018-2019 BY ECOLOGY ACTION.
      (Subject to limited distribution and restricted disclosure only).
                           All rights reserved.

==============================================================================*/
// imports ----------------------------//
import bodyParser from 'body-parser';
import express, { NextFunction, Request, response, Response } from 'express';

import { logger, wsBase } from '../config';
import { rulesFormat } from '../utils/rules-format';
import sampleRuntime from '../utils/sample-runtime';
import http from 'http';

// express router                      //
const router = express.Router();
router.use(bodyParser.json());

/*------------------------------------------------------------------------------

@path       /api/calculations/:moduleName/:calculationName
                                                                              */
/**
 *
 * Invoke rules engine calculation without payload
 *
 * @method: GET
 * @pathparam: calculationName - The name of the rule endpoint to invoke.
 *
 * @notes
 */
/**
 * @swagger
 * /api/rules/calculations/{moduleName}/{calculationName}:
 *    post:
 *      description: Post payload to rules engine endpoint
 *      tags:
 *        - calculations
 *      produces:
 *        - application/json
 *      parameters:
 *        - name: moduleName
 *          in:  path
 *          description: The rules engine module being targeted
 *          required: true
 *          type: string
 *        - name: calculationName
 *          in:  path
 *          description: The rules engine endpoint being targeted
 *          required: true
 *          type: string
 *        - name: body
 *          in: body
 *          description: rules engine payload
 *          required: true
 *          type: object
 *      responses:
 *        200:
 *          description: Result of calculation endpoint
 *          schema:
 *            type: object
 */
// -----------------------------------------------------------------------------
router.post('/:moduleName/:calculationName', async (req, res, next) => {
  logger.info(
    `POST to calc ${req.params.calculationName} for module ${req.params.moduleName}`
  );
  req.body.runtimeContext = sampleRuntime;
  calculation(req, res, next);
});
/*------------------------------------------------------------------------------

@path       /api/calculations/:moduleName/:calculationName
                                                                              */
/**
 *
 * Invoke rules engine calculation without a payload
 *
 * @method: GET
 * @path-param: calculationName - The name of the rule endpoint to invoke.
 *
 * @notes
 *
 */
/**
 * @swagger
 * /api/rules/calculations/{moduleName}/{calculationName}:
 *    get:
 *      description: Post payload to rules engine endpoint
 *      tags:
 *        - calculations
 *      produces:
 *        - application/json
 *      parameters:
 *        - name: moduleName
 *          in:  path
 *          description: The rules engine module being targeted
 *          required: true
 *          type: string
 *        - name: calculationName
 *          in:  path
 *          description: The rules engine endpoint being targeted
 *          required: true
 *          type: string
 *      responses:
 *        200:
 *          description: Result of calculation endpoint
 *          schema:
 *            type: object
 */
// -----------------------------------------------------------------------------
router.get('/:moduleName/:calculationName', async (req, res, next) => {
  logger.info(
    `GET to calc ${req.params.calculationName} for module ${req.params.moduleName}`
  );
  req.body = { runtimeContext: sampleRuntime };
  calculation(req, res, next);
});

/*------------------------------------------------------------------------------

@name       calculation
                                                                              */
/**
 *
 * middleware for handling calls to rules engine
 *
 * @notes
 */
// -----------------------------------------------------------------------------
const calculation = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const reqbody = rulesFormat(req.body);
    const moduleName = req.params.moduleName;
    const responseBody: any = [];

    const calcEngineRequest = http.request(
      `${wsBase}/${moduleName}/${moduleName}/${req.params.calculationName}`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      },
      async (calcEngineResponse) => {
        calcEngineResponse.on('data', (d) => {
          responseBody.push(d);
        });
        calcEngineResponse.on('end', () => {
          res.json(JSON.parse(responseBody.toString()));
        });
      }
    );

    calcEngineRequest.write(JSON.stringify(reqbody));
    calcEngineRequest.end();
  } catch (err) {
    next(err);
  }
};

export default router;
