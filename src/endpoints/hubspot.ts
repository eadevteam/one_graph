import express, { NextFunction, Request, response, Response } from 'express';
import { logger, hubspotAPIKey } from '../config';
import { v4 } from 'uuid';
import axios from 'axios';
import e from 'cors';

// express router                      //
const router = express.Router();

/**
 * Get pre-built List of Taper companies from Hubspot
 * Client consumes and stores hs_company_id and name fields
 **/
router.get(
  '/companies',
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      let ids = [];
      let taperCompanyIdsBatchResult = await axios.get(
        `https://api.hubapi.com/crm/v3/lists/1215/memberships`,
        {
          headers: {
            Authorization: `Bearer ${hubspotAPIKey}`,
          },
        }
      );

      ids = ids.concat(
        taperCompanyIdsBatchResult.data['results'].map((e) => e.recordId)
      );

      while (taperCompanyIdsBatchResult.data['paging']['next'] != null) {
        taperCompanyIdsBatchResult = await axios.get(
          taperCompanyIdsBatchResult.data['paging']['next']['link'],
          {
            headers: {
              Authorization: `Bearer ${hubspotAPIKey}`,
            },
          }
        );
        ids = ids.concat(
          taperCompanyIdsBatchResult.data['results'].map((e) => e.recordId)
        );
      }

      const batchSize = 100;
      const batches = Math.ceil(ids.length / 100);

      let companies = [];
      for (let i = 0; i < batches; i++) {
        const start = i * batchSize;
        const end = start + batchSize;

        const taperCompaniesResult = await axios.post(
          `https://api.hubapi.com/crm/v3/objects/companies/batch/read`,
          {
            inputs: [...ids.slice(start, end).map((id) => ({ id: id }))],
          },
          {
            method: 'POST',
            headers: {
              Authorization: `Bearer ${hubspotAPIKey}`,
            },
          }
        );

        if (taperCompaniesResult.data['status'] == 'error') {
          logger.error(`Batch ${i}` + taperCompaniesResult.data['message']);
          res.json([{ error: taperCompaniesResult.data['message'] }]);
        } else {
          companies = companies.concat(taperCompaniesResult.data['results']);
        }
      }

      res.json(companies);
    } catch (e) {
      logger.error(e);
      res.json([{ error: e }]);
    }
  }
);

export default router;
