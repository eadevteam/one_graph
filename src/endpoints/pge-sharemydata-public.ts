import express, { NextFunction, Request, Response } from 'express';
import {
  logger,
} from '../config';
import { PGEShareMyDataIntegration } from '../utility_integrations/pge-sharemydata';

// pge share my data api implementation //
const pgeIntegration = new PGEShareMyDataIntegration();

// express router                      //
const router = express.Router();

/**
 * Handles retrieving authorization from a PGE customer for us to pull
 * usage data via PGE's ShareMyData implementation of GreenButton
 **/
router.post(
  '/authorization',
  async (req: Request, res: Response, next: NextFunction) => {
    const authCode = req.body.code;
    logger.debug("Authorization request received for code: " + authCode);
    const retVal = pgeIntegration.authorizeCustomer(authCode);
    logger.debug(JSON.stringify(retVal));
    res.json(retVal);
  }
);

router.post(
  '/data-notification-queue',
  async (req: Request, res: Response, next: NextFunction) => {
    const notificationData = req.body;
    const results = pgeIntegration.addNotification(notificationData);
    logger.debug(JSON.stringify(results));
    res.json(results);
  }
);

export default router;
