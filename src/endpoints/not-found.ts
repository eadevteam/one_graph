import { NextFunction, Request, Response } from 'express';
import { logger } from '../config';

const notFound = (req: Request, res: Response, next: NextFunction) => {
  logger.debug('404, resource not found for: ' + req.path);
  res.status(404).json({
    id: 'not_found',
    message: 'resource not found on this server',
  });
};

export default notFound;
