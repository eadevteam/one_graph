import express, { NextFunction, Request, response, Response } from 'express';
import { GetObjectCommand } from '@aws-sdk/client-s3';
import Multer from 'multer';
import { logger, bucket, s3Client, buildEnvironment } from '../config';
import { uploadToBucket } from '../storage/aws-file-storage';
import { v4 } from 'uuid';

// express router                      //
const router = express.Router();

const multer = Multer({
  limits: {
    fileSize: 100 * 1024 * 1024, // max filesize of 100mb
  },
  storage: Multer.memoryStorage(),
});

router.get('/download', async (req, res, next) => {
  const path = decodeURIComponent(req.query.url as string);
  const key = path.substring(path.lastIndexOf('.com/') + 5);
  logger.debug('Path: ' + path);
  logger.debug('key' + key);
  try {
    const downloadResponse = await s3Client.send(
      new GetObjectCommand({
        Bucket: bucket,
        Key: key,
      })
    );

    const image = await downloadResponse.Body.transformToByteArray();
    res.setHeader('Content-Type', 'image/jpeg');

    // DO NOT CHANGE THIS IMAGES GO AWAY
    res.status(200);
    res.write(image);
    res.end();
  } catch (e) {
    logger.error(e);
  }
});

router.post(
  '/upload', 
  multer.array('files'), 
  async (req: any, res: any, next: any) => {

    const path = req.body.path;
    var results = {success: false, photoUrl: ''};

    logger.debug(`image upload started, path = ${path}`);
    if (!req.body.files || req.body.files.length === 0) {
      logger.debug('No files found');
      return next();
    }

    try {
        const key = v4();
        const photoUrl = await uploadToBucket({
          directory: `${buildEnvironment}/${path}`,
          photo: req.body.files,
          key: key,
        });
      
      if (photoUrl) {
        logger.debug(`Uploaded file to ${photoUrl}`);

        results.success = true;
        results.photoUrl = photoUrl;
      } else {
        throw Error('Photo url is null, image likely failed to upload');
      }
      
    } catch (uploadError) {
      logger.error(uploadError);
      next(uploadError);
    }
    res.json(results);
    res.status(200);
    res.end();
  }
);

export default router;
