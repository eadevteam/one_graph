import bodyParser from 'body-parser';
import express, { NextFunction, Request, response, Response } from 'express';
import https from 'https';
import { caEtrmToken, logger, pgPool } from '../config';
import axios from 'axios';
import qs from 'qs';
import { report } from 'process';
import FormData from 'form-data';

const router = express.Router();
router.use(bodyParser.json());

/**
 * Given a statewide workpaper Id in the format of SWCR005, find the latest version of the paper
 * and return a list of all permutations available from the ETRM database.
 * Returns a list of permutations, with each permutation in the format of:
*    {
      "MeasureID": "SWCR005",
      "MeasureVersionID": "SWCR005–03",
      "MeasureName": "Auto Closer for Refrigerated Storage Door",
      "OfferingID": "A",
      "BaseCase1st": "No auto door closer, cooler, CZ02",
      "BaseCase2nd": null,
      "MeasDescription": "Auto door closer, cooler, CZ02",
      "PreDesc": "No auto door closer, cooler",
      "StdDesc": "No auto door closer, cooler",
      "MeasAppType": "AOE",
      "BldgType": "Asm",
      "BldgVint": "Any",
      "BldgLoc": "CZ02",
      "NormUnit": "Each",
      "Sector": "Com",
      "PAType": "Any",
      "PA": "Any",
      "UnitkW1stBaseline": 0.272,
      "UnitkWh1stBaseline": 1410,
      "UnitTherm1stBaseline": –0.0136,
      "UnitkW2ndBaseline": 0,
      "UnitkWh2ndBaseline": 0,
      "UnitTherm2ndBaseline": 0,
      "UnitLabCost1stBaseline": 0,
      "UnitMatCost1stBaseline": 0,
      "UnitMeaCost1stBaseline": 478.46,
      "UnitMeasLabCost": 82.56,
      "UnitMeasMatCost": 395.9,
      "UnitLabCost2ndBaseline": 0,
      "UnitMatCost2ndBaseline": 0,
      "UnitMeaCost2ndBaseline": 0,
      "LocCostAdj": "None",
      "EUL_ID": "GrocWlkIn–DrClsr",
      "EUL_Yrs": 6.7,
      "RUL_ID": "HVAC–Chlr",
      "RUL_Yrs": 0,
      "Life1stBaseline": 6.7,
      "Life2ndBaseline": 0,
      "UECkWBase1": 258,
      "UECkWhBase1": 1640000,
      "UECThermBase1": 23000,
      "UECkWBase2": 0,
      "UECkWhBase2": 0,
      "UECThermBase2": 0,
      "UECkWMeas": 258,
      "UECkWhMeas": 1630000,
      "UECThermMeas": 23000,
      "DeliveryType": "DnDeemDI",
      "NTG_ID": "Com–Default>2yrs",
      "NTGRkWh": 0.6,
      "NTGRkW": 0.6,
      "NTGRTherm": 0.6,
      "NTGRCost": 0.6,
      "GSIA_ID": "Def–GSIA",
      "GSIA": 1,
      "E3MeaElecEndUseShape": "DEER:HVAC_Chillers",
      "E3GasSavProfile": "Annual",
      "UnitGasInfraBens": 0,
      "UnitRefrigCosts": 0,
      "UnitRefrigBens": 0,
      "UnitMiscCosts": 0,
      "MiscCostsDesc": null,
      "UnitMiscBens": 0,
      "MiscBensDesc": null,
      "MarketEffectsBenefits": null,
      "MarketEffectsCosts": null,
      "MeasInflation": null,
      "CombustionType": null,
      "MeasImpactCalcType": "Standard",
      "Upstream_Flag": false,
      "Version": "ExAnte2023",
      "VersionSource": "IOU workpaper",
      "ElecBen": 557.31,
      "GasBen": –0.06,
      "TRCCostNoAdmin": 287.08,
      "PACCostNoAdmin": 0,
      "TRCRatioNoAdmin": 1.94,
      "PACRatioNoAdmin": 0,
      "TotalSystemBenefit": 557.25,
      "OtherBen": 0,
      "OtherCost": 0,
      "WaterUse": null,
      "UnitGalWater1stBaseline": 0,
      "UnitGalWater2ndBaseline": null,
      "UnitkWhIOUWater1stBaseline": null,
      "UnitkWhIOUWater2ndBaseline": null,
      "UnitkWhTotalWater1stBaseline": null,
      "UnitkWhTotalWater2ndBaseline": null,
      "MeasTechID": null,
      "PreTechID": null,
      "StdTechID": null,
      "TechGroup": "Ref_Storage",
      "PreTechGroup": "Ref_Storage",
      "StdTechGroup": "Ref_Storage",
      "TechType": "WalkInDoor",
      "PreTechType": "WalkInDoor",
      "StdTechType": "WalkInDoor",
      "UseCategory": "ComRefrig",
      "UseSubCategory": "Storage",
      "BldgHVAC": "cWtd",
      "ETP_Flag": null,
      "ETP_YearFirstIntroducedToPrograms": null,
      "IE_Applicable": false,
      "IETableName": "NA",
      "MeasQualifier": "None",
      "DEER_MeasureID": null,
      "MeasCostID": null,
      "MeasImpactType": "Deem–WP",
      "OfferingDesc": "Auto door closer, cooler",
      "SourceDesc": "SWCR005–03",
      "PALead": "SCE",
      "StartDate": "2023–01–01",
      "EndDate": null,
      "MeasDetailID": "SWCR005–03–A–AOE–DnDeemDI–Com–Asm–Any–cWtd–CZ02–Com–Default>2yrs–Def–GSIA–Any–Any"
    }
 */

router.post(
  '/:statewideId',
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      console.log(
        `Requesting ETRM data for Statewide ID ${req.params.statewideId}`
      );

      console.log(req.body);

      const permutation = await getPermutationDatabase(
        req.params.statewideId,
        req.body.buildingType,
        req.body.climateZone,
        req.body.offeringId
      );
      res.json(permutation);

      // const etrmHttpOptions = {
      //   headers: {
      //     Authorization: `Token ${caEtrmToken}`,
      //   },
      // };

      // const versionResponse = await axios.get(
      //   `https://www.caetrm.com/api/v1/measures/${req.params.statewideId}`,
      //   {
      //     headers: {
      //       Authorization: `Token ${caEtrmToken}`,
      //     },
      //   }
      // );

      // if (versionResponse.status === 200) {
      //   const latestVersion =
      //     versionResponse.data.versions[0].version.split('–')[1];
      //   const permutationResponse = await axios.get(
      //     `https://www.caetrm.com/api/v1/measures/${req.params.statewideId}/${latestVersion}/permutations`,
      //     {
      //       headers: {
      //         Authorization: `Token ${caEtrmToken}`,
      //       },
      //     }
      //   );

      //   if (permutationResponse.status === 200) {
      //     const columnHeaders = permutationResponse.data.headers;

      //     // Sample prettified response
      //     const prettyPermutations = permutationResponse.data.results.map(
      //       (entry: any) => {
      //         let prettyEntry = {};
      //         columnHeaders.forEach((header: string, index: number) => {
      //           prettyEntry = { ...prettyEntry, ...{ [header]: entry[index] } };
      //         });
      //         return prettyEntry;
      //       }
      //     );

      //     // BldgVintage (Take Ex over Any, never match Old/Rec)

      //     res.json(prettyPermutations);
      //   } else {
      //     res.json({
      //       message: `Error getting permutations for ${req.params.statewideId}`,
      //       status: permutationResponse.status,
      //       etrmStatusMessage: permutationResponse.statusText,
      //     });
      //   }
      // } else {
      //   res.json({
      //     message: `Error getting available versions for ${req.params.statewideId}`,
      //     status: versionResponse.status,
      //     etrmStatusMessage: versionResponse.statusText,
      //   });
      // }
    } catch (err) {
      logger.error(err);
      next(err);
    }
  }
);

async function getPermutationDatabase(
  statewideId: string,
  buildingType: string,
  climateZone: string,
  offeringId: string
) {
  const filters = [{ key: 'key', value: 'value' }];

  const client = await pgPool.connect();
  try {
    logger.debug(buildingType);
    logger.debug(climateZone);
    logger.debug(offeringId);
    const res = await client.query(
      `SELECT * FROM permutations WHERE "MeasureVersionID" = $1 and "BldgType" = $2 and "BldgLoc"= $3 and "OfferingID" = $4 and "DeliveryType" = 'DnDeemed' `,
      [statewideId, buildingType, climateZone, offeringId]
    );
    logger.debug(res.rows);
    return res.rows[0];
  } catch (err) {
    console.error(err.stack);
    return `Error querying ETRM database: ${err}`;
  } finally {
    client.release();
  }
}

// async function getPermutationWebsite(statewideId: string, version: string) {
//   const filters = [{ key: 'key', value: 'value' }];

//   const browser = await puppeteer.launch();
//   const page = await browser.newPage();

//   await page.setUserAgent(
//     'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36'
//   );

//   await page.goto('https://www.caetrm.com');

//   const usernameSelector = 'input[name=username]';
//   const passwordSelector = 'input[name=password]';

//   await page.type(usernameSelector, 'stan.pratt@tapersolutions.com');
//   await page.type(passwordSelector, 'i7K1U0GYQxhbFGg');
  
//   const loginButtonSelector = '.login--submit';
//   await page.waitForSelector(loginButtonSelector);
//   await page.click(loginButtonSelector);

//   await page.waitForSelector('text/Dashboard');
//   const cookies = await page.cookies();

//   const sessionId = cookies.find((cookie) => cookie.name === 'sessionid').value;
//   const csrfToken = cookies.find((cookie) => cookie.name === 'csrftoken').value;

//   await page.goto(
//     `https://www.caetrm.com/measure/${statewideId}/${version}/permutation-reports/`
//   );

//   const reportButtonSelector = 'text/Create a report';
//   const reportBtn = await page.waitForSelector(reportButtonSelector);
//   await page.click(reportButtonSelector);

//   const csrfMiddlewareToken = decodeURIComponent(csrfToken);

//   const createReportUrl = await reportBtn.evaluate((el) => {
//     if (el instanceof HTMLAnchorElement) return el.href;
//   });

//   await browser.close();

//   const formData = new FormData();
//   formData.append('csrfmiddlewaretoken', csrfMiddlewareToken);
//   formData.append('display_fields_form_field', 'statewide_measure_id');
//   formData.append('display_fields_form_field', 'name');
//   formData.append('display_fields_form_field', 'version_string');
//   formData.append('display_fields_form_field', 'ds_offering_id');
//   formData.append('_filter_measure_detail_id', '');
//   formData.append('_filter_statewide_measure_id', '');
//   formData.append('_filter_name', '');
//   formData.append('_filter_ds_offering_id', 'B');
//   formData.append('_filter_ds_base_case_1st', '');
//   formData.append('_filter_ds_base_case_2nd', '');
//   formData.append('_filter_ds_measure_case', '');
//   formData.append('_filter_ds_existing_desc', '');
//   formData.append('_filter_ds_standard_Desc', '');
//   formData.append('_filter_ds_measure_app_type', '');
//   formData.append('_filter_ds_bldg_type', '');
//   formData.append('_filter_ds_bldg_vint', '');
//   formData.append('_filter_ds_bldg_loc', '');
//   formData.append('_filter_ds_norm_unit', '');
//   formData.append('_filter_ds_sector', '');
//   formData.append('_filter_ds_pa_type', '');
//   formData.append('_filter_ds_deliv_type', '');
//   formData.append('_filter_ds_ntg_id', '');
//   formData.append('_filter_ds_gsia_id', '');
//   formData.append('_filter_ds_elec_impact_profile_id', '');
//   formData.append('_filter_ds_gas_impact_profile_id', '');
//   formData.append('_filter_ds_unit_gas_infrastructure_benefits', '');
//   formData.append('_filter_ds_unit_refrigerant_costs', '');
//   formData.append('_filter_ds_unit_refrigerant_benefits', '');
//   formData.append('_filter_ds_unit_misc_costs', '');
//   formData.append('_filter_ds_unit_misc_cost_desc', '');
//   formData.append('_filter_ds_unit_misc_benefits', '');
//   formData.append('_filter_ds_unit_misc_benefits_desc', '');
//   formData.append('_filter_ds_market_effects_benefits', '');
//   formData.append('_filter_ds_market_effects_costs', '');
//   formData.append('_filter_ds_meas_inflation', '');
//   formData.append('_filter_ds_combustion_type', '');
//   formData.append('_filter_ds_meas_impact_calc_type', '');
//   formData.append('_filter_ds_tech_group', '');
//   formData.append('_filter_ds_tech_type', '');
//   formData.append('_filter_ds_use_category', '');
//   formData.append('_filter_ds_use_sub_category', '');
//   formData.append('_filter_ds_bldg_hvac', '');
//   formData.append('_filter_ds_etp_flag', '');
//   formData.append('_filter_ds_etp_first_year_introduced_to_programs', '');
//   formData.append('_filter_ds_ie_table_name', '');
//   formData.append('_filter_ds_meas_qualifier', '');
//   formData.append('_filter_ds_energy_impact_id', '');
//   formData.append('_filter_ds_meas_cost_id', '');
//   formData.append('_filter_p.caseType__label', '');
//   formData.append('_filter_p.MeasAppType__label', '');
//   formData.append('_filter_p.BldgType__label', '');
//   formData.append('_filter_p.BldgVint__label', '');
//   formData.append('_filter_p.BldgLoc__label', '');
//   formData.append('_filter_p.BldgHVAC__label', '');
//   formData.append('_filter_p.Sector__label', '');
//   formData.append('_filter_p.EULID__label', '');
//   formData.append('_filter_p.hostEULID__label', '');
//   formData.append('_filter_p.eULBldgType__label', '');
//   formData.append('_filter_p.PAType__label', '');
//   formData.append('_filter_p.PA__label', '');
//   formData.append('_filter_p.DelivType__label', '');
//   formData.append('_filter_p.GSIAID__label', '');
//   formData.append('_filter_p.NTGID__label', '');
//   formData.append('_filter_p.electricImpactProfileID__label', '');
//   formData.append('_filter_p.GasImpactProfileID__label', '');
//   formData.append('_filter_p.NormUnit__label', '');
//   formData.append('_filter_p.TechGroup__label', '');
//   formData.append('_filter_p.ExTechGroup__label', '');
//   formData.append('_filter_p.StdTechGroup__label', '');
//   formData.append('_filter_p.TechType__label', '');
//   formData.append('_filter_p.ExTechType__label', '');
//   formData.append('_filter_p.StdTechType__label', '');
//   formData.append('_filter_p.UseCategory__label', '');
//   formData.append('_filter_p.UseSubCategory__label', '');
//   formData.append('_filter_p.CostAdjustType__label', '');
//   formData.append('_filter_p.MeasImpactCalcType__label', '');
//   formData.append('_filter_p.MeasImpactType__label', '');
//   formData.append('_filter_p.MeasQualifier__label', '');
//   formData.append('_filter_p.version__label', '');
//   formData.append('_filter_p.versionSourceID__label', '');
//   formData.append('_filter_offerId__ID', '');
//   formData.append('_filter_offerId__measOfferDesc', '');
//   formData.append('_filter_measOffer__descBase1', '');
//   formData.append('_filter_measOffer__descMeas', '');
//   formData.append('_filter_implementationEligibility__available', '');
//   formData.append('_filter_description__ID', '');
//   formData.append('_filter_description__Ex', '');
//   formData.append('_filter_description__Std', '');
//   formData.append('_filter_EUL__description', '');
//   formData.append('_filter_EUL__hoursOfUseCategory', '');
//   formData.append('_filter_EUL__useCategory', '');
//   formData.append('_filter_EUL__useSubCategory', '');
//   formData.append('_filter_EUL__technologyGroup', '');
//   formData.append('_filter_EUL__technologyType', '');
//   formData.append('_filter_EUL__BasisType', '');
//   formData.append('_filter_EUL__BasisValue', '');
//   formData.append('_filter_EUL__sector', '');
//   formData.append('_filter_EUL__version', '');
//   formData.append('_filter_EUL__proposedFlag', '');
//   formData.append('_filter_EUL__expiryDate', '');
//   formData.append('_filter_EUL__startDate', '');
//   formData.append('_filter_hostEulAndRul__description', '');
//   formData.append('_filter_hostEulAndRul__startDate', '');
//   formData.append('_filter_hostEulAndRul__expiryDate', '');
//   formData.append('_filter_hostEulAndRul__proposedFlag', '');
//   formData.append('_filter_hostEulAndRul__version', '');
//   formData.append('_filter_hostEulAndRul__sector', '');
//   formData.append('_filter_hostEulAndRul__useCategory', '');
//   formData.append('_filter_hostEulAndRul__useSubCategory', '');
//   formData.append('_filter_hostEulAndRul__technologyGroup', '');
//   formData.append('_filter_hostEulAndRul__technologyType', '');
//   formData.append('_filter_hostEulAndRul__BasisType', '');
//   formData.append('_filter_hostEulAndRul__BasisValue', '');
//   formData.append('_filter_hostEulAndRul__hoursOfUseCategory', '');
//   formData.append('_filter_Null__NA', '');
//   formData.append('_filter_Null__Blank', '');
//   formData.append('outcome', '100');
//   formData.append('name', '');
//   formData.append(
//     '_selected_filters',
//     '{"measure_detail_id": {"name": "measure_detail_id", "label": "Measure Detail ID", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "statewide_measure_id": {"name": "statewide_measure_id", "label": "Statewide Measure ID", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "name": {"name": "name", "label": "Measure Name", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_offering_id": {"name": "ds_offering_id", "label": "Offering Id", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_base_case_1st": {"name": "ds_base_case_1st", "label": "First Base Case Description", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_base_case_2nd": {"name": "ds_base_case_2nd", "label": "Second Base Case Description", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_measure_case": {"name": "ds_measure_case", "label": "Measure Case Description", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_existing_desc": {"name": "ds_existing_desc", "label": "Existing Description", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_standard_Desc": {"name": "ds_standard_Desc", "label": "Standard Description", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_measure_app_type": {"name": "ds_measure_app_type", "label": "Measure Application Type", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_bldg_type": {"name": "ds_bldg_type", "label": "BldgType", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_bldg_vint": {"name": "ds_bldg_vint", "label": "BldgVint", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_bldg_loc": {"name": "ds_bldg_loc", "label": "BldgingLoc", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_norm_unit": {"name": "ds_norm_unit", "label": "NormUnit", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_sector": {"name": "ds_sector", "label": "Sector", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_pa_type": {"name": "ds_pa_type", "label": "Program Administrator Type", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_deliv_type": {"name": "ds_deliv_type", "label": "Delivery Type", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_ntg_id": {"name": "ds_ntg_id", "label": "Net to Gross Ratio ID", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_gsia_id": {"name": "ds_gsia_id", "label": "GSIA ID", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_elec_impact_profile_id": {"name": "ds_elec_impact_profile_id", "label": "Electric Impact Profile ID", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_gas_impact_profile_id": {"name": "ds_gas_impact_profile_id", "label": "Gas Impact Profile ID", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_unit_gas_infrastructure_benefits": {"name": "ds_unit_gas_infrastructure_benefits", "label": "Unit Gas Infrastructure Benefits", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_unit_refrigerant_costs": {"name": "ds_unit_refrigerant_costs", "label": "Unit Refrigerant Costs", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_unit_refrigerant_benefits": {"name": "ds_unit_refrigerant_benefits", "label": "Unit Refrigerant Benefits", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_unit_misc_costs": {"name": "ds_unit_misc_costs", "label": "Unit Miscellaneous Costs", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_unit_misc_cost_desc": {"name": "ds_unit_misc_cost_desc", "label": "Miscellaneous Cost Description", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_unit_misc_benefits": {"name": "ds_unit_misc_benefits", "label": "Unit Miscellaneous Benefits", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_unit_misc_benefits_desc": {"name": "ds_unit_misc_benefits_desc", "label": "Miscellaneous Benefits Description", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_market_effects_benefits": {"name": "ds_market_effects_benefits", "label": "Market Effects Benefits", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_market_effects_costs": {"name": "ds_market_effects_costs", "label": "Market Effects Costs", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_meas_inflation": {"name": "ds_meas_inflation", "label": "Measure Inflation", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_combustion_type": {"name": "ds_combustion_type", "label": "Combustion Type", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_meas_impact_calc_type": {"name": "ds_meas_impact_calc_type", "label": "Measure Impact Type", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_tech_group": {"name": "ds_tech_group", "label": "Technology Group", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_tech_type": {"name": "ds_tech_type", "label": "Technology Type", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_use_category": {"name": "ds_use_category", "label": "Use Category", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_use_sub_category": {"name": "ds_use_sub_category", "label": "Use Subcategory", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_bldg_hvac": {"name": "ds_bldg_hvac", "label": "Building HVAC", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_etp_flag": {"name": "ds_etp_flag", "label": "ETP Flag", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_etp_first_year_introduced_to_programs": {"name": "ds_etp_first_year_introduced_to_programs", "label": "ETP First Year Introduced to Programs", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_ie_table_name": {"name": "ds_ie_table_name", "label": "IE Table Name", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_meas_qualifier": {"name": "ds_meas_qualifier", "label": "Measure Qualifier", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_energy_impact_id": {"name": "ds_energy_impact_id", "label": "DEER Measure ID", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "ds_meas_cost_id": {"name": "ds_meas_cost_id", "label": "Measure Cost ID", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": true, "filter_with_nullable_semantics": false}, "p.caseType__label": {"name": "p.caseType__label", "label": "Equipment Type", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.MeasAppType__label": {"name": "p.MeasAppType__label", "label": "Measure Application Type", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.BldgType__label": {"name": "p.BldgType__label", "label": "Building Type", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.BldgVint__label": {"name": "p.BldgVint__label", "label": "Building Vintage", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.BldgLoc__label": {"name": "p.BldgLoc__label", "label": "Building Location", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.BldgHVAC__label": {"name": "p.BldgHVAC__label", "label": "Building HVAC", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.Sector__label": {"name": "p.Sector__label", "label": "Sector", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.EULID__label": {"name": "p.EULID__label", "label": "Effective Useful Life ID", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.hostEULID__label": {"name": "p.hostEULID__label", "label": "Host EUL ID", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.eULBldgType__label": {"name": "p.eULBldgType__label", "label": "EUL-Bldg Type", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.PAType__label": {"name": "p.PAType__label", "label": "Program Administrator Type", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.PA__label": {"name": "p.PA__label", "label": "Program Administrator", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.DelivType__label": {"name": "p.DelivType__label", "label": "Delivery Type", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.GSIAID__label": {"name": "p.GSIAID__label", "label": "GSIA ID", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.NTGID__label": {"name": "p.NTGID__label", "label": "Net to Gross Ratio ID", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.electricImpactProfileID__label": {"name": "p.electricImpactProfileID__label", "label": "Electric Impact Profile ID", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.GasImpactProfileID__label": {"name": "p.GasImpactProfileID__label", "label": "Gas Impact Profile ID", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.NormUnit__label": {"name": "p.NormUnit__label", "label": "Normalizing Unit", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.TechGroup__label": {"name": "p.TechGroup__label", "label": "Technology Group", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.ExTechGroup__label": {"name": "p.ExTechGroup__label", "label": "Pre-existing Tech Group", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.StdTechGroup__label": {"name": "p.StdTechGroup__label", "label": "Standard Tech Group", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.TechType__label": {"name": "p.TechType__label", "label": "Technology Type", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.ExTechType__label": {"name": "p.ExTechType__label", "label": "Pre-existing Tech Type", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.StdTechType__label": {"name": "p.StdTechType__label", "label": "Standard Tech Type", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.UseCategory__label": {"name": "p.UseCategory__label", "label": "Use Category", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.UseSubCategory__label": {"name": "p.UseSubCategory__label", "label": "Use Sub Category", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.CostAdjustType__label": {"name": "p.CostAdjustType__label", "label": "Cost Adjustment Types", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.MeasImpactCalcType__label": {"name": "p.MeasImpactCalcType__label", "label": "Measure Impact Calculation Type", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.MeasImpactType__label": {"name": "p.MeasImpactType__label", "label": "Measure Impact Type", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.MeasQualifier__label": {"name": "p.MeasQualifier__label", "label": "Measure Qualifier", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.version__label": {"name": "p.version__label", "label": "Version", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "p.versionSourceID__label": {"name": "p.versionSourceID__label", "label": "Version Source ID", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "offerId__ID": {"name": "offerId__ID", "label": "Offering ID: Statewide Measure Offering ID", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "offerId__measOfferDesc": {"name": "offerId__measOfferDesc", "label": "Offering ID: Measure Offering Description", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "measOffer__descMeas": {"name": "measOffer__descMeas", "label": "Measure Offerings: Measure Case Description", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "measOffer__descBase1": {"name": "measOffer__descBase1", "label": "Measure Offerings: First Base Case Description", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "implementationEligibility__available": {"name": "implementationEligibility__available", "label": "Implementation Eligibility: Available?", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "description__ID": {"name": "description__ID", "label": "Base Case Descriptions: Statewide Measure Offering ID", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "description__Ex": {"name": "description__Ex", "label": "Base Case Descriptions: Existing Description", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "description__Std": {"name": "description__Std", "label": "Base Case Descriptions: Standard Description", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "EUL__BasisType": {"name": "EUL__BasisType", "label": "Effective Useful Life and Remaining Useful Life: Basis Type", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "EUL__description": {"name": "EUL__description", "label": "Effective Useful Life and Remaining Useful Life: Description", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "EUL__hoursOfUseCategory": {"name": "EUL__hoursOfUseCategory", "label": "Effective Useful Life and Remaining Useful Life: Hours-of-use Category", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "EUL__startDate": {"name": "EUL__startDate", "label": "Effective Useful Life and Remaining Useful Life: Start Date", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "EUL__expiryDate": {"name": "EUL__expiryDate", "label": "Effective Useful Life and Remaining Useful Life: Expire Date", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "EUL__proposedFlag": {"name": "EUL__proposedFlag", "label": "Effective Useful Life and Remaining Useful Life: Proposed Flag", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "EUL__BasisValue": {"name": "EUL__BasisValue", "label": "Effective Useful Life and Remaining Useful Life: Basis Value", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "EUL__version": {"name": "EUL__version", "label": "Effective Useful Life and Remaining Useful Life: Version", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "EUL__sector": {"name": "EUL__sector", "label": "Effective Useful Life and Remaining Useful Life: Sector", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "EUL__useCategory": {"name": "EUL__useCategory", "label": "Effective Useful Life and Remaining Useful Life: Use Category", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "EUL__useSubCategory": {"name": "EUL__useSubCategory", "label": "Effective Useful Life and Remaining Useful Life: Use Sub-Category", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "EUL__technologyGroup": {"name": "EUL__technologyGroup", "label": "Effective Useful Life and Remaining Useful Life: Technology Group", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "EUL__technologyType": {"name": "EUL__technologyType", "label": "Effective Useful Life and Remaining Useful Life: Technology Type", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "hostEulAndRul__hoursOfUseCategory": {"name": "hostEulAndRul__hoursOfUseCategory", "label": "Effective Useful Life and Remaining Useful Life \u2013 Host: Hours-of-use Category", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "hostEulAndRul__description": {"name": "hostEulAndRul__description", "label": "Effective Useful Life and Remaining Useful Life \u2013 Host: Description", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "hostEulAndRul__version": {"name": "hostEulAndRul__version", "label": "Effective Useful Life and Remaining Useful Life \u2013 Host: Version", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "hostEulAndRul__sector": {"name": "hostEulAndRul__sector", "label": "Effective Useful Life and Remaining Useful Life \u2013 Host: Sector", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "hostEulAndRul__useCategory": {"name": "hostEulAndRul__useCategory", "label": "Effective Useful Life and Remaining Useful Life \u2013 Host: Use Category", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "hostEulAndRul__useSubCategory": {"name": "hostEulAndRul__useSubCategory", "label": "Effective Useful Life and Remaining Useful Life \u2013 Host: Use Sub-Category", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "hostEulAndRul__technologyGroup": {"name": "hostEulAndRul__technologyGroup", "label": "Effective Useful Life and Remaining Useful Life \u2013 Host: Technology Group", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "hostEulAndRul__technologyType": {"name": "hostEulAndRul__technologyType", "label": "Effective Useful Life and Remaining Useful Life \u2013 Host: Technology Type", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "hostEulAndRul__BasisType": {"name": "hostEulAndRul__BasisType", "label": "Effective Useful Life and Remaining Useful Life \u2013 Host: Basis Type", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "hostEulAndRul__BasisValue": {"name": "hostEulAndRul__BasisValue", "label": "Effective Useful Life and Remaining Useful Life \u2013 Host: Basis Value", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "hostEulAndRul__startDate": {"name": "hostEulAndRul__startDate", "label": "Effective Useful Life and Remaining Useful Life \u2013 Host: Start Date", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "hostEulAndRul__expiryDate": {"name": "hostEulAndRul__expiryDate", "label": "Effective Useful Life and Remaining Useful Life \u2013 Host: Expire Date", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "hostEulAndRul__proposedFlag": {"name": "hostEulAndRul__proposedFlag", "label": "Effective Useful Life and Remaining Useful Life \u2013 Host: Proposed Flag", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "Null__NA": {"name": "Null__NA", "label": "Null Values: Not Applicable", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}, "Null__Blank": {"name": "Null__Blank", "label": "Null Values: Blank", "filter_type": "text", "choices": [], "saved_data": null, "is_data_spec_field": false, "filter_with_nullable_semantics": false}}'
//   );

//   const reportCreatedResponse = await axios.post(createReportUrl, formData, {
//     headers: {
//       Origin: 'https://www.caetrm.com',
//       'Sec-Fetch-Site': 'same-origin',
//       'User-Agent':
//         'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36',
//       'Content-Type': 'application/x-www-form-urlencoded',
//       Cookie: `csrftoken=${csrfToken}; sessionid=${sessionId}`,
//       Referer: createReportUrl,
//     },
//   });

//   logger.debug(reportCreatedResponse.request.data);

//   // magic number lives on the HTML response to the above call
//   // top.window.location.href = "/measure/SWCR005/03/permutation–report–detail/580/";
//   const text = reportCreatedResponse.data;
//   const nextApiCallUrl = `top.window.location.href = "`;
//   const startIndex = text.lastIndexOf(nextApiCallUrl) + nextApiCallUrl.length;
//   const lastIndex = text.lastIndexOf('";');
//   const viewReportUrl = `https://www.caetrm.com${text.substring(
//     startIndex,
//     lastIndex
//   )}`;

//   const permutationResponse = await axios.get(viewReportUrl, {
//     headers: {
//       Origin: 'https://www.caetrm.com',
//       'Sec-Fetch-Site': 'same-origin',
//       'User-Agent':
//         'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36',
//       'Content-Type': 'application/x-www-form-urlencoded',
//       Cookie: `csrftoken=${csrfToken}; sessionid=${sessionId}`,
//       Referer: viewReportUrl,
//     },
//   });
//   const permutation = permutationResponse.data;

//   return permutation;
// }

export default router;
