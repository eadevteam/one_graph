import express, { NextFunction, Request, Response } from 'express';
import {
  logger,
} from '../config';
import { PGEShareMyDataIntegration } from '../utility_integrations/pge-sharemydata';

// express router                      //
const router = express.Router();

// pge share my data api implementation //
const pgeIntegration = new PGEShareMyDataIntegration();
/**
 * Handles retrieving authorization from a PGE customer for us to pull
 * usage data via PGE's ShareMyData implementation of GreenButton
 **/
router.post(
  '/approve-integration',
  async (req: Request, res: Response, next: NextFunction) => {
    const results = await pgeIntegration.approveIntegration(req.body.queue_item_id);
    logger.debug(JSON.stringify(results));
    res.json(results);
  }
);

/**
 * Retrieve usage data from a property using PGE's share my data API.
 **/
router.get(
  '/usage/interval',
  async (req: Request, res: Response, next: NextFunction) => {
    const usagePointId = req.query.usage_point_id;
    const intervalStart = req.query.start;
    const intervalEnd = req.query.end;
    const retailCustomerId = req.query.retail_customer_id;

    const results = await pgeIntegration.getUsageInterval(
      usagePointId,
      intervalStart,
      intervalEnd,
      retailCustomerId
    )
    logger.debug(JSON.stringify(results));
    res.json(results);
  }
);

router.get(
  '/process-notification/:notificationId',
  async (req: Request, res: Response, next: NextFunction) => {
    const notificationId = req.params.notificationId;
    const result = await pgeIntegration.processNotification(notificationId);
    res.json(result);
  }
);

export default router;
