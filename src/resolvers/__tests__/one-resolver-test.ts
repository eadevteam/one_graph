import { ApolloServer } from '@apollo/server';
import { typeDefs } from '../../schema';
import { appResolver } from '../one-resolver';
import { expect, jest, test } from '@jest/globals';
import assert from 'assert';
import {
  Properties,
  PropertyDocument,
} from '../../datasources/properties-datasource-mongo';
import { Surveys } from '../../datasources/surveys-datasource-mongo';
import { UtilityDataIntegration } from '../../datasources/utilitydataintegration-datasource-mongo';
import { oneMongoClient } from '../../config';

// Same setup as running the app
const testServer = new ApolloServer({
  typeDefs,
  resolvers: [appResolver],
});

// Setup a datasource w/ fake connection to db
// We can probably make this more fake
const propertiesDatasource = new Properties({
  modelOrCollection: oneMongoClient.db('fakeDb').collection('properties'),
});
const surveysDatasource = new Surveys({
  modelOrCollection: oneMongoClient.db('fakeDb').collection('surveys'),
});
const utilityDataIntegrationsDatasource = new UtilityDataIntegration({
  modelOrCollection: oneMongoClient.db('fakeDb').collection('utilityDataIntegrations'),
});

const testPropertyInput = {
  propertyId: 'test',
  property: {
    zip_code: 95832,
    utility_data_integrations: ['randomudiid'],
    time_last_modified: null,
    time_created: null,
    surveys: ['somerandomID'],
    state: 'CA',
    contact: 'email@addresss.com',
    company: {
      id: '19509218665',
      name: 'A Test Company',
    },
    city: 'Santa Cruz',
    banner: 'A Test Banner',
    address_2: '555 Lake Drive',
  },
};

const testProperty = {
  zip_code: 95832,
  time_last_modified: null,
  time_created: null,
  state: 'CA',
  company: {
    id: '19509218665',
    name: 'A Test Company',
  },
  city: 'Santa Cruz',
  banner: 'A Test Banner',
  address_2: '555 Lake Drive',
  surveys: ['somerandomID'],
  utility_data_integrations: ['randomudiid'],
};

// Setting up the mock response for addProperty
propertiesDatasource.addProperty = jest.fn().mockImplementation(async () => {
  return { data: [testProperty], success: true };
}) as jest.Mock<any>;
propertiesDatasource.getProperty = jest.fn().mockImplementation(async () => {
  return { data: [testProperty], success: true };
}) as jest.Mock<any>;
propertiesDatasource.deleteProperty = jest.fn().mockImplementation(async () => {
  return { data: [], success: true };
}) as jest.Mock<any>;
surveysDatasource.deleteSurvey = jest.fn().mockImplementation(async () => {
  return { data: [], success: true };
}) as jest.Mock<any>;
utilityDataIntegrationsDatasource.deleteUtilityDataIntegration = 
  jest.fn().mockImplementation(async () => {
    return { data: [], success: true };
  }) as jest.Mock<any>;


test('Test property', async () => {
  // Add a property with mutation
  const addResult = await testServer.executeOperation(
    {
      query: `mutation AddProperty($property: PropertyInput) {
      addProperty(property: $property) {
        success
        message
        data {
          zip_code
          time_last_modified
          time_created
          state
          company
          city
          banner
          address_2
        }
      }
    }`,
      variables: testPropertyInput,
    },
    {
      contextValue: {
        dataSources: {
          // Attach your fake + mocked datasource here
          properties: propertiesDatasource,
          surveys: surveysDatasource,
          utilityDataIntegrations: utilityDataIntegrationsDatasource,
        },
      },
    }
  );

  // Note the use of Node's assert rather than Jest's expect; if using
  // TypeScript, `assert`` will appropriately narrow the type of `body`
  // and `expect` will not.
  expect(propertiesDatasource.addProperty).toHaveBeenCalled();
  // delete the property
  const deleteResult = await testServer.executeOperation(
    {
      query: `mutation DeleteProperty($deletePropertyId: ID!) {
        deleteProperty(id: $deletePropertyId) {
          success
          message
          data {
            _id
          }
        }
      }`,
      variables: { deletePropertyId: 'fakeid' },
    },
    {
      contextValue: {
        dataSources: {
          properties: propertiesDatasource,
          surveys: surveysDatasource,
          utilityDataIntegrations: utilityDataIntegrationsDatasource,
        },
      },
    }
  );
  expect(propertiesDatasource.deleteProperty).toHaveBeenCalled();
  expect(surveysDatasource.deleteSurvey).toHaveBeenCalled();
  expect(
    utilityDataIntegrationsDatasource.deleteUtilityDataIntegration
  ).toHaveBeenCalled();
  expect(deleteResult).toBeDefined;
});
