import { logger } from '../config';
import { dateScalar, jsonScalar } from '../scalars/custom_scalars';

export const appResolver = {
  Date: dateScalar,
  JSON: jsonScalar,
  Program: {
    line_items: async (parent: any, {}: any, { dataSources }: any) => {
      if (parent.line_items) {
        const lineItems = await dataSources.lineItems.getLineItemsById(
          parent.line_items
        );

        if (lineItems) {
          if (lineItems.success) {
            return lineItems.data;
          }
        }
      }
      /**
       * If a List type wraps a Non-Null type, and one of the elements of that list resolves to null,
       * then the entire list must resolve to null. If the List type is also wrapped in a Non-Null,
       * the field error continues to propagate upwards.
       */
      return null;
    },
    savings: async (_: any, {}: any, { dataSources }: any) => {
      if (_.savings) {
        const savings = await dataSources.savings.getAllSavingsByIds(_.savings);
        if (savings) {
          if (savings.success) {
            return savings.data;
          }
        }
      }

      return null;
    },
  },
  Portfolio: {
    projects: async (parent: any, {}: any, { dataSources }: any) => {
      const projects = await dataSources.projects.getProjectsByIds(
        parent.projects
      );

      if (projects.success) {
        return projects.data;
      } else {
        return [];
      }
    },
  },
  Property: {
    contact: async (_: any, {}: any, { dataSources }: any) => {
      const contact = await dataSources.people.getPerson(_.contact);
      if (contact.success) {
        return contact.data[0];
      } else {
        return null;
      }
    },
    surveys: async (parent: any, {}: any, { dataSources }: any) => {
      const surveys = await dataSources.surveys.getSurveysByIds(parent.surveys);

      if (surveys.success) {
        return surveys.data;
      } else {
        return [];
      }
    },
    utility_data_integrations: async (
      parent: any,
      {}: any,
      { dataSources }: any
    ) => {
      const integrations = parent.utility_data_integrations
        ? await dataSources.utilityDataIntegrations.getUtilityDataIntegrationsById(
            parent.utility_data_integrations
          )
        : { success: false };

      if (integrations.success) {
        return integrations.data;
      } else {
        return [];
      }
    },
  },
  Project: {
    creator: async (_: any, {}: any, { dataSources }: any) => {
      const creator = await dataSources.people.getPerson(_.creator);
      if (creator.success) {
        return creator.data[0];
      } else {
        return null;
      }
    },
    contact: async (_: any, {}: any, { dataSources }: any) => {
      const contact = await dataSources.people.getPerson(_.contact);
      if (contact.success) {
        return contact.data[0];
      } else {
        return null;
      }
    },
    manager: async (_: any, {}: any, { dataSources }: any) => {
      const manager = await dataSources.people.getPerson(_.manager);
      if (manager.success) {
        return manager.data[0];
      } else {
        return null;
      }
    },
    property: async (_: any, {}: any, { dataSources }: any) => {
      const property = await dataSources.properties.getProperty(_.property);
      if (property) {
        if (property.success) {
          return property.data[0];
        }
      }
      /**
       * If a List type wraps a Non-Null type, and one of the elements of that list resolves to null,
       * then the entire list must resolve to null. If the List type is also wrapped in a Non-Null,
       * the field error continues to propagate upwards.
       */
      return null;
    },
    partners: async (_: any, {}: any, { dataSources }: any) => {
      console.log('program ids ' + _.partners);
      if (_.partners !== undefined) {
        const partners = await dataSources.partners.getPartnersByIds(
          _.partners
        );

        if (partners) {
          if (partners.success) {
            return partners.data;
          } else {
            return [];
          }
        }
      }
      /**
       * If a List type wraps a Non-Null type, and one of the elements of that list resolves to null,
       * then the entire list must resolve to null. If the List type is also wrapped in a Non-Null,
       * the field error continues to propagate upwards.
       */
      return null;
    },
    programs: async (_: any, {}: any, { dataSources }: any) => {
      console.log('program ids ' + _.programs);
      if (_.programs !== undefined) {
        const programs = await dataSources.programs.getProgramsByIds(
          _.programs
        );

        if (programs) {
          if (programs.success) {
            return programs.data;
          } else {
            return [];
          }
        }
      }
      /**
       * If a List type wraps a Non-Null type, and one of the elements of that list resolves to null,
       * then the entire list must resolve to null. If the List type is also wrapped in a Non-Null,
       * the field error continues to propagate upwards.
       */
      return null;
    },
    current_stage: async (_: any, {}: any, { dataSources }: any) => {
      const stage = await dataSources.stages.getStage(_.current_stage);
      if (stage.success) {
        return stage.data[0];
      } else {
        return null;
      }
    },
    workflow: async (_: any, {}: any, { dataSources }: any) => {
      console.log('workflow ids ' + _.workflow);
      if (_.workflow !== undefined) {
        const workflow = await dataSources.stages.getStagesByIds(_.workflow);

        if (workflow) {
          if (workflow.success) {
            return workflow.data;
          } else {
            return [];
          }
        }
      }
      /**
       * If a List type wraps a Non-Null type, and one of the elements of that list resolves to null,
       * then the entire list must resolve to null. If the List type is also wrapped in a Non-Null,
       * the field error continues to propagate upwards.
       */
      return null;
    },
  },
  Partner: {
    people: async (_: any, {}: any, { dataSources }: any) => {
      const person = await dataSources.people.getPerson(_.people);
      if (person.success) {
        return person.data[0];
      } else {
        return null;
      }
    },
  },
  Savings: {
    line_items: async (_: any, {}: any, { dataSources }: any) => {
      if (_.line_items) {
        console.log('getting line_items ' + JSON.stringify(_.line_items));
        const lineItems = await dataSources.lineItems.getLineItemsById(
          _.line_items
        );

        if (lineItems) {
          if (lineItems.success) {
            return lineItems.data;
          }
        }
      }
      /**
       * If a List type wraps a Non-Null type, and one of the elements of that list resolves to null,
       * then the entire list must resolve to null. If the List type is also wrapped in a Non-Null,
       * the field error continues to propagate upwards.
       */
      return null;
    },
  },
  Task: {
    parent: async (_: any, {}: any, { dataSources }: any) => {
      const parent = await dataSources.tasks.getTask(_.parent);
      if (parent.success) {
        return parent.data[0];
      } else {
        return null;
      }
    },
  },
  Stage: {
    tasks: async (_: any, {}: any, { dataSources }: any) => {
      const tasks = await dataSources.tasks.getTasksByIds(_.tasks);
      if (tasks) {
        if (tasks.success) {
          return tasks.data;
        } else {
          return [];
        }
      }
      /**
       * If a List type wraps a Non-Null type, and one of the elements of that list resolves to null,
       * then the entire list must resolve to null. If the List type is also wrapped in a Non-Null,
       * the field error continues to propagate upwards.
       */
      return null;
    },
  },
  Query: {
    person: async (_: any, { id }: any, { dataSources }: any) =>
      dataSources.people.getPerson(id),
    personByCognitoUUID: async (
      _: any,
      { cognito_uuid }: any,
      { dataSources }: any
    ) => dataSources.people.getPersonByCognitoUUID(cognito_uuid),
    people: async (_: any, {}: any, { dataSources }: any) =>
      dataSources.people.getPeople(),
    program: async (_: any, { id }: any, { dataSources }: any) =>
      dataSources.programs.getProgram(id),
    programs: async (_: any, { id }: any, { dataSources }: any) =>
      dataSources.programs.getPrograms(),
    project: async (_: any, { id }: any, { dataSources }: any) =>
      dataSources.projects.getProject(id),
    projects: async (_: any, { id }: any, { dataSources }: any) =>
      await dataSources.projects.getAllProjects(),
    projectsByPerson: async (_: any, { id }: any, { dataSources }: any) =>
      dataSources.projects.getProjectsByPerson(id),
    projectsByProgram: async (
      _: any,
      { programName }: any,
      { dataSources }: any
    ) => dataSources.projects.getProjectsByProgram(programName),
    partner: async (_: any, { id }: any, { dataSources }: any) =>
      dataSources.partners.getPartner(id),
    partners: async (_: any, { id }: any, { dataSources }: any) =>
      dataSources.partners.getPartners(),
    portfolio: async (_: any, { id }: any, { dataSources }: any) =>
      dataSources.portfolios.getPortfolio(id),
    portfolios: async (_: any, { id }: any, { dataSources }: any) =>
      dataSources.portfolios.getPortfolios(),
    property: async (_: any, { id }: any, { dataSources }: any) =>
      dataSources.properties.getProperty(id),
    properties: async (
      _: any,
      { ids, searchTerm }: any,
      { dataSources }: any
    ) => {
      if (ids) {
        return dataSources.properties.getProperties(ids);
      } else if (searchTerm) {
        return dataSources.properties.searchProperties(searchTerm);
      } else {
        return dataSources.properties.getProperties();
      }
    },
    findProperties: async (_: any, { filter }: any, { dataSources }: any) =>
      dataSources.properties.matchProperties(filter),
    survey: async (_: any, { id }: any, { dataSources }: any) =>
      dataSources.surveys.getSurvey(),
    proposal: async (_: any, { id }: any, { dataSources }: any) =>
      dataSources.proposals.getProposal(id),
    proposals: async (_: any, { id }: any, { dataSources }: any) =>
      dataSources.proposals.getProposals(),
    lineItem: async (_: any, { id }: any, { dataSources }: any) =>
      dataSources.lineItems.getLineItem(id),
    lineItems: async (_: any, { id }: any, { dataSources }: any) =>
      dataSources.lineItems.getLineItems(),
    savings: async (_: any, { id, isDraft }: any, { dataSources }: any) => {
      if (id) {
        return dataSources.savings.getSavings(id, isDraft);
      } else {
        return dataSources.savings.getSavings(_, isDraft);
      }
    },
    task: async (_: any, { id }: any, { dataSources }: any) =>
      dataSources.tasks.getTask(id),
    tasks: async (_: any, { id }: any, { dataSources }: any) =>
      dataSources.tasks.getTasks(),
    stage: async (_: any, { id }: any, { dataSources }: any) =>
      dataSources.stages.getStage(id),
    shareMyDataQueue: async (_: any, {}: any, { dataSources }: any) =>
      dataSources.shareMyDataQueue.getQueueItems(),
    utilityDataIntegration: async (_: any, { id }: any, { dataSources }: any) =>
      dataSources.utilityDataIntegrations.getUtilityDataIntegration(id),
  },
  Mutation: {
    addPerson: (_: any, { person }: any, { dataSources }: any) =>
      dataSources.people.addPerson(person),
    deletePerson: async (_: any, { id }: any, { dataSources }: any) =>
      await dataSources.people.deletePerson(id),
    // applyIncrementedProjectNumberToAllProjects: async (
    //   _: any,
    //   {}: any,
    //   { dataSources }: any
    // ) => {
    //   let projects = await dataSources.projects.getAllProjects();
    //   for (let project of projects.data) {
    //     let nextAutoProjectNumber =
    //       await dataSources.counters.getNextProjectNumber();
    //     project.number = nextAutoProjectNumber;
    //     await dataSources.projects.updateProject(project._id, project);
    //   }
    // },
    addProject: async (
      _: any,
      { project }: any,
      { dataSources, user }: any
    ) => {
      let nextAutoProjectNumber =
        await dataSources.counters.getNextProjectNumber();
      logger.debug(nextAutoProjectNumber);
      return await dataSources.projects.addProject(
        user,
        project,
        nextAutoProjectNumber
      );
    },
    deleteProject: async (_: any, { id }: any, { dataSources }: any) =>
      await dataSources.projects.deleteProject(id),
    updateProject: async (_: any, { id, project }: any, { dataSources }: any) =>
      await dataSources.projects.updateProject(id, project),
    updateProgram: async (_: any, { id, program }: any, { dataSources }: any) =>
      await dataSources.programs.updateProgram(id, program),
    deleteProgram: (_: any, { id }: any, { dataSources }: any) =>
      dataSources.programs.deleteProgram(id),
    addProgram: (_: any, { program }: any, { dataSources }: any) =>
      dataSources.programs.addProgram(program),
    addPartner: (_: any, { partner }: any, { dataSources }: any) =>
      dataSources.partners.addPartner(partner),
    deletePartner: (_: any, { id }: any, { dataSources }: any) =>
      dataSources.partners.deletePartner(id),
    updatePartner: (_: any, { id, partner }: any, { dataSources }: any) =>
      dataSources.partners.updatePartner(id, partner),
    updatePerson: (_: any, { id, person }: any, { dataSources }: any) =>
      dataSources.people.updatePerson(id, person),
    addPortfolio: (_: any, { portfolio }: any, { dataSources }: any) =>
      dataSources.portfolios.addPortfolio(portfolio),
    deletePortfolio: (_: any, { id }: any, { dataSources }: any) =>
      dataSources.portfolios.deletePortfolio(id),
    updatePortfolio: (_: any, { id, portfolio }: any, { dataSources }: any) =>
      dataSources.portfolios.updatePortfolio(id, portfolio),
    addProposal: (_: any, { proposal }: any, { dataSources }: any) =>
      dataSources.proposals.addProposal(proposal),
    deleteProposal: (_: any, { id }: any, { dataSources }: any) =>
      dataSources.proposals.deleteProposal(id),
    updateProposal: (_: any, { id, proposal }: any, { dataSources }: any) =>
      dataSources.proposals.updateProposal(id, proposal),
    addProperty: (_: any, { property }: any, { dataSources }: any) => {
      return dataSources.properties.addProperty(property);
    },
    addSurvey: (_: any, { survey }: any, { dataSources }: any) => {
      return dataSources.surveys.addSurvey(survey);
    },
    updateSurvey: (_: any, { id, survey }: any, { dataSources }: any) =>
      dataSources.surveys.updateSurvey(id, survey),
    deleteProperty: async (_: any, { id }: any, { dataSources }: any) => {
      const property = await dataSources.properties.getProperty(id);
      for (let survey of property.data[0].surveys) {
        console.log('deleting a survey ' + survey);
        dataSources.surveys.deleteSurvey(survey);
      }
      for (let udi of property.data[0].utility_data_integrations) {
        dataSources.utilityDataIntegrations.deleteUtilityDataIntegration(udi);
      }
      return await dataSources.properties.deleteProperty(id);
    },
    updateProperty: (_: any, { id, property }: any, { dataSources }: any) => {
      return dataSources.properties.updateProperty(id, property);
    },
    addLineItem: (_: any, { lineItem }: any, { dataSources }: any) => {
      return dataSources.lineItems.addLineItem(lineItem);
    },
    deleteLineItem: async (_: any, { id }: any, { dataSources }: any) =>
      await dataSources.lineItems.deleteLineItem(id),
    updateLineItem: (_: any, { id, measure }: any, { dataSources }: any) => {
      return dataSources.lineItems.updateLineItem(id, measure);
    },
    addSavings: async (
      _: any,
      { savings }: any,
      { dataSources, user }: any
    ) => {
      const result = await dataSources.savings.addSavings(savings);

      logger.debug(result);
      return result;
    },
    deleteSavings: (_: any, { id }: any, { dataSources }: any) =>
      dataSources.savings.deleteSavings(id),
    updateSavings: (_: any, { id, savings }: any, { dataSources }: any) =>
      dataSources.savings.updateSavings(id, savings),
    addTask: (_: any, { task }: any, { dataSources }: any) =>
      dataSources.tasks.addTask(task),
    deleteTask: (_: any, { id }: any, { dataSources }: any) =>
      dataSources.tasks.deleteTask(id),
    updateTask: (_: any, { id, task }: any, { dataSources }: any) =>
      dataSources.tasks.updateTask(id, task),
    addStage: (_: any, { stage }: any, { dataSources }: any) =>
      dataSources.stages.addStage(stage),
    deleteStage: (_: any, { id }: any, { dataSources }: any) =>
      dataSources.stages.deleteStage(id),
    updateStage: (_: any, { id, stage }: any, { dataSources }: any) =>
      dataSources.stages.updateTask(id, stage),
  },
};
