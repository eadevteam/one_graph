/**
 * Stupid DocumentDB doesn't implement the standard mongoDB aggregation pipeline $set,
 * which would allow for us to write valid mongoDB update queries like so:
 * updateMany({}, [{$set: {company: {name: '$name'}}}]);
 *
 * The important piece there is the [{}] array brackets wrapping the set operation, which allows us to access
 * existing field values. We can't do that in DocumentDB.
 *
 * So instead, we have to use the old, sloppy aggregate() method which creates a whole new collection.
 * If we give it an $out option, we can set the original collection equal to the new modified collection.
 *
 * BUT, there's a bug in the migrate-mongo library where the aggregate method $out option doesn't properly get awaited.
 * SO, we have to call .toArray() on the result of the aggregate() method. Idk man. Amazon sucks.
 */

module.exports = {
  async up(db, client) {
    // See https://github.com/seppevs/migrate-mongo/#creating-a-new-migration-script
    await db
      .collection('properties')
      .aggregate([
        { $addFields: { company: { name: '$name' } } },
        { $out: 'properties' },
      ])
      .toArray();

    await db.collection('properties').updateMany({}, { $unset: { name: '' } });
  },

  async down(db, client) {
    await db
      .collection('properties')
      .aggregate([
        { $addFields: { name: '$company.name' } },
        { $out: 'properties' },
      ])
      .toArray();

    await db
      .collection('properties')
      .updateMany({}, { $unset: { company: '' } });
  },
};
